window._ = require('lodash');

import 'core-js/stable'
import Vue from 'vue'
import App from './App'
import router from './router'
import CoreuiVue from '@coreui/vue'
import { iconsSet as icons } from './assets/icons/icons.js'
import store from './store'
import axios from 'axios';
import { getRequestHeader } from "./services/auth";
import VueMask from 'v-mask'
import Notifications from 'vue-notification'
import VuejsDialog from 'vuejs-dialog';
import vSelect from "vue-select";
import VModal from 'vue-js-modal'
import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
import vueNumeralFilterInstaller from 'vue-numeral-filter';
import popperjs from '@popperjs/core';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// include the default style
import 'vuejs-dialog/dist/vuejs-dialog.min.css';
import BackButton from '@/components/BackButton';

import moment from 'moment-timezone';
import VueCookies from 'vue-cookies';
require('moment/locale/id')

var tz = moment.tz.guess();
console.log(tz);
moment.tz.setDefault(tz);

// Tell Vue to install the plugin.
Vue.config.performance = true
Vue.use(CoreuiVue)
Vue.use(BootstrapVue);
Vue.use(Notifications)
Vue.use(VuejsDialog,{
  html: true,
  okText: 'Ok',
  cancelText: 'Cancel',
});
Vue.use(VModal);
Vue.use(vueNumeralFilterInstaller);
Vue.use(require('vue-moment'), {
  moment
});
Vue.use(VueMask)
Vue.use(VueCookies)

Vue.component("v-select", vSelect);
Vue.component("date-picker",DatePicker);
Vue.component("BackButton", BackButton);

let instance = axios.create();
axios.interceptors.request.use(function (config) {
  config.headers = getRequestHeader().headers;
  return config;
});

router.beforeEach((to, from, next) => {
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  const currentUser = store.state.auth.currentUser;
  // const permissions = to.matched.map(record => record.meta.permission);
  // var unauthorized = false;

  // check permissions

  // if (currentUser && permissions) {
  //     $.each(permissions, (index, item) => {
  //         if (item != undefined && !currentUser.can[item]) {
  //             unauthorized = true;
  //         }
  //     });
  // }
  if (requiresAuth && !currentUser) { // if not logged in, redirect to login page
      next('/login');
  // } else if (unauthorized) {
  //     next('/error/unauthorized');
  } else if (to.path == '/login' && currentUser) { // if logged in, redirect to dashboard
      next('/');
  } else {
      next();
  }

})
// axios.defaults.headers = getRequestHeader().headers;
Vue.mixin({
  created() {
    // Add a response interceptor
    let self = this;
    axios.interceptors.response.use(function(response) {
      // Any status code that lie within the range of 2xx cause this function to trigger
      // Do something with response data
      return response;
    }, function(error) {
      // Any status codes that falls outside the range of 2xx cause this function to trigger
      if (error.response.status == 401)
        self.$router.push({ path: '/login' }).catch(err => {});
      return Promise.reject(error);
    });

    this.$cookies.set("timezone", tz, 0);

  },
  methods: {
    forceRerender() {
      this.$eventHub.$emit('force-rerender');
    },
    downloadFile(param) {
      if (param.type.toLowerCase() == "get") {
        axios.get(param.url, {
            params: param.data,
            responseType: 'blob',
          })
          .then((response) => {
            this.download(response);
            this.$notify({
              type: 'info',
              title: 'Please Wait',
              text: 'Your file is downloading...'
            })
          })
          .catch(err => {
            this.$notify({
              type: 'error',
              title: 'Oh no!',
              text: 'Failed to download'
            })
          })
      } else if (param.type.toLowerCase() == "post") {
        axios.post(param.url, param.data, {
            responseType: 'blob',
          })
          .then((response) => {
            this.download(response);
          })
          .catch(err => {
            this.$notify({
              type: 'error',
              title: 'Oh no!',
              text: 'Failed to download'
            })
          })
      }
    },
    download(response) {
      // get filename
      var filename = "";
      var disposition = response.headers['content-disposition'];
      if (disposition && disposition.indexOf('attachment') !== -1) {
        var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
        var matches = filenameRegex.exec(disposition);
        if (matches != null && matches[1]) {
          filename = matches[1].replace(/['"]/g, '');
        }
      }

      const url = window.URL.createObjectURL(new Blob([response.data]));
      const link = document.createElement('a');
      link.href = url;
      link.setAttribute('download', filename); //or any other extension
      document.body.appendChild(link);
      link.click();
    }
  },
  computed: {
    currentUser() {
      return JSON.parse(sessionStorage.getItem("user"));
    }
  }
})

Vue.prototype.$eventHub = new Vue();

new Vue({
  el: '#app',
  router,
  store,  
  icons,
  template: '<App/>',
  components: {
    App
  },
})
