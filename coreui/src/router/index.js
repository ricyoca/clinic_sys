import Vue from 'vue'
import Router from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')
const ThePopupContainer = () => import('@/containers/ThePopupContainer')
// import TheContainer from '../containers/TheContainer';
// Views
const Dashboard = () => import('@/views/Dashboard')

// const Colors = () => import('@/views/theme/Colors')
// const Typography = () => import('@/views/theme/Typography')

// const Charts = () => import('@/views/charts/Charts')
// const Widgets = () => import('@/views/widgets/Widgets')

// // Views - Components
// const Cards = () => import('@/views/base/Cards')
// const Forms = () => import('@/views/base/Forms')
// const Switches = () => import('@/views/base/Switches')
// const Tables = () => import('@/views/base/Tables')
// const Tabs = () => import('@/views/base/Tabs')
// const Breadcrumbs = () => import('@/views/base/Breadcrumbs')
// const Carousels = () => import('@/views/base/Carousels')
// const Collapses = () => import('@/views/base/Collapses')
// const Jumbotrons = () => import('@/views/base/Jumbotrons')
// const ListGroups = () => import('@/views/base/ListGroups')
// const Navs = () => import('@/views/base/Navs')
// const Navbars = () => import('@/views/base/Navbars')
// const Paginations = () => import('@/views/base/Paginations')
// const Popovers = () => import('@/views/base/Popovers')
// const ProgressBars = () => import('@/views/base/ProgressBars')
// const Tooltips = () => import('@/views/base/Tooltips')

// // Views - Buttons
// const StandardButtons = () => import('@/views/buttons/StandardButtons')
// const ButtonGroups = () => import('@/views/buttons/ButtonGroups')
// const Dropdowns = () => import('@/views/buttons/Dropdowns')
// const BrandButtons = () => import('@/views/buttons/BrandButtons')

// // Views - Icons
// const CoreUIIcons = () => import('@/views/icons/CoreUIIcons')
// const Brands = () => import('@/views/icons/Brands')
// const Flags = () => import('@/views/icons/Flags')

// // Views - Notifications
// const Alerts = () => import('@/views/notifications/Alerts')
// const Badges = () => import('@/views/notifications/Badges')
// const Modals = () => import('@/views/notifications/Modals')

// Views - Pages
const Page404 = () => import('@/views/pages/Page404')
// const Page500 = () => import('@/views/pages/Page500')
const Login = () => import('@/views/pages/Login')
const Register = () => import('@/views/pages/Register')

// Users
const IndexUser = () => import('@/views/users/IndexUser')
const ShowUser = () => import('@/views/users/ShowUser')
const EditUser = () => import('@/views/users/EditUser')
const CreateUser = () => import('@/views/users/CreateUser')

// Satusehat Master Integration
const IndexSatusehatMasterIntegration = () => import('@/views/satusehatmaster/IndexSatusehatMasterIntegration')
const PatientSatusehatMasterIntegration = () => import('@/views/satusehatmaster/PatientSatusehatMasterIntegration')
const ConfigSatusehatMasterIntegration = () => import('@/views/satusehatmaster/ConfigSatusehatMasterIntegration')

// //Notes
// const Notes = () => import('@/views/notes/Notes')
// const Note = () => import('@/views/notes/Note')
// const EditNote = () => import('@/views/notes/EditNote')
// const CreateNote = () => import('@/views/notes/CreateNote')

// //Roles
// const Roles = () => import('@/views/roles/Roles')
// const Role = () => import('@/views/roles/Role')
// const EditRole = () => import('@/views/roles/EditRole')
// const CreateRole = () => import('@/views/roles/CreateRole')

// //Bread
// const Breads = () => import('@/views/bread/Breads')
// const Bread = () => import('@/views/bread/Bread')
// const EditBread = () => import('@/views/bread/EditBread')
// const CreateBread = () => import('@/views/bread/CreateBread')
// const DeleteBread = () => import('@/views/bread/DeleteBread')

// //Resources
// const Resources = () => import('@/views/resources/Resources')
// const CreateResource = () => import('@/views/resources/CreateResources')
// const Resource = () => import('@/views/resources/Resource')
// const EditResource = () => import('@/views/resources/EditResource')
// const DeleteResource = () => import('@/views/resources/DeleteResource')

// //Email
// const Emails        = () => import('@/views/email/Emails')
// const CreateEmail   = () => import('@/views/email/CreateEmail')
// const EditEmail     = () => import('@/views/email/EditEmail')
// const ShowEmail     = () => import('@/views/email/ShowEmail')
// const SendEmail     = () => import('@/views/email/SendEmail')

// const Menus       = () => import('@/views/menu/MenuIndex')
// const CreateMenu  = () => import('@/views/menu/CreateMenu')
// const EditMenu    = () => import('@/views/menu/EditMenu')
// const DeleteMenu  = () => import('@/views/menu/DeleteMenu')

// const MenuElements = () => import('@/views/menuElements/ElementsIndex')
// const CreateMenuElement = () => import('@/views/menuElements/CreateMenuElement')
// const EditMenuElement = () => import('@/views/menuElements/EditMenuElement')
// const ShowMenuElement = () => import('@/views/menuElements/ShowMenuElement')
// const DeleteMenuElement = () => import('@/views/menuElements/DeleteMenuElement')

// const Media = () => import('@/views/media/Media')

// category
// const IndexCategory = () => import('@/views/category/IndexCategory')
// const ShowCategory = () => import('@/views/category/ShowCategory')
// const EditCategory = () => import('@/views/category/EditCategory')
// const CreateCategory = () => import('@/views/category/CreateCategory')

// // subcategory
// const IndexSubcategory = () => import('@/views/subcategory/IndexSubcategory')
// const ShowSubcategory = () => import('@/views/subcategory/ShowSubcategory')
// const EditSubcategory = () => import('@/views/subcategory/EditSubcategory')
// const CreateSubcategory = () => import('@/views/subcategory/CreateSubcategory')

// // product
// const IndexProduct = () => import('@/views/product/IndexProduct')
// const ShowProduct = () => import('@/views/product/ShowProduct')
// const EditProduct = () => import('@/views/product/EditProduct')
// const CreateProduct = () => import('@/views/product/CreateProduct')

// // supplier
// const IndexSupplier = () => import('@/views/supplier/IndexSupplier')
// const ShowSupplier = () => import('@/views/supplier/ShowSupplier')
// const EditSupplier = () => import('@/views/supplier/EditSupplier')
// const CreateSupplier = () => import('@/views/supplier/CreateSupplier')

// // customer
// const IndexCustomer = () => import('@/views/customer/IndexCustomer')
// const ShowCustomer = () => import('@/views/customer/ShowCustomer')
// const EditCustomer = () => import('@/views/customer/EditCustomer')
// const CreateCustomer = () => import('@/views/customer/CreateCustomer')

// // Mechanic
// const IndexMechanic = () => import('@/views/mechanic/IndexMechanic')
// const ShowMechanic = () => import('@/views/mechanic/ShowMechanic')
// const EditMechanic = () => import('@/views/mechanic/EditMechanic')
// const CreateMechanic = () => import('@/views/mechanic/CreateMechanic')

// // Location
// const IndexLocation = () => import('@/views/location/IndexLocation')
// const ShowLocation = () => import('@/views/location/ShowLocation')
// const EditLocation = () => import('@/views/location/EditLocation')
// const CreateLocation = () => import('@/views/location/CreateLocation')

// // PO
// const IndexPo = () => import('@/views/po/IndexPo')
// const ShowPo = () => import('@/views/po/ShowPo')
// const EditPo = () => import('@/views/po/EditPo')
// const CreatePo = () => import('@/views/po/CreatePo')

// // Purchase
// const IndexPurchase = () => import('@/views/purchase/IndexPurchase')
// const ShowPurchase = () => import('@/views/purchase/ShowPurchase')
// const EditPurchase = () => import('@/views/purchase/EditPurchase')
// const CreatePurchase = () => import('@/views/purchase/CreatePurchase')

// // Purchase Return
// const IndexPurchaseReturn = () => import('@/views/purchase_return/IndexPurchaseReturn')
// const ShowPurchaseReturn = () => import('@/views/purchase_return/ShowPurchaseReturn')
// const EditPurchaseReturn = () => import('@/views/purchase_return/EditPurchaseReturn')
// const CreatePurchaseReturn = () => import('@/views/purchase_return/CreatePurchaseReturn')

// // Account Payable Payment
// const IndexAccountPayablePayment = () => import('@/views/account_payable_payment/IndexAccountPayablePayment')
// const ShowAccountPayablePayment = () => import('@/views/account_payable_payment/ShowAccountPayablePayment')
// const EditAccountPayablePayment = () => import('@/views/account_payable_payment/EditAccountPayablePayment')
// const CreateAccountPayablePayment = () => import('@/views/account_payable_payment/CreateAccountPayablePayment')

// // Sales
// const IndexSales = () => import('@/views/sales/IndexSales')
// const ShowSales = () => import('@/views/sales/ShowSales')
// const EditSales = () => import('@/views/sales/EditSales')
// const CreateSales = () => import('@/views/sales/CreateSales')

// // Account Receivable Payment
// const IndexAccountReceivablePayment = () => import('@/views/account_receivable_payment/IndexAccountReceivablePayment')
// const ShowAccountReceivablePayment = () => import('@/views/account_receivable_payment/ShowAccountReceivablePayment')
// const EditAccountReceivablePayment = () => import('@/views/account_receivable_payment/EditAccountReceivablePayment')
// const CreateAccountReceivablePayment = () => import('@/views/account_receivable_payment/CreateAccountReceivablePayment')

// // Stock Transfer
// const IndexStockTransfer = () => import('@/views/stock_transfer/IndexStockTransfer')
// const ShowStockTransfer = () => import('@/views/stock_transfer/ShowStockTransfer')
// const EditStockTransfer = () => import('@/views/stock_transfer/EditStockTransfer')
// const CreateStockTransfer = () => import('@/views/stock_transfer/CreateStockTransfer')


// // Stock Opname
// const IndexStockOpname = () => import('@/views/stock_opname/IndexStockOpname')
// const ShowStockOpname = () => import('@/views/stock_opname/ShowStockOpname')
// const EditStockOpname = () => import('@/views/stock_opname/EditStockOpname')
// const CreateStockOpname = () => import('@/views/stock_opname/CreateStockOpname')

// const IndexStockCard = () => import('@/views/stock_card/IndexStockCard')

const IndexPatient = () => import('@/views/patient/IndexPatient')
const IndexDisease = () => import('@/views/disease/IndexDisease')

const CreateExamRegistration = () => import('@/views/examRegistration/CreateExamRegistration')
const EditExamRegistration = () => import('@/views/examRegistration/EditExamRegistration')
const IndexExamRegistration = () => import('@/views/examRegistration/IndexExamRegistration')

const EditExam = () => import('@/views/exam/EditExam')
const IndexExam = () => import('@/views/exam/IndexExam')
const ExamInvoice = () => import('@/views/exam/ExamInvoice')

const IndexAppointment = () => import('@/views/appointment/IndexAppointment')
const CreateAppointment = () => import('@/views/appointment/CreateAppointment')
const EditAppointment = () => import('@/views/appointment/EditAppointment')


const ChangePassword = () => import('@/views/ChangePassword')

//Popups
const EditPatientPopup = () => import('@/components/popups/EditPatientPopup')


Vue.use(Router)

export default new Router({
  mode: 'hash', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: TheContainer,
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: Dashboard
        },
        
        {
          path: 'user',
          meta: { label: 'Users'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: IndexUser,
            },
            {
              path: 'create',
              meta: { label: 'Create User'},
              name: 'Create User',
              component: CreateUser,
            },
            {
              path: ':id',
              meta: { label: 'User Details'},
              name: 'User',
              component: ShowUser,
            },
            {
              path: ':id/edit',
              meta: { label: 'Edit User' },
              name: 'Edit User',
              component: EditUser
            },
          ]
        },

        {
          path: 'satusehatmaster',
          meta: { label: 'Satusehat Master Integration'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [
            {
              path: '',
              component: IndexSatusehatMasterIntegration,
            },
            {
              path: 'patient-sync',
              component: PatientSatusehatMasterIntegration,
            },
            {
              path: 'config',
              component: ConfigSatusehatMasterIntegration,
            },
          ]
        },
        
        
        //////////////// additional routes
        {
          path: 'patient',
          meta: {label: 'Pasien'},
          component: IndexPatient,
        },
        {
          path: 'disease',
          meta: {label: 'Penyakit'},
          component: IndexDisease,
        },
        {
          path: 'exam-registration',
          meta: {label: 'Pendaftaran Antrian'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [{
            path: '/',
            meta: {label: 'Lihat Antrian'},
            component: IndexExamRegistration
          }, {
            path: 'new',
            name: 'Buat Baru',
            component: CreateExamRegistration
          }, {
            path: ':id/edit',
            name: 'Edit',
            component: EditExamRegistration
          }]
        },
        {
          path: 'exam',
          meta: {label: 'Pemeriksaan'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [{
            path: '/',
            meta: {label: 'Lihat Antrian'},
            component: IndexExam
          }, {
            path: ':id/edit',
            name: 'Edit',
            component: EditExam
          }, {
            path: ':id/invoice',
            name: 'Invoice',
            component: ExamInvoice
          }]
        },
        {
          path: 'appointment',
          meta: {label: 'Janji'},
          component: {
            render (c) { return c('router-view') }
          },
          children: [{
            path: '/',
            meta: {label: 'Lihat Janji'},
            component: IndexAppointment
          }, {
            path: 'new',
            name: 'Buat Baru',
            component: CreateAppointment
          }, {
            path: ':id/edit',
            name: 'Edit',
            component: EditAppointment
          }]
        },
        {
          path: 'change-password',
          component: ChangePassword
        }
        //////////////// end of additional routes
      ]
    },
    {
      path: '/component',
      redirect: '/dashboard',
      name: 'Component Home',
      component: ThePopupContainer,
      children: [
        {
          path: 'edit-patient-popup',
          name: 'Edit Patient Popup',
          component: EditPatientPopup
        },
        //End of popup routes
      ]
    },
    {
      path: '/',
      redirect: '/login',
      name: 'Auth',
      component: {
        render (c) { return c('router-view') }
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login
        },
        {
          path: 'register',
          name: 'Register',
          component: Register
        },
      ]
    },
    {
      path: '*',
      name: '404',
      component: Page404
    },
  ]
}
