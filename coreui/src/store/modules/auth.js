import { getLocalUser, getRequestHeader } from '../../services/auth.js'


const user = getLocalUser();

export default {
    namespaced: true,
    state: {
        currentUser: user,
        isLoggedIn: !!user,
        auth_error: null
    },
    getters: {
        isLoggedIn(state) {
            return state.isLoggedIn;
        },
        currentUser(state) {
            return state.currentUser;
        },
        authError(state) {
            return state.auth_error;
        }
    },
    mutations: {
        login(state) {
            state.auth_error = null;
        },
        loginSuccess(state, payload) {
            state.auth_error = null;
            state.isLoggedIn = true;
            state.currentUser = Object.assign({}, payload.user, { token: payload.access_token });

            sessionStorage.setItem('user', JSON.stringify(state.currentUser))
            axios.defaults.headers = getRequestHeader().headers;
        },
        loginFailed(state, payload) {
            state.auth_error = payload.error;
        },
        logout(state) {
            sessionStorage.removeItem('user');
            state.currentUser = null;
            axios.defaults.headers = getRequestHeader().headers;
        }
    },
    actions: {
        login(context) {
            context.commit('login');
        },
        refresh(context) {
            
        }
    }
}