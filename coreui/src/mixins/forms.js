export default {
    props: {
        dataModel: Object,
        errorModel: {
            type: Object,
            default: () => {
                return {}
            }
        },
        readOnly: {
            type: Boolean,
            default: false
        },
        labelWidth: {
            type: Number,
            default: 3
        },
    },
    watch: {
        // dataModel: {
        //   immediate: true,
        //   handler() {
        //     this.form = this.dataModel;
        //   }
        // },
        errorModel: {
            immediate: true,
            handler() {
                this.errors = this.errorModel;
            }
        },
    },
    data() {
        return {
            form: {},
            errors: {}
        }
    },
    methods: {
        setFormData(data) {
            this.form = Object.assign({},data);
        },
        getFormData() {
            return this.form;
        },
        clearFormData() {
            this.form = {};
        }
    }
}