import {
    AgGridVue
} from "ag-grid-vue";
import numeral from "numeral";

export default {
    components: {
        AgGridVue,
    },
    data() {
        return {
            gridOptions: null,
        }
    },
    beforeMount() {

        this.gridOptions = {
            headerHeight: 35,
            rowHeight: 30,
            rowSelection: 'multiple',
            stopEditingWhenGridLosesFocus: true,
            suppressScrollOnNewData: true,
            defaultColDef: {
                tooltipValueGetter: (params) => {
                    if (this.errors && this.errors['detail.' + params.node.rowIndex + '.' + params.colDef.field]) {
                        return this.errors['detail.' + params.node.rowIndex + '.' + params.colDef.field][0]
                    }

                    if (params.valueFormatted === '-' && params.colDef.editable) {
                        return '(Click to Edit)'
                    }

                    if (params.valueFormatted !== null)
                        return params.valueFormatted

                    if (params.value !== undefined && params.value !== null)
                        return params.value.toString()


                    return ' ';
                },
                cellClassRules: {
                    'editable-grid-cell': (params) => {
                        return params.colDef.editable == true || (typeof params.colDef.editable == 'function' && params.colDef.editable(params) == true);
                    },
                    'bg-danger': (params) => {
                        return this.errors && this.errors['detail.' + params.rowIndex + '.' + params.colDef.field];
                    },
                },
                valueFormatter: (params) => {
                    if (!params.value) {
                        return '-';
                    }
                    return params.value;
                },
                singleClickEdit: true,
                resizable: true,
            },
            enterMovesDownAfterEdit: true,
            rowClassRules: {
                'locked-price-row': (params) => {
                    return params.data.hasOwnProperty('will_update_sales_price') && params.data.will_update_sales_price == false;
                }
            },
            // onRowDataChanged: (event) => {
            //     let lastIndex = event.api.getModel().allChildrenCount - 1;
            //     event.api.ensureIndexVisible(lastIndex,'bottom');
            // },
            context: {
                componentParent: this
            },
            navigateToNextCell: (params) => {

                var previousCell = params.previousCellPosition;
                var suggestedNextCell = params.nextCellPosition;

                var KEY_UP = 38;
                var KEY_DOWN = 40;
                var KEY_LEFT = 37;
                var KEY_RIGHT = 39;

                switch (params.key) {
                    case KEY_DOWN:
                        var previousNode = previousCell.column.gridApi.getRowNode(previousCell.rowIndex);
                        previousCell = params.previousCellPosition;
                        // set selected cell on current cell + 1
                        previousCell.column.gridApi.forEachNode(function(node) {
                            if (previousCell.rowIndex + 1 === node.rowIndex) {
                                if (params.event.shiftKey) {
                                    if (node.isSelected()) {
                                        previousNode.setSelected(false);
                                    } else {
                                        node.setSelected(true);
                                    }
                                } else
                                    node.setSelected(true, true);
                            }
                        });
                        return suggestedNextCell;
                    case KEY_UP:
                        var previousNode = previousCell.column.gridApi.getRowNode(previousCell.rowIndex);
                        previousCell = params.previousCellPosition;
                        // set selected cell on current cell - 1
                        previousCell.column.gridApi.forEachNode(function(node) {
                            if (previousCell.rowIndex - 1 === node.rowIndex) {
                                if (params.event.shiftKey) {
                                    if (node.isSelected()) {
                                        previousNode.setSelected(false);
                                    } else {
                                        node.setSelected(true);
                                    }
                                } else
                                    node.setSelected(true, true);
                            }
                        });
                        return suggestedNextCell;
                    case KEY_LEFT:
                    case KEY_RIGHT:
                        return suggestedNextCell;
                    default:
                        throw "this will never happen, navigation is always one of the 4 keys above";
                }
            },
            onCellKeyDown: (e) => {

                e.event.preventDefault();
                let keyPressed = e.event.key;

                switch (keyPressed) {
                    case 'Delete':
                        let removeCol = e.columnApi.getColumn('');
                        if (!removeCol) return;

                        if (typeof removeCol.colDef.cellRendererParams.removeItemCallback == 'function') {
                            // if(!confirm("Are you sure you want to delete the selected rows?")) return;

                            let selectedNodes = e.api.getSelectedNodes().reverse();
                            _.forEach(selectedNodes, (item, index) => {
                                removeCol.colDef.cellRendererParams.removeItemCallback(item.data, item.id, removeCol.colDef.cellRendererParams.dataArray(), removeCol.colDef.cellRendererParams.deleteDataArray());
                            })
                        }

                        break;
                    case 'F3':
                        // e.event.originalEvent.keyCode = 0;
                    default:
                        break;
                }



            },
            //     suppressKeyboardEvent: (params) => {
            //         var KEY_A = 65;
            //         var KEY_C = 67;
            //         var KEY_V = 86;
            //         var KEY_D = 68;
            //         var KEY_PAGE_UP = 33;
            //         var KEY_PAGE_DOWN = 34;
            //         var KEY_TAB = 9;
            //         var KEY_LEFT = 37;
            //         var KEY_UP = 38;
            //         var KEY_RIGHT = 39;
            //         var KEY_DOWN = 40;
            //         var KEY_F2 = 113;
            //         var KEY_F3 = 114;
            //         var KEY_BACKSPACE = 8;
            //         var KEY_ESCAPE = 27;
            //         var KEY_SPACE = 32;
            //         var KEY_DELETE = 46;
            //         var KEY_PAGE_HOME = 36;
            //         var KEY_PAGE_END = 35;
            //         var event = params.event;
            //         var key = event.which;
            //         var keysToSuppress = [
            //           KEY_PAGE_UP,
            //           KEY_PAGE_DOWN,
            //           KEY_TAB,
            //           KEY_F3,
            //           KEY_ESCAPE,
            //         ];
            //         var editingKeys = [
            //           KEY_LEFT,
            //           KEY_RIGHT,
            //           KEY_UP,
            //           KEY_DOWN,
            //           KEY_BACKSPACE,
            //           KEY_DELETE,
            //           KEY_SPACE,
            //           KEY_PAGE_HOME,
            //           KEY_PAGE_END,
            //         ];
            //         if (event.ctrlKey || event.metaKey) {
            //           keysToSuppress.push(KEY_A);
            //           keysToSuppress.push(KEY_V);
            //           keysToSuppress.push(KEY_C);
            //           keysToSuppress.push(KEY_D);
            //         }
            //         if (!params.editing) {
            //           keysToSuppress = keysToSuppress.concat(editingKeys);
            //         }
            //         var suppress = keysToSuppress.indexOf(key) >= 0;
            //         return suppress;

            //     },
        }
    },
    methods: {
        refreshCells() {
            this.gridOptions.api.refreshCells();
            // this.gridOptions.api.redrawRows();
        },
        redrawRows() {
            this.gridOptions.api.redrawRows();
        },
        dateComparator(date1, date2) {
            var date1Number = this.monthToComparableNumber(date1);
            var date2Number = this.monthToComparableNumber(date2);

            if (date1Number === null && date2Number === null) {
                return 0;
            }
            if (date1Number === null) {
                return -1;
            }
            if (date2Number === null) {
                return 1;
            }
            return date1Number - date2Number;
        },
        monthToComparableNumber(date) {
            if (date === undefined || date === null || !this.$moment(date).isValid()) {
                return null;
            }
            var yearNumber = this.$moment(date).year();
            var monthNumber = this.$moment(date).month();
            var dayNumber = this.$moment(date).date();

            var result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
            return result;
        },
        updateSeqNumber() {
            this.$nextTick(() => {
                var itemsToUpdate = [];
                this.gridOptions.api.forEachNodeAfterFilterAndSort(function(rowNode, index) {

                    var data = rowNode.data;
                    data.seq_no = index + 1;
                    itemsToUpdate.push(data);
                });
                var res = this.gridOptions.api.applyTransaction({
                    update: itemsToUpdate
                });
            })
        },
        numberFormatter(params) {
            if (params.value === null || params.value === '' || params.value === undefined) {
                return '-';
            }
            return numeral(params.value).format();
        },
        dateFormatter(params) {
            if (params.value === null || params.value === '' || params.value === undefined) {
                return '-';
            }
            return this.$moment(params.value).format("DD-MM-YYYY");
        }
    },
    beforeDestroy() {
        if (this.gridOptions.api) {
            this.gridOptions.api.destroy();

        }
    },
}