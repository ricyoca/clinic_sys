export function getConfig() {
    return new Promise((res, rej) => {
        axios.get(`/api/config/all`)
            .then((response) => {
                res(response.data);
            })
            .catch((err) => {
                rej('rejected');
            })
    })
}

export function setConfig(data) {
  return new Promise((res, rej) => {
      axios.post(`/api/config/update`,data)
          .then((response) => {
              res(response.data);
          })
          .catch((err) => {
              rej('rejected');
          })
  })
}

export function getLocalConfig() {
  const configStr = sessionStorage.getItem("config");
  if (!configStr) {
      return null;
  }

  return JSON.parse(configStr);
}