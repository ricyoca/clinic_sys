


export function login(credentials) {
    return new Promise((res, rej) => {
        axios.post('/api/login', credentials)
            .then((response) => {
                res(response.data);
                
            })
            .catch((err) => {
                rej('wrong username or password');
            })
    })
}

export function getLocalUser() {
    const userStr = sessionStorage.getItem('user');
    if (!userStr) {
        return null;
    }

    return JSON.parse(userStr);
}

export function getRequestHeader() {
    let user = getLocalUser();
    if (user) {
        return {
            headers: {
                'Authorization' : `Bearer ${user.token}`,
                'Content-Type': 'application/json;charset=UTF-8',
                accept: 'application/json',
            }
        }
    }
    return {headers:{
        'Content-Type': 'application/json;charset=UTF-8',
        accept: 'application/json',
    }};
}