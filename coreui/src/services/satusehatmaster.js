const satusehatMasterAPIUrl = process.env.MIX_SATUSEHATMASTER_API_URL;
const solamedicUsername = process.env.MIX_SOLAMEDIC_USERNAME;
const solamedicPassword = process.env.MIX_SOLAMEDIC_PASSWORD;

import { v4 as uuidv4 } from "uuid";

export function getConfig() {
  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
  return {
    headers: { Authorization: `Bearer ${token}` }
  };

}

export async function sendBundle(data) {
  var config = getConfig();

  const practitionerUuidResponse = await axios.get('/api/config/get-value', {
    params: {
      key: 'practitioner_ihs_number',
    }
  });

  let practitionerUuid = practitionerUuidResponse.data;
  
  console.log(satusehatMasterAPIUrl);

  const encounterUUid = uuidv4();
  const conditionUUid = uuidv4();


  const response = await axios.post(`${satusehatMasterAPIUrl}/bundle/send-bundle`, {
    'entries': [
      {
        "resource_type": "Encounter",
        "uuid": encounterUUid,
        "period": {
          "start": data.period_start,
          "end": data.period_end
        },
        "encounter_class_id": data.encounter_class_id,
        "encounter_status_id": data.encounter_status_id,
        "patient_mr_number": data.patient.mr_number,
        "participant_uuid": practitionerUuid,
        "location_uuid": data.location_uuid,
        "status_history": data.encounter_status_history,
        "diagnosises": [
          {
            "encounter_diagnosis_use_id": 2, //DD / Discharge Diagnosis
            "encounter_diagnosis_uuid": conditionUUid,
            "encounter_diagnosis_display": data.icd10_diagnosis_description,
            "encounter_diagnosis_rank": 1,
          }
        ]
      },
      {
        "resource_type": "Condition",
        "uuid": conditionUUid,
        "condition_clinicalstatus_id": data.condition_clinicalstatus_id,
        "condition_category_id": data.condition_category_id,
        "icd10_code": data.icd10_diagnosis_code,
        "patient_mr_number": data.patient.mr_number,
      }
    ],
  }, config);

  if (response.data.status == 'success') {
    const response = await axios.post(`api/exam/${data.id}/satusehat-integrated`, {});
    console.log(response);
  }

  return response;
}

export async function generateToken() {
  const response = await axios.post(`${satusehatMasterAPIUrl}/spa-login`, {
    'username': solamedicUsername,
    'password': solamedicPassword,
  })

  if (response.status == 200) {
    localStorage.setItem('satusehatBearerToken', JSON.stringify(response.data.token));
    await axios.post(`api/token/save-solamedic-token`, { solamedicToken: response.data.token });
    return response.data.token;
  } else {
    return "error";
  }
}

export async function getToken() {
  const response = await axios.get(`api/token/get-solamedic-token`);

  if (response.status == 200) {
    localStorage.setItem('satusehatBearerToken', JSON.stringify(response.data));
    return response.data;
  } else {
    return "error";
  }
}

export async function sendNewPatient(data) {
  var config = getConfig();

  let payload = {
    "is_newborn": false, //karena case klinik og jadi tidak ada kasus pendaftaran pasien yang baru lahir
    "nik": data.nik,
    "gender": "female",
    "active": true,
    "name": data.name,
    "mr_number": data.mr_number,
    "birthdate": data.birth_date
  }
  
  const response = await axios.post(`${satusehatMasterAPIUrl}/patient/register-patient`, payload, config);

  if (response.data.status == 'success') {
    const response = await axios.post(`api/patient/${data.id}/satusehat-integrated`, {});
    console.log(response);
  }
  // console.log('below here is response form console log in line 80');
  // console.log(response);

  return response;
}

export async function getConditionClinicalStatus() {

  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
  const config = {
    headers: { Authorization: `Bearer ${token}` }
  };

  return axios.get(`${satusehatMasterAPIUrl}/condition/get-condition-clinical-status`, config);
};
export async function getConditionCategory() {
  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);

  const config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.get(`${satusehatMasterAPIUrl}/condition/get-condition-category`, config);
};
export async function getEncounterStatus() {
  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);

  const config = {
    headers: { Authorization: `Bearer ${token}` }
  };
  return axios.get(`${satusehatMasterAPIUrl}/encounter/get-encounter-status`, config);
};
export async function getEncounterClass() {
  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);

  const config = {
    headers: { Authorization: `Bearer ${token}` }
  };

  return axios.get(`${satusehatMasterAPIUrl}/encounter/get-encounter-class`, config);
};
export async function getLocations() {
  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);

  const config = {
    headers: { Authorization: `Bearer ${token}` }
  };

  return axios.get(`${satusehatMasterAPIUrl}/location`, config);
};