<?php
namespace App\Services;

use App\Models;

class AppointmentService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Appointment::with('patient')->select([
            'appointment.*'
          ]);

          if (isset($request['search_text']) && !empty($request['search_text'])) {
              $rows = $rows->where(function ($rows) use ($request) {
                  
              });
          }

          if (isset($request['appointment_date']) && !empty($request['appointment_date'])) {
            $rows = $rows->where('appointment_date', $request['appointment_date']);
        }

          if (isset($request['unregistered'])) {
              $existingId = Models\Exam::whereNotNull('appointment_id')->pluck('appointment_id');
              $rows = $rows->whereNotIn('id', $existingId);
          }
  
          if (isset($request['sort']) && !empty($request['sort'])) {
              $sortOrder = explode('|', $request['sort']);
              $rows = $rows->orderBy($sortOrder[0], $sortOrder[1]);
          } else {
              $rows = $rows->orderBy('appointment.id', 'asc');
          }
          
          if (isset($request['per_page']) && !empty($request['per_page'])) {
              $rows = $rows->paginate($request['per_page']);
          } else {
              $rows = $rows->get();
          }

          return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $appointment= Models\Appointment::with('patient')->find($id);
        return $appointment;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $appointment = new Models\Appointment;
            // attributes here
            $appointment->appointment_date = $request['appointment_date'];
            $appointment->patient_id = $request['patient_id'];
            $appointment->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $appointment;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $appointment = Models\Appointment::find($id);
            $appointment->appointment_date = $request['appointment_date'];
            $appointment->patient_id = $request['patient_id'];
            // attributes here
            $appointment->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $appointment;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $appointment = Models\Appointment::find($id);
        if($appointment){
            return $appointment->delete();
        }
    }
}