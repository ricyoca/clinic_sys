<?php 
namespace App\Services;
use App\Models\Token;

class TokenService
{

    public static function saveSolamedicToken($request)
    {
        $token = Token::where('type', 'solamedic')->first();

        if(!isset($token))
            $token = new Token();

        $token->token = $request->solamedicToken;
        $token->type = 'solamedic';
        return $token->save();
    }

    public static function getSolamedicToken()
    {
        $token = Token::where('type', 'solamedic')->first();
        return $token->token;
    }
}