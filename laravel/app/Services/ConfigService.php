<?php
namespace App\Services;

use App\Models;
use App\Helpers;
use App\Models\Config;
use App\Models\Exam;
use Carbon\Carbon;
use Arr;
use Storage;
use Illuminate\Support\Str;
class ConfigService
{
    public static function getAllConfig()
    {
        $response = [];

        $configs = Config::all();
        foreach ($configs as $config) {
            $response[$config->key] = $config->value;
        }

        return $response;
    }

    public static function getValue($request)
    {
        $config = Config::where('key', $request->key)->first();
        return $config->value;
    }

    public static function setValue($request)
    {
        $config = Config::where('key', $request->key)->first();
        $config->value = $request->value;
        return $config->save();
    }

    public static function massSetValue($request)
    {
        $allRequest = $request->all();
        foreach($allRequest as $requestKey => $requestValue)
        {
            if($requestValue!=null)
            {
                $config = Config::where('key', $requestKey)->first();
                $config->value = $requestValue;
                $config->save();
            }
        }
    }
}