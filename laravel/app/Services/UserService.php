<?php
namespace App\Services;

use App\Models;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     */
    public static function getAll($request = [])
    {
        $rows = Models\User::select(
            'users.id',
            'users.name',
            'users.username',
            'users.status',
            'users.email_verified_at as registered');

          if (isset($request['search_text']) && !empty($request['search_text'])) {
              $rows = $rows->where(function ($q) use ($request) {
                  $q->where('users.name','LIKE','%'.$request['search_text'].'%')
                      ->orWhere('users.username','LIKE','%'.$request['search_text'].'%');
              });
          }
  
          if (isset($request['sort']) && !empty($request['sort'])) {
              $rows = $rows->orderBy($request['sort'], $request['order']);
          } else {
              $rows = $rows->orderBy('id', 'asc');
          }
  
          if (isset($request['per_page']) && !empty($request['per_page'])) {
              $rows = $rows->paginate($request['per_page']);
          } else {
              $rows = $rows->get();
          }
          return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $user = Models\User::find($id);
        return $user;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $user = new Models\User;
            $user->name       = $request['name'];
            $user->username   = $request['username'];
            $user->status     = $request['status'];
            $user->password   = bcrypt($request['password']);
            $user->menuroles  = 'user';
            $user->save();

            $user->syncRoles([$request->role]);

        } catch (\Exception $e) {
            throw $e;
        }
        return $user;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $user = Models\User::find($id);
            $user->name       = $request['name'];
            $user->username      = $request['username'];
            $user->status     = $request['status'];
            $user->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $user;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $user = Models\User::find($id);
        if($user){
            return $user->delete();
        }
    }

    public static function changePassword($id, $request)
    {
        try {
            $user = Models\User::find($id);
            
            if (!Hash::check($request['old_password'], $user->password)) 
                throw new \Exception("Password does not match.", 1);
                
            $user->password = bcrypt($request['new_password']);
            $user->save();
            
        } catch (\Exception $e) {
            throw $e;
        }
        return $user;
    }
}