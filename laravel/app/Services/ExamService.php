<?php

namespace App\Services;

use App\Models;
use App\Helpers;
use App\Models\Config;
use App\Models\Exam;
use Carbon\Carbon;
use Arr;
use Illuminate\Support\Facades\DB;
use Storage;
use Illuminate\Support\Str;

class ExamService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Exam::with('patient')
            ->join('patient', 'exam.patient_id', '=', 'patient.id')
            ->leftJoin('invoice', 'invoice.id', '=', 'exam.invoice_id')
            ->select([
                'exam.*',
                'invoice.paid_at'
            ]);

        $rows = $rows->with('encounterStatusHistory');

        if (isset($request['search_text']) && !empty($request['search_text'])) {
            $rows = $rows->where(function ($rows) use ($request) {});
        }

        if (isset($request['exam_date']) && !empty($request['exam_date'])) {
            $rows = $rows->where('exam_date', $request['exam_date']);
        }

        if (isset($request['patient_id']) && !empty($request['patient_id'])) {
            $rows = $rows->where('patient_id', $request['patient_id']);
        }

        if (isset($request['complete_only']) && !empty($request['complete_only'])) {
            //   dd($request['show_complete']);
            $rows = $rows->where('exam.status', 'COMPLETE');
        } elseif (!(isset($request['show_complete']) && !empty($request['show_complete']) && $request['show_complete'] === 'true')) {
            //   dd($request['show_complete']);
            $rows = $rows->where(function ($rows) use ($request) {
                $rows->where('status', '<>', 'COMPLETE')
                    ->orWhereNull('status');
            });
        }

        if (isset($request['sort']) && !empty($request['sort'])) {
            $sortOrder = explode('|', $request['sort']);
            $rows = $rows->orderBy($sortOrder[0], $sortOrder[1]);
        } else {
            $rows = $rows->orderBy('exam.id', 'asc');
        }

        if (isset($request['per_page']) && !empty($request['per_page'])) {
            $rows = $rows->paginate($request['per_page']);
        } else {
            $rows = $rows->get();
        }

        return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $exam = Models\Exam::with(['patient' => function ($q) {
            $q->with('diseaseHistory', 'familyDiseaseHistory', 'obstetry');
        }, 'secondaryDiagnosis', 'encounterStatusHistory', 'usgFile', 'supportFile', 'invoice.invoiceMedication', 'invoice.invoiceTreatment'])->find($id);
        return $exam;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $exam = Models\Exam::where('patient_id', $request['patient_id'])
                ->where('exam_date', Carbon::now()->format('Y-m-d'))
                ->first();

            if ($exam) {
                throw new \Exception('Pasien sudah teregistrasi antrian hari ini.');
            }

            $exam = new Models\Exam;
            $exam->number = self::generateNumber();
            $exam->patient_id = $request['patient_id'];
            $exam->appointment_id = $request['appointment_id'];
            $exam->exam_date = $request['exam_date'];
            $exam->status = $request['status'];
            $exam->blood_pressure = $request['blood_pressure'];
            $exam->heart_rate = $request['heart_rate'];
            $exam->body_temperature = $request['body_temperature'];
            $exam->respiratory_rate = $request['respiratory_rate'];
            $exam->fall_history = $request['fall_history'];
            $exam->gestational_age = $request['gestational_age'];
            $exam->height = $request['height'];
            $exam->weight = $request['weight'];
            $exam->vulva_vagina = $request['vulva_vagina'];
            $exam->porsio = $request['porsio'];
            $exam->corpusuteri = $request['corpusuteri'];
            $exam->adnexad = $request['adnexad'];
            $exam->adnexas = $request['adnexas'];
            $exam->cavumdouglas = $request['cavumdouglas'];
            $exam->subjective_symptoms = $request['subjective_symptoms'];
            $exam->head = $request['head'];
            $exam->eye = $request['eye'];
            $exam->otology = $request['otology'];
            $exam->neck = $request['neck'];
            $exam->oral = $request['oral'];
            $exam->cardiology = $request['cardiology'];
            $exam->thorax_chest = $request['thorax_chest'];
            $exam->abdomen = $request['abdomen'];
            $exam->dermatology = $request['dermatology'];
            $exam->backbone_anatomy = $request['backbone_anatomy'];
            $exam->neurology = $request['neurology'];
            $exam->genital = $request['genital'];
            $exam->extremity = $request['extremity'];
            $exam->vagina_toucher = $request['vagina_toucher'];
            $exam->hodge_height = $request['hodge_height'];
            $exam->effice = $request['effice'];
            $exam->bloodslem = $request['bloodslem'];
            $exam->fetal_membrane = $request['fetal_membrane'];
            $exam->obstetric_notes = $request['obstetric_notes'];
            // $exam->icd_code = $request['icd_code'];
            $exam->diagnosis = $request['diagnosis'];
            $exam->planning = $request['planning'];
            $exam->monitoring = $request['monitoring'];
            $exam->next_appointment = $request['next_appointment'];
            $exam->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $exam;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {

        // dd($request);
        try {
            $exam = Models\Exam::find($id);

            $exam->appointment_id = $request['appointment_id'];
            $exam->exam_date = $request['exam_date'];

            if ($request['status'])
                $exam->status = $request['status'];
            else
                $exam->status = 'DRAFT';
            $exam->blood_pressure = $request['blood_pressure'];
            $exam->heart_rate = $request['heart_rate'];
            $exam->body_temperature = $request['body_temperature'];
            $exam->respiratory_rate = $request['respiratory_rate'];
            $exam->fall_history = $request['fall_history'];
            $exam->gestational_age = $request['gestational_age'];
            $exam->height = $request['height'];
            $exam->weight = $request['weight'];
            $exam->vulva_vagina = $request['vulva_vagina'];
            $exam->porsio = $request['porsio'];
            $exam->corpusuteri = $request['corpusuteri'];
            $exam->adnexad = $request['adnexad'];
            $exam->adnexas = $request['adnexas'];
            $exam->cavumdouglas = $request['cavumdouglas'];
            $exam->subjective_symptoms = $request['subjective_symptoms'];
            $exam->head = $request['head'];
            $exam->eye = $request['eye'];
            $exam->otology = $request['otology'];
            $exam->neck = $request['neck'];
            $exam->oral = $request['oral'];
            $exam->cardiology = $request['cardiology'];
            $exam->thorax_chest = $request['thorax_chest'];
            $exam->abdomen = $request['abdomen'];
            $exam->dermatology = $request['dermatology'];
            $exam->backbone_anatomy = $request['backbone_anatomy'];
            $exam->neurology = $request['neurology'];
            $exam->genital = $request['genital'];
            $exam->extremity = $request['extremity'];
            $exam->vagina_toucher = $request['vagina_toucher'];
            $exam->hodge_height = $request['hodge_height'];
            $exam->effice = $request['effice'];
            $exam->bloodslem = $request['bloodslem'];
            $exam->fetal_membrane = $request['fetal_membrane'];
            $exam->obstetric_notes = $request['obstetric_notes'];
            // $exam->icd_code = $request['icd_code'];
            $exam->diagnosis = $request['diagnosis'];

            $icd10Diagnosis = $request['icd10_diagnosis'];
            $icd10DiagnosisArray = explode(" - ", $icd10Diagnosis);
            $icd10DiagnosisCode = $icd10DiagnosisArray[0];
            $icd10DiagnosisDescription = $icd10DiagnosisArray[1];

            $exam->icd10_diagnosis_code = $icd10DiagnosisCode;
            $exam->icd10_diagnosis_description = $icd10DiagnosisDescription;

            $exam->condition_clinicalstatus_id = Config::getValue('condition_clinicalstatus_id');
            $exam->condition_category_id = Config::getValue('condition_category_id');
            $exam->encounter_status_id = Config::getValue('encounter_status_id');
            $exam->encounter_class_id = Config::getValue('encounter_class_id');
            $exam->location_uuid = Config::getValue('location_uuid');

            $exam->period_start = $request['period_start'];
            $exam->period_end = $request['period_end'];

            $exam->planning = $request['planning'];
            $exam->monitoring = $request['monitoring'];
            $exam->next_appointment = $request['next_appointment'];

            $exam->usg_fetal_weight_estimate = $request['usg_fetal_weight_estimate'];
            $exam->usg_biometry = $request['usg_biometry'];
            $exam->usg_fetal_heart_beat = $request['usg_fetal_heart_beat'];
            $exam->usg_amnion = $request['usg_amnion'];
            $exam->usg_placenta = $request['usg_placenta'];
            $exam->usg_fetal_movement = $request['usg_fetal_movement'];
            $exam->usg_fetal_survey = $request['usg_fetal_survey'];
            $exam->usg_screening = $request['usg_screening'];
            $exam->usg_others = $request['usg_others'];

            $configs = ConfigService::getAllConfig();
            foreach ($configs as $key => $value) {
                switch ($key) {
                    case "condition_clinicalstatus_id":
                        $exam->$key = $value;
                        break;
                    case "condition_category_id":
                        $exam->$key = $value;
                        break;
                    case "encounter_status_id":
                        $exam->$key = $value;
                    case "encounter_class_id":
                        $exam->$key = $value;
                        break;
                    case "encounter_location_uuid":
                        $exam->location_uuid = $value;
                        break;
                }
            }


            $exam->save();



            // save second diagnosis
            $secondaryDiagnosisArr = [];

            foreach ($request['secondary_diagnosis'] as $item) {
                $item['exam_id'] = $exam->id;
                $secondaryDiagnosisArr[] = Arr::only($item, ['id', 'exam_id', 'diagnosis']);
            }
            $exam->secondaryDiagnosis()->whereNotIn('id', Arr::pluck($secondaryDiagnosisArr, 'id'))->delete();
            Models\SecondaryDiagnosis::upsert($secondaryDiagnosisArr, ['id']);


            $encounterStatusHistoryArr = [];
            foreach ($request['encounter_status_history'] as $item) {
                $item['exam_id'] = $exam->id;
                $encounterStatusHistoryArr[] = Arr::only($item, ['id', 'exam_id', 'start', 'end', 'encounter_status_id']);

                DB::table('encounter_status_history')->updateOrInsert(
                    ['exam_id' => $item['exam_id'], 'encounter_status_id' => $item['encounter_status_id']],
                    ['start' => $item['start'], 'end' => $item['end']],
                );
            }


            // $exam->encounterStatusHistory()->whereNotIn('id',Arr::pluck($encounterStatusHistoryArr, 'id'))->delete();

            // Models\EncounterStatusHistory::upsert($encounterStatusHistoryArr, uniqueBy: ['exam_id']);



        } catch (\Exception $e) {
            throw $e;
        }
        return $exam;
    }

    public static function uploadUsgFile($request, $id)
    {
        // file upload
        if ($request->hasFile('usg_file')) {

            // remove the previous file first
            $usgFile = Models\UsgFile::where('exam_id', $id)->first();
            if ($usgFile) {
                Storage::disk('public')->delete($usgFile->url);
            }

            // upload the new file
            $uploadedFile = $request->file('usg_file');
            $filename = time() . $uploadedFile->getClientOriginalName();
            Storage::disk('public')->putFileAs(
                'usg_files/',
                $uploadedFile,
                $filename
            );

            // save the filename in database
            $usgFile = Models\UsgFile::firstOrNew(['exam_id' => $id]);
            $usgFile->url = 'usg_files/' . $filename;
            $usgFile->save();
        }

        return $usgFile;
    }

    public static function uploadSupportFile($request, $id)
    {
        // file upload
        if ($request->hasFile('support_file')) {

            // remove the previous file first
            // $usgFile = Models\UsgFile::where('exam_id',$id)->first();
            // if($usgFile) {
            //     Storage::disk('public')->delete($usgFile->url);
            // }

            // upload the new file
            $uploadedFile = $request->file('support_file');
            $filename = time() . $uploadedFile->getClientOriginalName();
            Storage::disk('public')->putFileAs(
                'support_files/',
                $uploadedFile,
                $filename
            );

            // save the filename in database
            $supportFile = new Models\ExamSupportFile(['exam_id' => $id]);
            $supportFile->url = 'support_files/' . $filename;
            $supportFile->save();
        }

        return $supportFile;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $exam = Models\Exam::find($id);
        if ($exam) {
            return $exam->delete();
        }
    }

    public static function generateNumber()
    {
        $existingData = Models\Exam::withTrashed()
            ->where('exam_date', Carbon::now()->format('Y-m-d'))
            ->whereNotNull('number')
            ->orderBy('number', 'DESC')
            ->first();
        if ($existingData) {
            $lastNumber = intval($existingData->number);
            $number = str_pad($lastNumber + 1, 3, '0', STR_PAD_LEFT);
        } else {
            $number = str_pad(1, 3, '0', STR_PAD_LEFT);
        }
        return $number;
    }

    public static function downloadReport($id)
    {
        $exam = Models\Exam::find($id);
        return Helpers\PdfHelper::generatePDF('reports.exam_report', compact('exam'), 'RM ' . $exam->exam_date . '-' . $exam->patient->mr_number . '.pdf');
    }

    public static function downloadInvoice($id)
    {
        // return Helpers\PdfHelper::generatePDF('print.exam_invoice');
        $exam = Models\Exam::find($id);
        $exam->invoice->print_count++;
        $exam->invoice->printed_at = Carbon::now();
        $exam->invoice->save();

        return view('print.exam_invoice', compact('exam'));
    }

    public static function saveInvoice($examId, $request)
    {

        $exam = Models\Exam::find($examId);
        if (!$exam->invoice_id) {
            $code = self::generateInvoiceCode();
            $invoice = new Models\Invoice();
        } else {
            $code = $exam->invoice_id;
            $invoice = $exam->invoice;
        }
        $invoice->billed_at = $request['billed_at'];
        $invoice->billed_to = $request['billed_to'];
        $invoice->grand_total = $request['grand_total'];
        $invoice->paid_at = $request['paid_at'];
        $invoice->id = $code;
        $invoice->save();

        $detailIds = collect($request['invoice_treatment'])->pluck('id')->filter(function ($value) {
            return !empty($value);
        })->all();
        $invoice->invoiceTreatment()->whereNotIn('id', $detailIds)->delete();

        $invoiceTreatments = [];
        foreach ($request['invoice_treatment'] as $item) {
            $invoiceTreatment = [
                'id' => isset($item['id']) ? $item['id'] : null,
                'invoice_id' => $code,
                'description' => $item['description'],
                'subtotal' => $item['subtotal'],
                'updated_at' => Carbon::now(),
            ];
            if (!isset($item['created_at'])) $invoiceTreatment['created_at'] = Carbon::now();
            $invoiceTreatments[] = $invoiceTreatment;
        }
        $invoice->invoiceTreatment()->upsert($invoiceTreatments, 'id');


        $detailIds = collect($request['invoice_medication'])->pluck('id')->filter(function ($value) {
            return !empty($value);
        })->all();
        $invoice->invoiceMedication()->whereNotIn('id', $detailIds)->delete();

        $invoiceMedications = [];
        foreach ($request['invoice_medication'] as $item) {
            $invoiceMedication = [
                'id' => isset($item['id']) ? $item['id'] : null,
                'invoice_id' => $code,
                'description' => $item['description'],
                'qty' => $item['qty'],
                'uom' => $item['uom'],
                'unit_price' => $item['unit_price'],
                'subtotal' => $item['subtotal'],
                'updated_at' => Carbon::now(),
            ];
            if (!isset($item['created_at'])) $invoiceMedication['created_at'] = Carbon::now();
            $invoiceMedications[] = $invoiceMedication;
        }

        $invoice->invoiceMedication()->upsert($invoiceMedications, 'id');

        $exam->invoice_id = $code;
        $exam->save();

        return $exam;
    }

    public static function generateInvoiceCode()
    {
        $part1 = 'INV';
        $part2 = Carbon::now()->year;
        $part3 = str_pad(Carbon::now()->month, 2, '0', STR_PAD_LEFT);

        $substr = "$part1/$part2/$part3/";

        $existingData = Models\Invoice::withTrashed()
            ->where('id', 'LIKE', '%' . $substr . '%')
            ->orderBy('id', 'DESC')
            ->first();
        if ($existingData) {
            $lastNumber = intval(substr($existingData->id, strlen($substr)));
            $part4 = str_pad($lastNumber + 1, 4, '0', STR_PAD_LEFT);
        } else {
            $part4 = str_pad(1, 4, '0', STR_PAD_LEFT);
        }
        $code = $substr . $part4;
        return $code;
    }

    public static function updateSatusehatIntegrationDateTime($id)
    {
        $exam = Exam::find($id);
        $exam->satusehat_integration_datetime = now();
        $exam->save();
    }

    public static function sendExamDataToSatusehat()
    {
        $exams = Exam::whereNull('satusehat_integration_datetime')->get();
        foreach ($exams as $exam) {
            $data = [
                'patient_id' => $exam->patient_id,
                'exam_date' => $exam->exam_date,
                'status' => $exam->status,
                'blood_pressure' => $exam->blood_pressure,
                'heart_rate' => $exam->heart_rate,
                'body_temperature' => $exam->body_temperature,
                'respiratory_rate' => $exam->respiratory_rate,
                'fall_history' => $exam->fall_history,
                'gestational_age' => $exam->gestational_age,
                'height' => $exam->height,
                'weight' => $exam->weight,
            ];

            $practitionerUuid = Config::getValue('practitioner_uuid');
            $encounterUuid = Str::uuid()->toString();
            $conditionUuid = Str::uuid()->toString();
        }
    }
}
