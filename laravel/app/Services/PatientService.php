<?php

namespace App\Services;

use App\Models;
// use App\Models\Patient;
use App\Models\Patient;
use App\Services\TokenService;
use App\Models\Exam;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
// use Illuminate\Support\Facades\Config;
use App\Models\Config;
use Ramsey\Uuid\Uuid;

class PatientService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Patient::select([
            'patient.*'
        ]);

        $rows = $rows->get();

        if (isset($request['search_text']) && !empty($request['search_text'])) {
            $rows = $rows->filter(function ($item) use ($request) {
                return false !== stristr($item->mr_number, $request['search_text'])
                    || false !== stristr($item->name, $request['search_text'])
                    || false !== stristr($item->address, $request['search_text']);
                //   $rows->where('mr_number','LIKE','%'.$request['search_text'].'%')
                //     ->orWhere('name','LIKE','%'.$request['search_text'].'%')
                //     ->orWhere('address','LIKE','%'.$request['search_text'].'%');
            });
        }

        if (isset($request['search_date']) && !empty($request['search_date'])) {
            $rows = $rows->filter(function ($item) use ($request) {
                return false !== ($request['search_date'] == $item->birth_date);
                //   $rows->where('mr_number','LIKE','%'.$request['search_text'].'%')
                //     ->orWhere('name','LIKE','%'.$request['search_text'].'%')
                //     ->orWhere('address','LIKE','%'.$request['search_text'].'%');
            });
        }


        if (isset($request['sort']) && !empty($request['sort'])) {
            $rows = $rows->sortBy($request['sort'], SORT_REGULAR, $request['order'] == 'desc');
        } else {
            $rows = $rows->sortBy('patient.id', SORT_REGULAR);
        }


        if (isset($request['per_page']) && !empty($request['per_page'])) {

            $rows = $rows->values();

            $totalData = count($rows);
            $perPage = $request['per_page'];
            $currentPage = $request['page'];
            $startingData = ($currentPage * $perPage) - $perPage;
            $rows = array_slice($rows->toArray(), $startingData, $perPage, true);

            $rows = new LengthAwarePaginator($rows, $totalData, $perPage, $currentPage, [
                'path' => $request->url(),
                'query' => $request->query(),
            ]);
        } else {
            $rows = $rows->values();
        }

        //   if (isset($request['perPage']) && !empty($request['perPage'])) {
        //       $rows = $rows->paginate($request['perPage']);
        //   }

        // $rows = $rows->with('exam');

        return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getByCode($code)
    {
        $patient = Models\Patient::with('diseaseHistory', 'familyDiseaseHistory', 'obstetry')->where('mr_number', $code)->firstOrFail();
        return $patient;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $patient = new Models\Patient;
            $patient->mr_number = self::generateNumber();

            $patient->nik = $request['nik'];
            // $patient->passport = $request['passport'];
            $patient->name = $request['name'];
            $patient->birth_date = $request['birth_date'];
            $patient->spouse_name = $request['spouse_name'];
            $patient->address = $request['address'];
            $patient->phone = $request['phone'];
            $patient->blood_type = $request['blood_type'];
            $patient->spouse_blood_type = $request['spouse_blood_type'];
            $patient->kb_history = $request['kb_history'];
            $patient->medicine_allergy = $request['medicine_allergy'];
            $patient->save();

            $patient->diseaseHistory()->sync($request['disease_history']);
            $patient->familyDiseaseHistory()->sync($request['family_disease_history']);
        } catch (\Exception $e) {
            throw $e;
        }
        //run integration to satusehat
        self::integratePatient($patient);
        return $patient->load('diseaseHistory', 'familyDiseaseHistory');
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $patient = Models\Patient::find($id);

            $patient->nik = $request['nik'];
            $patient->name = $request['name'];
            $patient->birth_date = $request['birth_date'];
            $patient->spouse_name = $request['spouse_name'];
            $patient->address = $request['address'];
            $patient->phone = $request['phone'];
            $patient->blood_type = $request['blood_type'];
            $patient->spouse_blood_type = $request['spouse_blood_type'];
            $patient->kb_history = $request['kb_history'];
            $patient->medicine_allergy = $request['medicine_allergy'];
            $patient->save();

            $patient->diseaseHistory()->sync($request['disease_history']);
            $patient->familyDiseaseHistory()->sync($request['family_disease_history']);
        } catch (\Exception $e) {
            throw $e;
        }
        return $patient->load('diseaseHistory', 'familyDiseaseHistory');
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $patient = Models\Patient::find($id);
        if ($patient) {
            return $patient->delete();
        }
    }

    public static function generateNumber()
    {
        // $part1 = 'P'; // replace this depending on the type of document
        // $part2 = substr(Carbon::now()->year,2);
        // $part3 = str_pad(Carbon::now()->month,2,'0',STR_PAD_LEFT);

        // $substr = $part1 . $part2 . $part3;
        // $substr = $part1;

        $existingData = Models\Patient::withTrashed()
            // ->where('mr_number','LIKE','%'.$substr.'%')
            ->orderBy('mr_number', 'DESC')
            ->first();
        if ($existingData) {
            $lastNumber = intval($existingData->mr_number);
            $number = str_pad($lastNumber + 1, 8, '0', STR_PAD_LEFT);
        } else {
            $number = str_pad(1, 8, '0', STR_PAD_LEFT);
        }
        return $number;
    }

    public static function updateSatusehatIntegrationDateTime($id)
    {
        $patient = Patient::find($id);
        $patient->satusehat_integration_datetime = now();
        $patient->save();
    }

    public static function checkNIK($nik)
    {
        $patient = Patient::where('nik', $nik)->first();
        if (isset($patient))
            return true;
        else
            return false;
    }

    public static function integrateAllPatient()
    {
        ini_set('max_execution_time', 180); //3 minutes

        $patients = Patient::whereNull('satusehat_integration_datetime')->whereNotNull('nik')->get();
        // $token = tokenService::getSolamedicToken();
        foreach ($patients->chunk(5) as $patientChunk) {
            foreach ($patientChunk as $patient) {
                // kirim data ke solamedic pakai function integratePatient
                self::integratePatient($patient);
            }
        }
    }
    public static function integratePatient($patient)
    {

        $token = TokenService::getSolamedicToken();

        // kirim data ke solamedic
        echo $patient->nik . "|"   . $patient->name . "|" . $patient->birth_date . "|" . $patient->mr_number . "| ";

        try {
            //check if nik under 16 character
            // if (strlen($patient->nik) < 16) {
            //     $patient->nik = str_pad($patient->nik, 16, '0', STR_PAD_LEFT);
            // }
            // get token from token service
            $patientData = [
                'nik' => $patient->nik,
                'name' => $patient->name,
                'birthdate' => $patient->birth_date,
                'gender' => "female",
                'active' => true,
                'is_newborn' => false,
                'mr_number' => $patient->mr_number,
            ];
            // dd($patientData);

            $response = Http::withToken($token)->post(env('MIX_SATUSEHATMASTER_API_URL') . '/patient/register-patient', $patientData);
            log::info(env('MIX_SATUSEHATMASTER_API_URL') . '/patient/register-patient');
            log::info($patientData);
            if ($response->status() == 200 && $response->json()['status'] != "error") {
                $patient->satusehat_integration_datetime = now();
                $patient->save();
            }
            Log::info($response);
            echo " - " . $response->json()['message'];
            echo "<br>";
        } catch (\Exception $e) {
            // Handle the exception, you can log it or take any other action
            Log::error('Failed to integrate patient: ' . $patient->name . " with error - " . $e->getMessage());
        }
    }

    public static function integrateAllExam()
    {
        ini_set('max_execution_time', 180); //3 minutes

        $exams = Exam::whereNull('satusehat_integration_datetime')->where('exam_date', '>=', now()->subDays(6))->where('status', 'COMPLETE')->get();
        $examcount = $exams->count();
        Log::info("Total exams to integrate: " . $examcount);
        // $token = tokenService::getSolamedicToken();
        foreach ($exams->chunk(5) as $examChunk) {
            foreach ($examChunk as $exam) {
                // kirim data ke solamedic pakai function integratePatient
                self::integrateExam($exam);
            }
        }
    }

    //function to integrateExam
    public static function integrateExam($exam)
    {
        $token = TokenService::getSolamedicToken();
        //get practtioner ihs from config
        log::info($token);
        $practitionerIhs = Config::getValue('practitioner_ihs_number');

        $encounterUUid = Uuid::uuid4()->toString();
        $conditionUUid = Uuid::uuid4()->toString();
        // kirim data ke solamedic
        echo "ID = " . $exam->id ."| Date =  ". $exam->exam_date . " | Nik =" . $exam->patient->nik . "|Name = "   . $exam->patient->name . "| Dob = " . $exam->patient->birth_date . "|Mr.Number = " . $exam->patient->mr_number . "| location" . $exam->location_uuid . "\n";
        log::info("ID = " . $exam->id . "| Date =  " . $exam->exam_date . " | Nik =" . $exam->patient->nik . "|Name = "   . $exam->patient->name . "| Dob = " . $exam->patient->birth_date . "|Mr.Number = " . $exam->patient->mr_number );


        try {
            $examData = [
                'entries' => [
                    [
                        "resource_type" => "Encounter",
                        "uuid" => $encounterUUid,
                        "period" => [
                            "start" => $exam->period_start,
                            "end" => $exam->period_end,
                        ],
                        "encounter_class_id" => Config::getValue('encounter_class_id')->value, // Example value
                        "encounter_status_id" => Config::getValue('encounter_status_id')->value, // Example value
                        "patient_mr_number" => $exam->patient->mr_number,
                        "participant_uuid" => $practitionerIhs->value,
                        "location_uuid" => $exam->location_uuid, // Example value
                        "status_history" => $exam->encounterStatusHistory->map(function ($status) {
                            return [
                                "id" => $status->id,
                                "exam_id" => $status->exam_id,
                                "encounter_status_id" => $status->encounter_status_id,
                                "start" => $status->start,
                                "end" => $status->end,
                                "created_by" => $status->created_by,
                                "updated_by" => $status->updated_by,
                                "deleted_by" => $status->deleted_by,
                                "created_at" => $status->created_at,
                                "updated_at" => $status->updated_at,
                                "deleted_at" => $status->deleted_at,
                            ];
                        })->toArray(),
                        "diagnosises" => [
                            [
                                "encounter_diagnosis_use_id" => 2, // DD / Discharge Diagnosis
                                "encounter_diagnosis_uuid" => $conditionUUid,
                                "encounter_diagnosis_display" => $exam->icd10_diagnosis_description, // Example value
                                "encounter_diagnosis_rank" => 1,
                            ]
                        ]
                    ],
                    [
                        "resource_type" => "Condition",
                        "uuid" => $conditionUUid,
                        "condition_clinicalstatus_id" => Config::getValue('condition_clinicalstatus_id')->value, // Example value
                        "condition_category_id" =>  Config::getValue('condition_category_id')->value, // Example value
                        "icd10_code" => $exam->icd10_diagnosis_code, // Example value
                        "patient_mr_number" => $exam->patient->mr_number,
                    ]
                ],
            ];

            log::info($examData);
            $response = Http::withToken($token)->post(env('MIX_SATUSEHATMASTER_API_URL') . '/bundle/send-bundle', $examData);
            //log satusehat api url
            Log::info('Satusehat API URL: ' . env('MIX_SATUSEHATMASTER_API_URL') . '/bundle/send-bundle');
            // log::info($response);

            if ($response->status() == 200 && $response->json()['status'] == 'success') {
                $exam->satusehat_integration_datetime = now();
                $exam->save();
            }

            Log::info($response);
            // dd($response);
        } catch (\Exception $e) {
            Log::error('Failed to integrate exam: ' . $exam->id . " with error - " . $e->getMessage());
            echo "Failed to integrate exam: " . $exam->id . " with error - " . $e->getMessage();
        }
    }
}
