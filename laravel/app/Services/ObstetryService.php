<?php
namespace App\Services;

use App\Models;

class ObstetryService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Obstetry::select([
            'obstetry.*'
            ]);

            if (isset($request['search_text']) && !empty($request['search_text'])) {
                $rows = $rows->where(function ($rows) use ($request) {
                    
                });
            }

            if (isset($request['patient_id']) && !empty($request['patient_id'])) {
                $rows = $rows->where('patient_id', $request['patient_id']);
            }

            if (isset($request['sort']) && !empty($request['sort'])) {
                $sortOrder = explode('|', $request['sort']);
                $rows = $rows->orderBy($sortOrder[0], $sortOrder[1]);
            } else {
                $rows = $rows->orderBy('obstetry.id', 'asc');
            }
            
            if (isset($request['per_page']) && !empty($request['per_page'])) {
            $rows = $rows->paginate($request['per_page']);
        } else {
            $rows = $rows->get();
        }

          return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $obstetry= Models\Obstetry::find($id);
        
        return $obstetry;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $obstetry = new Models\Obstetry;
            $obstetry->patient_id = $request['patient_id'];
            $obstetry->year = $request['year'];
            $obstetry->sex = $request['sex'];
            $obstetry->birth_weight = $request['birth_weight'];
            $obstetry->birth_length = $request['birth_length'];
            $obstetry->paraabortus = $request['paraabortus'];
            $obstetry->childbirth_method = $request['childbirth_method'];
            $obstetry->complication = $request['complication'];
            $obstetry->birth_place = $request['birth_place'];
            $obstetry->first_day_of_last_period = $request['first_day_of_last_period'];
            $obstetry->pregnancy_estimate = $request['pregnancy_estimate'];
            $obstetry->g = $request['g'];
            $obstetry->p = $request['p'];
            $obstetry->a = $request['a'];
            $obstetry->pregnancy_type = $request['pregnancy_type'];
            $obstetry->chroniocity = $request['chroniocity'];
            $obstetry->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $obstetry;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $obstetry = Models\Obstetry::find($id);
            $obstetry->patient_id = $request['patient_id'];
            $obstetry->year = $request['year'];
            $obstetry->sex = $request['sex'];
            $obstetry->birth_weight = $request['birth_weight'];
            $obstetry->birth_length = $request['birth_length'];
            $obstetry->paraabortus = $request['paraabortus'];
            $obstetry->childbirth_method = $request['childbirth_method'];
            $obstetry->complication = $request['complication'];
            $obstetry->birth_place = $request['birth_place'];
            $obstetry->first_day_of_last_period = $request['first_day_of_last_period'];
            $obstetry->pregnancy_estimate = $request['pregnancy_estimate'];
            $obstetry->g = $request['g'];
            $obstetry->p = $request['p'];
            $obstetry->a = $request['a'];
            $obstetry->pregnancy_type = $request['pregnancy_type'];
            $obstetry->chroniocity = $request['chroniocity'];
            $obstetry->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $obstetry;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $obstetry = Models\Obstetry::find($id);
        if($obstetry){
            return $obstetry->delete();
        }
    }
}