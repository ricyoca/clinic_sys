<?php
namespace App\Services;

use App\Models;

class RoleService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Roles::select([
            'roles.*'
          ]);

          if (isset($request['search_text']) && !empty($request['search_text'])) {
              $rows = $rows->where(function ($rows) use ($request) {
                  
              });
          }
  
          if (isset($request['sort']) && !empty($request['sort'])) {
              $sortOrder = explode('|', $request['sort']);
              $rows = $rows->orderBy($sortOrder[0], $sortOrder[1]);
          } else {
              $rows = $rows->orderBy('roles.id', 'asc');
          }
          
          if (isset($request['per_page']) && !empty($request['per_page'])) {
              $rows = $rows->paginate($request['per_page']);
          } else {
              $rows = $rows->get();
          }

          return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $role= Models\Roles::find($id);
        return $role;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $role = new Models\Roles;
            // attributes here
            $role->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $role;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $role = Models\Roles::find($id);
            // attributes here
            $role->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $role;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $role = Models\Roles::find($id);
        if($role){
            return $role->delete();
        }
    }
}