<?php
namespace App\Services;

use App\Models;

class IcdService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Icd::select([
            'icd.*'
          ]);

          if (isset($request['search_text']) && !empty($request['search_text'])) {
              $rows = $rows->where(function ($rows) use ($request) {
                  $rows->where('code','LIKE','%'.$request['search_text'].'%')
                  ->orWhere('diagnosis','LIKE','%'.$request['search_text'].'%');
              });
          }
  
          if (isset($request['sort']) && !empty($request['sort'])) {
              $sortOrder = explode('|', $request['sort']);
              $rows = $rows->orderBy($sortOrder[0], $sortOrder[1]);
          } else {
              $rows = $rows->orderBy('icd.id', 'asc');
          }
          
          if (isset($request['per_page']) && !empty($request['per_page'])) {
              $rows = $rows->paginate($request['per_page']);
          } else {
              $rows = $rows->get();
          }

          return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $icd= Models\Icd::find($id);
        return $icd;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $icd = new Models\Icd;
            // attributes here
            $icd->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $icd;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $icd = Models\Icd::find($id);
            // attributes here
            $icd->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $icd;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $icd = Models\Icd::find($id);
        if($icd){
            return $icd->delete();
        }
    }
}