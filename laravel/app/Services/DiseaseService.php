<?php
namespace App\Services;

use App\Models;

class DiseaseService
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll($request = [])
    {
        $rows = Models\Disease::select([
            'disease.*'
          ]);

          if (isset($request['search_text']) && !empty($request['search_text'])) {
              $rows = $rows->where(function ($rows) use ($request) {
                $rows->where('name','LIKE','%'.$request['search_text'].'%');
              });
          }
  
          if (isset($request['sort']) && !empty($request['sort'])) {
              $sortOrder = explode('|', $request['sort']);
              $rows = $rows->orderBy($sortOrder[0], $sortOrder[1]);
          } else {
              $rows = $rows->orderBy('disease.id', 'asc');
          }

          // sort order
        // if (isset($request['sort']) && !empty($request['sort'])) {
        //     $sortOrder = explode('|', $request['sort']);
        //     $data = $data->orderBy($sortOrder[0], $sortOrder[1]);
        // } else {
        //     $data = $data->orderBy('id', 'desc');
        // }
        
          if (isset($request['per_page']) && !empty($request['per_page'])) {
              $rows = $rows->paginate($request['per_page']);
          } else {
              $rows = $rows->get();
          }

          return $rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById($id)
    {
        $disease= Models\Disease::find($id);
        return $disease;
    }
    /**
     * Attempt to create data
     */
    public static function store($request)
    {
        try {
            $disease = new Models\Disease;
            $disease->name = $request->name;
            $disease->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $disease;
    }

    /**
     * Attempt to update data
     */
    public static function update($request, $id)
    {
        try {
            $disease = Models\Disease::find($id);
            $disease->name = $request->name;
            $disease->save();
        } catch (\Exception $e) {
            throw $e;
        }
        return $disease;
    }

    /**
     * Attempt to delete data
     */

    public static function delete($id)
    {
        $disease = Models\Disease::find($id);
        if($disease){
            return $disease->delete();
        }
    }
}