<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use DB;

class DiseaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            
            $rows = Services\DiseaseService::getAll($request);

            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        try {
            DB::beginTransaction();
            // call service here
            $result = Services\DiseaseService::store($request);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $disease = Services\DiseaseService::getById($id);
        return response()->json($disease);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $disease = Services\DiseaseService::getById($id);
        return response()->json($disease);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
        ]);

        try {
            DB::beginTransaction();
            Services\DiseaseService::update($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Services\DiseaseService::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( ['status' => 'success'] );
    }
}
