<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models;
use App\Services;

class UsersController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            
            $rows = Services\UserService::getAll($request);
            
            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create(Request $request)
    {
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:1|max:128',
            'username' => 'required|max:256',
            'status' => 'required',
            'password' => 'required|min:8|max:16|confirmed',
        ]);

        try {
            DB::beginTransaction();
            $result = Services\UserService::store($request);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $you = auth()->user()->id;
        $user = Services\UserService::getById($id);
        return response()->json( compact('user','you') );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $you = auth()->user()->id;
        $user = Services\UserService::getById($id);
        return response()->json( compact('user','you') );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name'       => 'required|min:1|max:256',
            'username'      => 'required|max:256',
            'status'      => 'required',
        ]);
        try {
            DB::beginTransaction();
            $result = Services\UserService::update($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        //$request->session()->flash('message', 'Successfully updated user');
        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Services\UserService::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( ['status' => 'success'] );
    }

    public function changePassword($id, Request $request)
    {
        $request->validate([
            'old_password' => 'required|min:6',
            'new_password' => 'required|min:6|confirmed',
        ]);

        try {
            DB::beginTransaction();
            $result = Services\UserService::changePassword($id, $request);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        return response()->json(['status' => 'success']);
    }
}
