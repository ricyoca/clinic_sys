<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use DB;
use Illuminate\Support\Facades\Validator;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'search_text' => 'required_without:search_date',

        ]);

        if ($validator->fails()) {
            return response()->json($validator->messages(), 500);
        }

        try {
            $rows = Services\PatientService::getAll($request);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
        return response()->json($rows);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json(['status' => 'success']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'birth_date' => 'required',
            'spouse_name' => 'required',
            'address' => 'required',
            'phone' => 'required',

        ]);

        try {
            DB::beginTransaction();
            // call service here
            $result = Services\PatientService::store($request);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return response()->json($result);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($code)
    {
        try {
            $patient = Services\PatientService::getByCode($code);
        } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e) {
            return response()->json(['status' => 'error', 'message' => 'Data not found.'], 500);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
        return response()->json($patient);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patient = Services\PatientService::getById($id);
        return response()->json($patient);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([]);

        try {
            DB::beginTransaction();
            $result = Services\PatientService::update($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return response()->json($result);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Services\PatientService::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success']);
    }

    /**
     * Update satusehat_integration_datetime column in patient table
     * 
     */
    public function updateSatusehatIntegrationDateTime($id)
    {
        try {
            DB::beginTransaction();
            Services\PatientService::updateSatusehatIntegrationDateTime($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }

        return response()->json(['status' => 'success']);
    }

    /**
     * Check if NIK already exist in database
     * 
     */
    public function checkNIK($nik)
    {
        try {
            $response = Services\PatientService::checkNIK($nik);
            return response()->json(['status' => 'success', 'patient_exist' => $response]);
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }

    //function to integrateAllPatient

    public function integrateAllPatient()
    {
        // dd("hola");
        // echo "hola";
        try {

            Services\PatientService::integrateAllPatient();
        } catch (\Exception $e) {

            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }

    //function to integrateExam
    public function integrateAllExam()
    {
        try {
            Services\PatientService::integrateAllExam();
        } catch (\Exception $e) {
            return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
        }
    }
}
