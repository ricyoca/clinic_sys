<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;

class ConfigController extends Controller
{
    public function getAllConfig()
    {
        try {
            $rows = Services\ConfigService::getAllConfig();

            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    
    public function getValue(Request $request)
    {
        try {
            $rows = Services\ConfigService::getValue($request);

            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function setValue(Request $request)
    {
        try {
            $rows = Services\ConfigService::setValue($request);

            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    public function massSetValue(Request $request)
    {
        try {
            $rows = Services\ConfigService::massSetValue($request);

            return response()->json( 'success' );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
}
