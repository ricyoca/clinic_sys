<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            
            $rows = Services\RoleService::getAll($request);

            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            
        ]);

        try {
            DB::beginTransaction();
            // call service here
            $result = Services\RoleService::store($request);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Services\RoleService::getById($id);
        return response()->json($role);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Services\RoleService::getById($id);
        return response()->json($role);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            
        ]);

        try {
            DB::beginTransaction();
            $result = Services\RoleService::update($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Services\RoleService::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( ['status' => 'success'] );
    }
}
