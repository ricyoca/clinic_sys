<?php

namespace App\Http\Controllers;

use App\Services\TokenService;
use Illuminate\Http\Request;

class TokenController extends Controller
{
    public function saveSolamedicToken(Request $request)
    {
        try {
            $response = TokenService::saveSolamedicToken($request);
            return $response;
        } catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }

    public function getSolamedicToken()
    {
        try {
            $response = TokenService::getSolamedicToken();
            return $response;
        } catch (\Exception $e)
        {
            return $e->getMessage();
        }
    }
}
