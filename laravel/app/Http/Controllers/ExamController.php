<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use DB;

class ExamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            
            $rows = Services\ExamService::getAll($request);

            return response()->json( $rows );
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'blood_pressure' => 'required'
        ]);

        try {
            DB::beginTransaction();
            // call service here
            $result = Services\ExamService::store($request);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $exam = Services\ExamService::getById($id);
        return response()->json($exam);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $exam = Services\ExamService::getById($id);
        return response()->json($exam);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'icd10_diagnosis' => 'required',
            
        ]);

        try {
            DB::beginTransaction();
            $result = Services\ExamService::update($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    public function uploadUsgFile(Request $request, $id)
    {
        $request->validate([
            
        ]);
        
        try {
            DB::beginTransaction();
            $result = Services\ExamService::uploadUsgFile($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    public function uploadSupportFile(Request $request, $id)
    {
        $request->validate([
            
        ]);
        
        try {
            DB::beginTransaction();
            $result = Services\ExamService::uploadSupportFile($request, $id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            Services\ExamService::delete($id);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( ['status' => 'success'] );
    }

    public function downloadReport($id)
    {
        $file = Services\ExamService::downloadReport($id);
        return $file;
    }

    public function downloadInvoice($id) {
        $file = Services\ExamService::downloadInvoice($id);
        return $file;
    }

    public function saveInvoice($id, Request $request) {
        $request->validate([
            
        ]);

        try {
            DB::beginTransaction();
            // call service here
            $result = Services\ExamService::saveInvoice($id, $request);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }

    public function updateSatusehatIntegrationDateTime($id)
    {
        try {
            DB::beginTransaction();
            $result = Services\ExamService::updateSatusehatIntegrationDateTime($id);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => $e->getMessage()],500 );
        }
        
        return response()->json( $result );
    }
}
