<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamSupportFile extends Model
{
    protected $table = 'exam_support_file';
    protected $guarded = [];
}
