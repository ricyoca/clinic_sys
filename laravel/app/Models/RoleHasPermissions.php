<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleHasPermissions extends Model
{
    protected $table = 'role_has_permissions';
    
    public function permission()
    {
        return $this->belongsTo('App\Models\Permission','permission_id');
    }
    public function role()
    {
        return $this->belongsTo(App\Models\Roles::class,'role_id');
    }
}
