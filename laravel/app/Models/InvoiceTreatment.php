<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceTreatment extends Model
{
    use SoftDeletes;

    protected $table = 'invoice_treatment';
    protected $guarded = [];

}
