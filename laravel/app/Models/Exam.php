<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Exam extends Model
{
    use SoftDeletes;
    use Traits\Signature;

    protected $table = 'exam';

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id','id');
    }

    public function secondaryDiagnosis()
    {
        return $this->hasMany(SecondaryDiagnosis::class,'exam_id','id');
    }

    public function encounterStatusHistory()
    {
        return $this->hasMany(EncounterStatusHistory::class,'exam_id','id');
    }

    public function usgFile()
    {
        return $this->hasOne(UsgFile::class,'exam_id','id');
    }

    public function supportFile()
    {
        return $this->hasMany(ExamSupportFile::class,'exam_id','id');
    }

    public function invoice() {
        return $this->hasOne(Invoice::class, 'id', 'invoice_id');
    }
}
