<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes;
    use Traits\Signature;

    protected $table = 'appointment';

    public function patient()
    {
        return $this->belongsTo(Patient::class, 'patient_id','id');
    }

}
