<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceMedication extends Model
{
    use SoftDeletes;

    protected $table = 'invoice_medication';
    protected $guarded = [];

}
