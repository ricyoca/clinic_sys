<?php
namespace App\Models\Traits;

trait Signature {

  protected static function boot() {

    parent::boot();

    static::creating(function($item) {
          if (auth('api')) {
              $item->created_by = auth('api')->id();
          }
        // Event::fire('item.created', $item);

      });
      
      static::saving(function($item) {
          
          if (auth('api')) {
              $item->updated_by = auth('api')->id();
          }
        // Event::fire('item.created', $item);

    });

    static::updating(function($item) {
          // dd(auth('api')->id());
          if (auth('api')) {
              $item->updated_by = auth('api')->id();
          }
        // Event::fire('item.updated', $item);

    });

    static::deleting(function($item) {
          // dd(auth('api')->id());
          if (auth('api')) {
              $item->deleted_by = auth('api')->id();
              $item->save();
          }

    });
  }
  
  public function createdBy()
  {
      if(!$this->attributes['created_by']) {
          return;
      }
      return $this->belongsTo(User::class,'created_by','id');
  }

  public function updatedBy()
  {
      if(!$this->attributes['updated_by']) {
          return;
      }
      return $this->belongsTo(User::class,'updated_by','id');
  }
  public function deletedBy()
  {
      if(!$this->attributes['deleted_by']) {
          return;
      }
      return $this->belongsTo(User::class,'deleted_by','id');
  }
}