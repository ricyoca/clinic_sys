<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Roles extends Role
{
    protected $table = 'roles';

    public function roleHasPermission()
    {
        return $this->hasMany('App\Models\RoleHasPermission','role_id','id');
    }
    public function roleHierarchy()
    {
        return $this->hasMany('App\Models\RoleHierarchy','role_id','id');
    }
}
