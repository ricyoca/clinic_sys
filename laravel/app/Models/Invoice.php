<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;

    protected $table = 'invoice';
    protected $keyType = 'string';
    public $incrementing = false;

    protected $dates = ['billed_at', 'printed_at'];

    protected $guarded = [];

    public function exam() {
        return $this->belongsTo(Exam::class, 'id','invoice_id');
    }

    public function invoiceMedication() {
        return $this->hasMany(InvoiceMedication::class, 'invoice_id', 'id');
    }

    public function invoiceTreatment() {
        return $this->hasMany(InvoiceTreatment::class, 'invoice_id', 'id');
    }
}
