<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Permissions extends Model
{
    use Traits\Signature;
    protected $table = 'permissions';

    public function modelHasPermission()
    {
        return $this->hasMany('App\Models\ModelHasPermission', 'permission_id','id');
    }
    
    public function roleHasPermission()
    {
        return $this->hasMany('App\Models\RoleHasPermission','permission_id','id');
    }
}
