<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelHasRoles extends Model
{
    protected $table = 'model_has_roles';

    public function users()
    {
        return $this->belongsTo(App\Models\User::class, 'users_id');
    }
    public function status()
    {
        return $this->belongsTo('App\Models\Status', 'status_id');
    }
}
