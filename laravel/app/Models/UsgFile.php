<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UsgFile extends Model
{
    protected $table = 'usg_file';
    protected $guarded = [];
}
