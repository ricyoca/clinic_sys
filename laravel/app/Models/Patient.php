<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Magros\Encryptable\Encryptable;

class Patient extends Model
{
    use Traits\Signature;
    use SoftDeletes;
    use Encryptable;
    
    protected $table = 'patient';
    protected $encryptable = ['name','address','spouse_name','birth_date'];
    protected $camelcase = ['name','address','spouse_name'];
    
    public function diseaseHistory()
    {
        return $this->belongsToMany(Disease::class, 'disease_history', 'patient_id', 'disease_id');
    }
    
    public function familyDiseaseHistory()
    {
        return $this->belongsToMany(Disease::class, 'family_disease_history', 'patient_id', 'disease_id');
    }

    public function exam()
    {
        return $this->hasMany(Exam::class,'patient_id','id');
    }

    public function obstetry()
    {
        return $this->hasMany(Obstetry::class, 'patient_id','id');
    }
    
}
