<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoleHierarchy extends Model
{
    protected $table = 'role_hierarchy';
    public $timestamps = false;
    
    public function roles()
    {
        return $this->belongsTo('App\Models\Roles','role_id');
    }
}
