<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecondaryDiagnosis extends Model
{
    use Traits\Signature;
    use SoftDeletes;
    protected $table = 'secondary_diagnosis';
}
