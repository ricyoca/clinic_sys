<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EncounterStatusHistory extends Model
{
    use Traits\Signature;
    use SoftDeletes;
    protected $table = 'encounter_status_history';
}
