<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends Model
{
    use Traits\Signature;
    use SoftDeletes;
    protected $table = 'config';

    public static function getValue($key)
    {
        return Config::where('key', $key)->first();
    }
}
