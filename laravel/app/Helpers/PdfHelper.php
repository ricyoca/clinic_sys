<?php
namespace App\Helpers;

use DB, PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PdfHelper {

  public static function generatePDF($view = 'print.test',$data = [], $filename="file.pdf", $orientation = 'portrait', $width = null, $height = null)
  {
    $pdf = PDF::loadView($view, $data);
    
    if ($width && $height) {
      // $pdf = $pdf->setPaper([0,0,$width/2.54*72,$height/2.54*72 ]);
      $pdf = $pdf->setOption('page-width',$width)
        ->setOption('page-height',$height)
        ->setOption('margin-top',0)
        ->setOption('margin-bottom',0)
        ->setOption('margin-left',0)
        ->setOption('margin-right',0);
    } else {
      $pdf = $pdf->setPaper('a4')
      ->setOption('margin-top',5)
      ->setOption('margin-bottom',5)
      ->setOption('margin-left',5)
      ->setOption('margin-right',5)
      ->setOrientation($orientation);
    }

    return $pdf->stream($filename);
    // return view('print.test');
  }

}