<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeGenericView extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:generic-view {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[CUSTOM] Create scaffolded Vue files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $words = implode(" ",preg_split('/(?=[A-Z])/',$this->argument('name')));
        $titleCase = Str::title($words);
        $camelCase = ucfirst(Str::camel($words));
        $slug = Str::slug(strtolower($words));
        $snakeCase = Str::snake(strtolower($words));

        $indexFileContents = <<<EOT
<template>
<CRow>
    <CCol col="12">
    <CCard>
        <CCardHeader>
        <CCardTitle>{$titleCase}
            <div class="float-right">
            <CButton color="primary" @click="createData()"><i class="fas fa-plus"></i> Create {$titleCase}</CButton>
            </div>
        </CCardTitle>
        </CCardHeader>
        <CCardBody>
        <CRow class="mb-2">
            <CCol>
            <input type="text" class="form-control" placeholder="Search..." v-model="serverParams.search_text" @keypress.enter="getData()">
            </CCol>
        </CRow>
        <vue-good-table mode="remote" :columns="fields" :totalRows="totalRows" :rows="items" styleClass="vgt-table condensed" :pagination-options="paginationOptions" @on-page-change="onPageChange" @on-sort-change="onSortChange" @on-column-filter="onColumnFilter" @on-per-page-change="onPerPageChange">
            <template slot="table-row" slot-scope="props">
            <span v-if="props.column.field == 'show'">
                <CButton color="primary" size="sm" @click="showRow( props.row.id )"><i class="fas fa-eye"></i> </CButton>
            </span>
            <span v-else-if="props.column.field == 'edit'">
                <CButton color="primary" size="sm" @click="editRow( props.row.id )"><i class="fas fa-edit"></i> </CButton>
            </span>
            <span v-else-if="props.column.field == 'delete'">
                <CButton color="danger" size="sm" @click="deleteRow( props.row.id )"><i class="fas fa-trash"></i> </CButton>
            </span>
            <!-- add slots here -->
            <span v-else>
                {{props.formattedRow[props.column.field]}}
            </span>
            </template>
        </vue-good-table>
        </CCardBody>
    </CCard>
    </CCol>
</CRow>
</template>

<script>
import axios from 'axios'

export default {
    data () {
    return {
        fields: [
        // add more fields here
        {
            field: 'show',
            label: 'Show',
            sortable: false,
            width: '75px',
            thClass: 'text-center',
            tdClass: 'text-center',
        },
        {
            field: 'edit',
            label: 'Edit',
            sortable: false,
            width: '75px',
            thClass: 'text-center',
            tdClass: 'text-center',
        },
        {
            field: 'delete',
            label: 'Delete',
            sortable: false,
            width: '75px',
            thClass: 'text-center',
            tdClass: 'text-center',
        }
        ],
        paginationOptions: {
        enabled: true,
        mode: 'records',
        perPage: 10,
        position: 'bottom',
        perPageDropdown: [5, 10, 20],
        dropdownAllowAll: false,
        setCurrentPage: 1,
        nextLabel: 'next',
        prevLabel: 'prev',
        rowsPerPageLabel: 'Rows per page',
        ofLabel: 'of',
        pageLabel: 'page', // for 'pages' mode
        allLabel: 'All',
        },
        serverParams: {
        columnFilters: {},
        sort: '',
        order: '',
        page: 1,
        perPage: 10,
        search_text: null
        },
        items: [],
        totalRows: 0,
    }
    },
    paginationProps: {
    align: 'center',
    doubleArrows: false,
    previousButtonHtml: 'prev',
    nextButtonHtml: 'next'
    },
    methods: {
    updateParams(newProps) {
        this.serverParams = Object.assign({}, this.serverParams, newProps);
    },
    onPageChange(params) {
        this.updateParams({
        page: params.currentPage
        });
        this.getData();
    },
    onPerPageChange(params) {
        this.updateParams({
        perPage: params.currentPerPage
        });
        this.getData();
    },
    onSortChange(params) {
        this.updateParams({
        sort: params[0].field,
        order: params[0].type,
        });
        this.getData();
    },
    onColumnFilter(params) {
        this.updateParams(params);
        this.getData();
    },
    getBadge(status) {
        return status === 'ACTIVE' ? 'success' :
        status === 'INACTIVE' ? 'secondary' :
        status === 'Pending' ? 'warning' :
        status === 'Banned' ? 'danger' : 'primary'
    },
    showRow(id) {
        this.\$router.push({
        path: `{$slug}/\${id.toString()}`
        });
    },
    editRow(id) {
        this.\$router.push({
        path: `{$slug}/\${id.toString()}/edit`
        });
    },
    deleteRow(id) {
        this.\$dialog
        .confirm('Are you sure you want to delete this data?')
        .then((dialog) => {
            axios.delete('/api/{$slug}/' + id.toString())
            .then((response) => {
                this.\$notify({
                type: 'success',
                title: 'Success!',
                text: '{$titleCase} deleted successfully.',
                })
                this.getData();
            })
          .catch(error => {
                console.log(error);
            });
        })
        .catch(() => {

        });
    },
    createData() {
        this.\$router.push({
            path: '{$slug}/create'
        });
    },
    getData() {
        axios.get('/api/{$slug}', {
            params: this.serverParams
        })
        .then((response) => {
            this.items = response.data.rows.data;
            this.totalRows = response.data.rows.total;
        })
          .catch(error => {
            console.log(error);
            this.\$notify({
            type: 'error',
            title: 'Oh no!',
            text: error.response.data.message,
            })
        });
    }
    },
    mounted: function() {
    this.getData();
    }
}
</script>
EOT;
        if(\Storage::has('/coreui/src/views/'. $snakeCase .'/Index'.$titleCase.'.vue')) {
            $this->info('Component already exists!');
        } else {

            $written = \Storage::put('/coreui/src/views/'. $snakeCase .'/Index'.$camelCase.'.vue', $indexFileContents);
            if($written)
            {
                $this->info('Created new Component Index'.$camelCase.'.vue in /coreui/src/views/'. $snakeCase);
            } else {
                $this->info('Something went wrong');
            }
        }

        $createFileContents = <<<EOT
<template>
    <CRow>
        <CCol col="12" lg="6">
        <CCard>
            <CCardHeader>
              <CCardTitle class="mb-0">Create {$titleCase}</CCardTitle>
            </CCardHeader>
            <CCardBody>
            <CForm>
                <!-- fields goes here -->
                
                <CButton color="primary" :disabled="disableButton" @click="store()"><i class="fas fa-save"></i> Save</CButton>
                <CButton color="primary" :disabled="disableButton" @click="goBack">Back</CButton>
            </CForm>
            </CCardBody>
        </CCard>
        </CCol>
    </CRow>
</template>
    
<script>
import axios from 'axios'
export default {
    data () {
    return {
        form: {
            // add more form attribute here
        },
        errors: {},
        disableButton: false,
    }
    },
    methods: {
        goBack() {
            this.\$router.go(-1)
            // this.\$router.replace({path: '/{$slug}'})
        },
        store() {
            this.disableButton = true;
            this.\$notify({
                type: 'info',
                title: 'Saving...',
                duration: 3000,
            })
            axios.post('/api/{$slug}', this.form)
            .then((response) => {
                this.\$notify({
                    type: 'success',
                    title: 'Success!',
                    text: 'This data has been saved successfully.',
                })
            })
          .catch(error => {
                this.\$notify({
                    type: 'error',
                    title: 'Oh no!',
                    text: error.response.data.message,
                })
                if (error.response.status == 422) {
                    this.errors = error.response.data.errors;
                }
            }).then(() => {
                this.disableButton = false;
            });
        },
    },
    mounted: function() {
        axios.get('/api/{$slug}/create')
            .then((response) => {
                this.form = response.data;
            })
          .catch(error => {
                console.log(error);
                this.\$notify({
                    type: 'error',
                    title: 'Oh no!',
                    text: error.response.data.message,
                })
            });
    }
}
</script>
EOT;

        if(\Storage::has('/coreui/src/views/'. $snakeCase .'/Create'.$titleCase.'.vue')) {
            $this->info('Component already exists!');
        } else {

            $written = \Storage::put('/coreui/src/views/'. $snakeCase .'/Create'.$camelCase.'.vue', $createFileContents);
            if($written)
            {
                $this->info('Created new Component Create'.$camelCase.'.vue in /coreui/src/views/'. $snakeCase);
            } else {
                $this->info('Something went wrong');
            }
        }

        $editFileContents = <<<EOT
<template>
  <CRow>
    <CCol col="12" lg="6">
      <CCard>
        <CCardHeader>
          <CCardTitle class="mb-0">Edit {$titleCase}</CCardTitle>
        </CCardHeader>
        <CCardBody>
          <CForm>
            <!-- fields goes here -->
            
            <CButton color="primary" :disabled="disableButton" @click="update()"><i class="fas fa-save"></i> Save</CButton>
            <CButton color="primary" :disabled="disableButton" @click="goBack">Back</CButton>
          </CForm>
        </CCardBody>
      </CCard>
    </CCol>
  </CRow>
</template>

<script>
  import axios from 'axios'
  export default {
    data () {
      return {
        form: {
          // add more form attribute here
        },
        errors: {},
        disableButton: false,
      }
    },
    methods: {
      goBack() {
        this.\$router.go(-1)
        // this.\$router.replace({path: '/{$slug}'})
      },
      update() {
        this.disableButton = true;
        this.\$notify({
          type: 'info',
          title: 'Saving...',
          duration: 3000,
        })
        axios.put('/api/{$slug}/' + this.\$route.params.id , this.form)
          .then((response) => {
            this.\$notify({
              type: 'success',
              title: 'Success!',
              text: 'This data has been saved successfully.',
            })
          })
          .catch(error => {
            this.\$notify({
              type: 'error',
              title: 'Oh no!',
              text: error.response.data.message,
            })
            if (error.response.status == 422) {
              this.errors = error.response.data.errors;
            }
          }).then(() => {
            this.disableButton = false;
          });
      },
    },
    mounted: function() {
      axios.get('/api/{$slug}/' + this.\$route.params.id + '/edit')
        .then((response) => {
          this.form = response.data;
        })
          .catch(error => {
          console.log(error);
          this.\$notify({
            type: 'error',
            title: 'Oh no!',
            text: error.response.data.message,
          })
        });
    }
  }
</script>

EOT;
      if(\Storage::has('/coreui/src/views/'. $snakeCase .'/Edit'.$titleCase.'.vue')) {
        $this->info('Component already exists!');
      } else {

        $written = \Storage::put('/coreui/src/views/'. $snakeCase .'/Edit'.$camelCase.'.vue', $editFileContents);
        if($written)
        {
            $this->info('Created new Component Edit'.$camelCase.'.vue in /coreui/src/views/'. $snakeCase);
        } else {
            $this->info('Something went wrong');
        }
      }

      if(\Storage::has('/coreui/src/views/'. $snakeCase .'/Show'.$titleCase.'.vue')) {
        $this->info('Component already exists!');
      } else {

        $written = \Storage::put('/coreui/src/views/'. $snakeCase .'/Show'.$camelCase.'.vue', "");
        if($written)
        {
            $this->info('Created new Component Show'.$camelCase.'.vue in /coreui/src/views/'. $snakeCase);
        } else {
            $this->info('Something went wrong');
        }
      }
    }
    
}
