<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeCustomController extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
    */
    protected $signature = 'make:custom-controller  {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[CUSTOM] Create custom controller';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = str_replace('Controller','',$this->argument('name'));
        // $nameLowerCase = str_replace('Controller','',strtolower($this->argument('name')));
        $titleCase = Str::title($name);
        $camelCase = lcfirst(Str::camel($name));
        $slug = Str::slug(strtolower($name));
        $snakeCase = Str::snake(strtolower($name));

        $fileContents = <<<EOT
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services;
use DB;

class {$this->argument('name')} extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request \$request)
    {
        try {
            
            \$rows = Services\\{$name}Service::getAll(\$request);

            return response()->json( \$rows );
        } catch (\Exception \$e) {
            return response()->json(\$e->getMessage(), 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json( ['status' => 'success'] );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @return \Illuminate\Http\Response
     */
    public function store(Request \$request)
    {
        \$request->validate([
            
        ]);

        try {
            DB::beginTransaction();
            // call service here
            \$result = Services\\{$name}Service::store(\$request);

            DB::commit();
        } catch (\Exception \$e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => \$e->getMessage()],500 );
        }
        
        return response()->json( \$result );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  \$id
     * @return \Illuminate\Http\Response
     */
    public function show(\$id)
    {
        \${$camelCase} = Services\\{$name}Service::getById(\$id);
        return response()->json(\${$camelCase});
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  \$id
     * @return \Illuminate\Http\Response
     */
    public function edit(\$id)
    {
        \${$camelCase} = Services\\{$name}Service::getById(\$id);
        return response()->json(\${$camelCase});
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  \$request
     * @param  int  \$id
     * @return \Illuminate\Http\Response
     */
    public function update(Request \$request, \$id)
    {
        \$request->validate([
            
        ]);

        try {
            DB::beginTransaction();
            \$result = Services\\{$name}Service::update(\$request, \$id);
            DB::commit();
        } catch (\Exception \$e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => \$e->getMessage()],500 );
        }
        
        return response()->json( \$result );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  \$id
     * @return \Illuminate\Http\Response
     */
    public function destroy(\$id)
    {
        try {
            DB::beginTransaction();
            Services\\{$name}Service::delete(\$id);
            DB::commit();
        } catch (\Exception \$e) {
            DB::rollback();
            return response()->json( ['status' => 'error','message' => \$e->getMessage()],500 );
        }
        
        return response()->json( ['status' => 'success'] );
    }
}

EOT;
        if(\Storage::has('laravel/app/Http/Controllers/'.$this->argument('name').'.php')) {
            $this->info('Controller already exists!');

        } else {

            $written = \Storage::put('laravel/app/Http/Controllers/'.$this->argument('name').'.php', $fileContents);
            if($written)
            {
                $this->info('Created new Controller '.$this->argument('name').'.php in App\Http\Controllers.');
            } else {
                $this->info('Something went wrong');
            }
        }
    }
}
