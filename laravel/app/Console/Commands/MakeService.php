<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeService extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:service {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '[CUSTOM] Create a service file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = str_replace('Service','',($this->argument('name')));
        $words = implode(" ",preg_split('/(?=[A-Z])/',$name));
        $titleCase = Str::title($words);
        $camelCase = lcfirst(Str::camel($words));
        $slug = Str::slug(strtolower($words));
        $snakeCase = Str::snake(strtolower($words));

        //
        $fileContents = <<<EOT
<?php
namespace App\Services;

use App\Models;

class {$this->argument('name')}
{
    /**
     * Retrieve all data filtered by parameters
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAll(\$request = [])
    {
        \$rows = Models\\{$name}::select([
            '{$snakeCase}.*'
          ]);

          if (isset(\$request['search_text']) && !empty(\$request['search_text'])) {
              \$rows = \$rows->where(function (\$rows) use (\$request) {
                  
              });
          }
  
          if (isset(\$request['sort']) && !empty(\$request['sort'])) {
              \$sortOrder = explode('|', \$request['sort']);
              \$rows = \$rows->orderBy(\$sortOrder[0], \$sortOrder[1]);
          } else {
              \$rows = \$rows->orderBy('{$snakeCase}.id', 'asc');
          }
          
          if (isset(\$request['per_page']) && !empty(\$request['per_page'])) {
              \$rows = \$rows->paginate(\$request['per_page']);
          } else {
              \$rows = \$rows->get();
          }

          return \$rows;
    }

    /**
     * Retrieve data by ID
     * 
     */
    public static function getById(\$id)
    {
        \${$camelCase}= Models\\{$name}::find(\$id);
        return \${$camelCase};
    }
    /**
     * Attempt to create data
     */
    public static function store(\$request)
    {
        try {
            \${$camelCase} = new Models\\{$name};
            // attributes here
            \${$camelCase}->save();
        } catch (\Exception \$e) {
            throw \$e;
        }
        return \${$camelCase};
    }

    /**
     * Attempt to update data
     */
    public static function update(\$request, \$id)
    {
        try {
            \${$camelCase} = Models\\{$name}::find(\$id);
            // attributes here
            \${$camelCase}->save();
        } catch (\Exception \$e) {
            throw \$e;
        }
        return \${$camelCase};
    }

    /**
     * Attempt to delete data
     */

    public static function delete(\$id)
    {
        \${$camelCase} = Models\\{$name}::find(\$id);
        if(\${$camelCase}){
            return \${$camelCase}->delete();
        }
    }
}
EOT;
        if(\Storage::has('laravel/app/Services/'.$this->argument('name').'.php')) {
            $this->info('Service already exists!');

        } else {

            $written = \Storage::put('laravel/app/Services/'.$this->argument('name').'.php', $fileContents);
            if($written)
            {
                $this->info('Created new Service '.$this->argument('name').'.php in App\Services.');
            } else {
                $this->info('Something went wrong');
            }
        }
    
    }
}
