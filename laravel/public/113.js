(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[113],{

/***/ "../coreui/src/views/purchase/EditPurchase.vue":
/*!*****************************************************!*\
  !*** ../coreui/src/views/purchase/EditPurchase.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _EditPurchase_vue_vue_type_template_id_99b89802___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./EditPurchase.vue?vue&type=template&id=99b89802& */ "../coreui/src/views/purchase/EditPurchase.vue?vue&type=template&id=99b89802&");
/* harmony import */ var _EditPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./EditPurchase.vue?vue&type=script&lang=js& */ "../coreui/src/views/purchase/EditPurchase.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _EditPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _EditPurchase_vue_vue_type_template_id_99b89802___WEBPACK_IMPORTED_MODULE_0__["render"],
  _EditPurchase_vue_vue_type_template_id_99b89802___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/purchase/EditPurchase.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/purchase/EditPurchase.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ../coreui/src/views/purchase/EditPurchase.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditPurchase.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/EditPurchase.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/purchase/EditPurchase.vue?vue&type=template&id=99b89802&":
/*!************************************************************************************!*\
  !*** ../coreui/src/views/purchase/EditPurchase.vue?vue&type=template&id=99b89802& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPurchase_vue_vue_type_template_id_99b89802___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./EditPurchase.vue?vue&type=template&id=99b89802& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/EditPurchase.vue?vue&type=template&id=99b89802&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPurchase_vue_vue_type_template_id_99b89802___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_EditPurchase_vue_vue_type_template_id_99b89802___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/EditPurchase.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/purchase/EditPurchase.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchPo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchPo */ "../coreui/src/views/components/SearchPo.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchPo: _components_SearchPo__WEBPACK_IMPORTED_MODULE_1__["default"],
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    var _this = this;

    return {
      form: {
        // add more form attribute here
        po: {},
        detail: [],
        purchase_date: this.$moment().format('YYYY-MM-DD'),
        delete_detail: []
      },
      errors: {},
      disableButton: false,
      locationList: [],
      supplierList: [],
      scanCode: '',
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: '⌧',
        colId: 'removeButton',
        width: 35,
        cellRendererFramework: _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__["default"],
        cellRendererParams: {
          removeItemCallback: this.removeItem,
          dataArray: function dataArray() {
            return _this.form.detail;
          },
          deleteDataArray: function deleteDataArray() {
            return _this.form.delete_detail;
          }
        }
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 120
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50,
        editable: true
      }, {
        headerName: 'Harga',
        field: 'unit_purchase_price',
        width: 120,
        editable: true,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'PPN',
        field: 'tax_value',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Subtotal',
        field: 'subtotal',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  watch: {
    detail: {
      handler: function handler(val) {
        this.calculateDetail();
        this.refreshCells();
      },
      deep: true
    }
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/purchase'})
    },
    update: function update() {
      var _this2 = this;

      this.disableButton = true;
      this.$notify({
        type: 'info',
        title: 'Saving...',
        duration: 3000
      });
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put('/api/purchase/' + this.$route.params.id, this.form).then(function (response) {
        _this2.$notify({
          type: 'success',
          title: 'Success!',
          text: 'This data has been saved successfully.'
        });

        _this2.forceRerender();
      })["catch"](function (error) {
        _this2.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this2.errors = error.response.data.errors;
        }
      }).then(function () {
        _this2.disableButton = false;
      });
    },
    openSearchProductModal: function openSearchProductModal() {
      this.$refs['searchProduct'].openModal();
    },
    getProductByCode: function getProductByCode() {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("api/product/get-by-code/".concat(this.scanCode)).then(function (response) {
        if (!_.isEmpty(response.data)) {
          _this3.addDetail(response.data);
        } else {
          _this3.$notify({
            type: 'error',
            title: 'Oh no!',
            text: 'Kode tidak ditemukan'
          });
        }
      })["catch"](function (error) {
        _this3.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.data
        });
      }).then(function () {
        _this3.scanCode = '';
      });
    },
    addDetail: function addDetail(product) {
      var _this4 = this;

      var foundRow = this.form.detail.find(function (x) {
        return x.product_id == product.id;
      });

      if (foundRow) {
        return;
      }

      var productSupplier = product.product_supplier.find(function (x) {
        return x.supplier_id == _this4.form.supplier_id;
      });
      this.form.detail.push({
        seq_no: this.form.detail.length + 1,
        product_id: product.id,
        product_code: product.sku_code,
        product_name: product.name,
        qty: 1,
        unit_purchase_price: productSupplier ? productSupplier.purchase_price : 0,
        tax_percent: this.form.is_taxable ? 10 : 0,
        tax_value: 0,
        subtotal: 0
      });
    },
    openSearchPoModal: function openSearchPoModal() {
      this.$refs['searchPo'].openModal();
    },
    retrievePo: function retrievePo(poCode) {
      var _this5 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("/api/po/".concat(poCode, "/get-by-code")).then(function (response) {
        _this5.usePo(response.data);
      });
    },
    usePo: function usePo(po) {
      var _this6 = this;

      this.form.po_id = po.id;
      this.form.location_id = po.location_id;
      this.form.supplier_id = po.supplier_id;
      this.form.is_taxable = po.is_taxable;
      this.form.tax_total = po.tax_total;
      this.form.grand_total = po.grand_total;
      this.form.po = po;
      this.form.detail = [];

      _.forEach(po.detail, function (item, index) {
        _this6.form.detail.push({
          seq_no: _this6.form.detail.length + 1,
          po_detail_id: item.id,
          product_id: item.product_id,
          product_code: item.product_code,
          product_name: item.product_name,
          qty: item.qty,
          unit_purchase_price: item.unit_purchase_price,
          tax_percent: item.tax_percent,
          net_purchase_price: item.net_purchase_price,
          subtotal: item.subtotal
        });
      });
    },
    calculateItemTaxValue: function calculateItemTaxValue(item) {
      return item.unit_purchase_price * item.tax_percent / 100;
    },
    calculateItemNetPrice: function calculateItemNetPrice(item) {
      return Number(item.unit_purchase_price) + Number(item.tax_value);
    },
    calculateItemSubtotal: function calculateItemSubtotal(item) {
      return item.qty * item.net_purchase_price;
    },
    calculateDetail: function calculateDetail() {
      var _this7 = this;

      this.form.net_total = 0;
      this.form.tax_total = 0;
      this.form.grand_total = 0;

      _.forEach(this.form.detail, function (item, index) {
        item.tax_value = _this7.calculateItemTaxValue(item);
        item.net_purchase_price = _this7.calculateItemNetPrice(item);
        item.subtotal = _this7.calculateItemSubtotal(item);
        _this7.form.net_total += item.net_purchase_price;
        _this7.form.tax_total += item.tax_value;
        _this7.form.grand_total += item.subtotal;
      });
    },
    removeItem: function removeItem(item, index, arr) {
      var arrDelete = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      if (item.id && arrDelete) {
        arrDelete.push(item.id);
      }

      arr.splice(index, 1);
      this.updateSeqNumber();
    },
    onChangeTaxable: function onChangeTaxable() {
      var _this8 = this;

      _.forEach(this.form.detail, function (item, index) {
        item.tax_percent = _this8.form.is_taxable ? 10 : 0;
      });
    },
    process: function process() {
      var _this9 = this;

      this.$dialog.confirm('Are you sure you want to process this data?').then(function (dialog) {
        _this9.disableButton = true;

        _this9.$notify({
          type: 'info',
          title: 'Saving...',
          duration: 3000
        });

        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/purchase/' + _this9.$route.params.id + '/process', _this9.form).then(function (response) {
          _this9.$notify({
            type: 'success',
            title: 'Success!',
            text: 'This data has been saved successfully.'
          });

          _this9.$router.push({
            path: '/purchase/' + _this9.$route.params.id
          });
        })["catch"](function (error) {
          _this9.$notify({
            type: 'error',
            title: 'Oh no!',
            text: error.response.data.message
          });

          if (error.response.status == 422) {
            _this9.errors = error.response.data.errors;
          }
        }).then(function () {
          _this9.disableButton = false;
        });
      })["catch"](function () {});
    }
  },
  mounted: function mounted() {
    var _this10 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/purchase/' + this.$route.params.id + '/edit').then(function (response) {
      _this10.form = response.data.purchase;

      if (!_this10.form.po) {
        _this10.form.po = {};
      }

      _this10.form.delete_detail = [];
      _this10.supplierList = Object.freeze(response.data.supplier);
      _this10.locationList = Object.freeze(response.data.location);
    })["catch"](function (error) {
      console.log(error);

      _this10.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
    this.$refs['searchPo'].$on('po-selected', function (eventData) {
      _this10.usePo(eventData);
    });
    this.$refs['searchProduct'].$on("product-selected", function (eventData) {
      _.forEach(eventData, function (item, index) {
        _this10.addDetail(item);
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/EditPurchase.vue?vue&type=template&id=99b89802&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/purchase/EditPurchase.vue?vue&type=template&id=99b89802& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Edit Purchase")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Kode Hutang")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.code,
                                        expression: "form.code"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      readonly: "",
                                      placeholder: "(autogenerate)"
                                    },
                                    domProps: { value: _vm.form.code },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.code, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Pilih PO")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("div", { staticClass: "input-group" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.po.code,
                                          expression: "form.po.code"
                                        }
                                      ],
                                      class: [
                                        "form-control",
                                        _vm.errors.po_id ? " is-invalid" : ""
                                      ],
                                      attrs: { type: "text" },
                                      domProps: { value: _vm.form.po.code },
                                      on: {
                                        keypress: function($event) {
                                          if (
                                            !$event.type.indexOf("key") &&
                                            _vm._k(
                                              $event.keyCode,
                                              "enter",
                                              13,
                                              $event.key,
                                              "Enter"
                                            )
                                          ) {
                                            return null
                                          }
                                          $event.preventDefault()
                                          return _vm.retrievePo(
                                            _vm.form.po.code
                                          )
                                        },
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.form.po,
                                            "code",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "input-group-append" },
                                      [
                                        _c(
                                          "button",
                                          {
                                            staticClass:
                                              "btn btn-outline-primary",
                                            attrs: { type: "button" },
                                            on: {
                                              click: function($event) {
                                                return _vm.openSearchPoModal()
                                              }
                                            }
                                          },
                                          [_vm._v("Cari")]
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.po_id, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal Hutang*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.purchase_date
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY"
                                    },
                                    model: {
                                      value: _vm.form.purchase_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "purchase_date", $$v)
                                      },
                                      expression: "form.purchase_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.purchase_date, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal Jatuh Tempo*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.due_date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY"
                                    },
                                    model: {
                                      value: _vm.form.due_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "due_date", $$v)
                                      },
                                      expression: "form.due_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.due_date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Nomor Nota Supplier *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.supplier_invoice_no,
                                        expression: "form.supplier_invoice_no"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.supplier_invoice_no
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: { type: "text" },
                                    domProps: {
                                      value: _vm.form.supplier_invoice_no
                                    },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "supplier_invoice_no",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(
                                    _vm.errors.supplier_invoice_no,
                                    function(item) {
                                      return _c(
                                        "div",
                                        { staticClass: "text-danger small" },
                                        [_vm._v(_vm._s(item))]
                                      )
                                    }
                                  )
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "form-group row" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Keterangan")]
                                ),
                                _vm._v(" "),
                                _c("span", { staticClass: "col-sm-8" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.remarks,
                                        expression: "form.remarks"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.remarks ? " is-invalid" : ""
                                    ],
                                    attrs: { cols: "30" },
                                    domProps: { value: _vm.form.remarks },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "remarks",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.errors.remarks, function(item) {
                                  return _c(
                                    "div",
                                    { staticClass: "text-danger small" },
                                    [_vm._v(_vm._s(item))]
                                  )
                                })
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.locationList,
                                      reduce: function(location) {
                                        return location.id
                                      },
                                      label: "name",
                                      name: "location"
                                    },
                                    model: {
                                      value: _vm.form.location_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "location_id", $$v)
                                      },
                                      expression: "form.location_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Supplier *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.supplier_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.supplierList,
                                      reduce: function(supplier) {
                                        return supplier.id
                                      },
                                      label: "name",
                                      name: "supplier",
                                      disabled: !!_vm.form.po_id
                                    },
                                    model: {
                                      value: _vm.form.supplier_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "supplier_id", $$v)
                                      },
                                      expression: "form.supplier_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.supplier_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("PPN")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("div", { staticClass: "form-check" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.is_taxable,
                                          expression: "form.is_taxable"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: {
                                        type: "checkbox",
                                        id: "is_taxable"
                                      },
                                      domProps: {
                                        checked: Array.isArray(
                                          _vm.form.is_taxable
                                        )
                                          ? _vm._i(_vm.form.is_taxable, null) >
                                            -1
                                          : _vm.form.is_taxable
                                      },
                                      on: {
                                        change: [
                                          function($event) {
                                            var $$a = _vm.form.is_taxable,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = null,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "is_taxable",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "is_taxable",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(
                                                _vm.form,
                                                "is_taxable",
                                                $$c
                                              )
                                            }
                                          },
                                          function($event) {
                                            return _vm.onChangeTaxable()
                                          }
                                        ]
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "label",
                                      {
                                        staticClass: "form-check-label",
                                        attrs: { for: "is_taxable" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                      Ya\n                    "
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.is_taxable, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tandai PO Selesai")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("div", { staticClass: "form-check" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.mark_po_complete,
                                          expression: "form.mark_po_complete"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: {
                                        type: "checkbox",
                                        id: "mark_po_complete"
                                      },
                                      domProps: {
                                        checked: Array.isArray(
                                          _vm.form.mark_po_complete
                                        )
                                          ? _vm._i(
                                              _vm.form.mark_po_complete,
                                              null
                                            ) > -1
                                          : _vm.form.mark_po_complete
                                      },
                                      on: {
                                        change: [
                                          function($event) {
                                            var $$a = _vm.form.mark_po_complete,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = null,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "mark_po_complete",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "mark_po_complete",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(
                                                _vm.form,
                                                "mark_po_complete",
                                                $$c
                                              )
                                            }
                                          },
                                          function($event) {
                                            return _vm.onChangeTaxable()
                                          }
                                        ]
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "label",
                                      {
                                        staticClass: "form-check-label",
                                        attrs: { for: "mark_po_complete" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                      Ya\n                    "
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.mark_po_complete, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        { staticClass: "mb-2" },
                        [
                          _c("CCol", { attrs: { col: "6", lg: "4" } }, [
                            _c("div", { staticClass: "input-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.scanCode,
                                    expression: "scanCode"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder:
                                    "Scan / masukkan kode SKU barang..."
                                },
                                domProps: { value: _vm.scanCode },
                                on: {
                                  keypress: function($event) {
                                    if (
                                      !$event.type.indexOf("key") &&
                                      _vm._k(
                                        $event.keyCode,
                                        "enter",
                                        13,
                                        $event.key,
                                        "Enter"
                                      )
                                    ) {
                                      return null
                                    }
                                    $event.preventDefault()
                                    return _vm.getProductByCode($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.scanCode = $event.target.value
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "input-group-append" },
                                [
                                  _c(
                                    "CButton",
                                    {
                                      attrs: {
                                        color: "primary",
                                        disabled: _vm.disableButton
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openSearchProductModal()
                                        }
                                      }
                                    },
                                    [
                                      _c("i", { staticClass: "fas fa-plus" }),
                                      _vm._v(" Product\n                  ")
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        [
                          _c(
                            "CCol",
                            {
                              staticClass: "offset-sm-6 offset-lg-8",
                              attrs: { col: "6", lg: "4" }
                            },
                            [
                              _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Grand Total")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "col-sm-8" },
                                  [
                                    _c("cleave", {
                                      staticClass:
                                        "form-control-plaintext form-control-lg text-right",
                                      attrs: {
                                        options: {
                                          numeral: true,
                                          numeralDecimalScale: 0
                                        },
                                        readonly: ""
                                      },
                                      model: {
                                        value: _vm.form.grand_total,
                                        callback: function($$v) {
                                          _vm.$set(_vm.form, "grand_total", $$v)
                                        },
                                        expression: "form.grand_total"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _vm._l(_vm.errors.grand_total, function(
                                      item
                                    ) {
                                      return _c(
                                        "div",
                                        { staticClass: "text-danger small" },
                                        [_vm._v(_vm._s(item))]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: {
                            click: function($event) {
                              return _vm.update()
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fas fa-save" }),
                          _vm._v(" Save\n          ")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: {
                            click: function($event) {
                              return _vm.process()
                            }
                          }
                        },
                        [_vm._v("\n            Process\n          ")]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "btn-group" }, [
                        _c(
                          "div",
                          [
                            _c(
                              "CDropdown",
                              {
                                staticClass: "m-2",
                                attrs: {
                                  "toggler-text": "Dropdown Button",
                                  color: "primary"
                                }
                              },
                              [
                                _c("CDropdownItem", [_vm._v("First Action")]),
                                _vm._v(" "),
                                _c("CDropdownItem", [_vm._v("Second Action")]),
                                _vm._v(" "),
                                _c("CDropdownItem", [_vm._v("Third Action")]),
                                _vm._v(" "),
                                _c("CDropdownDivider"),
                                _vm._v(" "),
                                _c("CDropdownItem", [
                                  _vm._v("Something else here...")
                                ]),
                                _vm._v(" "),
                                _c(
                                  "CDropdownItem",
                                  { attrs: { disabled: "" } },
                                  [_vm._v("Disabled action")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchPo", { ref: "searchPo" }),
      _vm._v(" "),
      _c("SearchProduct", {
        ref: "searchProduct",
        attrs: { allowSelectMultiple: true }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);