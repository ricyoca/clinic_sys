(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[158],{

/***/ "../coreui/src/mixins/ag-grid-options.js":
/*!***********************************************!*\
  !*** ../coreui/src/mixins/ag-grid-options.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ag-grid-vue */ "../coreui/node_modules/ag-grid-vue/main.js");
/* harmony import */ var ag_grid_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! numeral */ "../coreui/node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AgGridVue: ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__["AgGridVue"]
  },
  data: function data() {
    return {
      gridOptions: null
    };
  },
  beforeMount: function beforeMount() {
    var _this = this;

    this.gridOptions = {
      headerHeight: 35,
      rowHeight: 30,
      rowSelection: 'multiple',
      stopEditingWhenGridLosesFocus: true,
      suppressScrollOnNewData: true,
      defaultColDef: {
        tooltipValueGetter: function tooltipValueGetter(params) {
          if (_this.errors && _this.errors['detail.' + params.node.rowIndex + '.' + params.colDef.field]) {
            return _this.errors['detail.' + params.node.rowIndex + '.' + params.colDef.field][0];
          }

          if (params.valueFormatted === '-' && params.colDef.editable) {
            return '(Click to Edit)';
          }

          if (params.valueFormatted !== null) return params.valueFormatted;
          if (params.value !== undefined && params.value !== null) return params.value.toString();
          return ' ';
        },
        cellClassRules: {
          'editable-grid-cell': function editableGridCell(params) {
            return params.colDef.editable == true || typeof params.colDef.editable == 'function' && params.colDef.editable(params) == true;
          },
          'bg-danger': function bgDanger(params) {
            return _this.errors && _this.errors['detail.' + params.rowIndex + '.' + params.colDef.field];
          }
        },
        valueFormatter: function valueFormatter(params) {
          if (!params.value) {
            return '-';
          }

          return params.value;
        },
        singleClickEdit: true,
        resizable: true
      },
      enterMovesDownAfterEdit: true,
      rowClassRules: {
        'locked-price-row': function lockedPriceRow(params) {
          return params.data.hasOwnProperty('will_update_sales_price') && params.data.will_update_sales_price == false;
        }
      },
      // onRowDataChanged: (event) => {
      //     let lastIndex = event.api.getModel().allChildrenCount - 1;
      //     event.api.ensureIndexVisible(lastIndex,'bottom');
      // },
      context: {
        componentParent: this
      },
      navigateToNextCell: function navigateToNextCell(params) {
        var previousCell = params.previousCellPosition;
        var suggestedNextCell = params.nextCellPosition;
        var KEY_UP = 38;
        var KEY_DOWN = 40;
        var KEY_LEFT = 37;
        var KEY_RIGHT = 39;

        switch (params.key) {
          case KEY_DOWN:
            var previousNode = previousCell.column.gridApi.getRowNode(previousCell.rowIndex);
            previousCell = params.previousCellPosition; // set selected cell on current cell + 1

            previousCell.column.gridApi.forEachNode(function (node) {
              if (previousCell.rowIndex + 1 === node.rowIndex) {
                if (params.event.shiftKey) {
                  if (node.isSelected()) {
                    previousNode.setSelected(false);
                  } else {
                    node.setSelected(true);
                  }
                } else node.setSelected(true, true);
              }
            });
            return suggestedNextCell;

          case KEY_UP:
            var previousNode = previousCell.column.gridApi.getRowNode(previousCell.rowIndex);
            previousCell = params.previousCellPosition; // set selected cell on current cell - 1

            previousCell.column.gridApi.forEachNode(function (node) {
              if (previousCell.rowIndex - 1 === node.rowIndex) {
                if (params.event.shiftKey) {
                  if (node.isSelected()) {
                    previousNode.setSelected(false);
                  } else {
                    node.setSelected(true);
                  }
                } else node.setSelected(true, true);
              }
            });
            return suggestedNextCell;

          case KEY_LEFT:
          case KEY_RIGHT:
            return suggestedNextCell;

          default:
            throw "this will never happen, navigation is always one of the 4 keys above";
        }
      },
      onCellKeyDown: function onCellKeyDown(e) {
        e.event.preventDefault();
        var keyPressed = e.event.key;

        switch (keyPressed) {
          case 'Delete':
            var removeCol = e.columnApi.getColumn('removeButton');
            if (!removeCol) return;

            if (typeof removeCol.colDef.cellRendererParams.removeItemCallback == 'function') {
              // if(!confirm("Are you sure you want to delete the selected rows?")) return;
              var selectedNodes = e.api.getSelectedNodes().reverse();

              _.forEach(selectedNodes, function (item, index) {
                removeCol.colDef.cellRendererParams.removeItemCallback(item.data, item.id, removeCol.colDef.cellRendererParams.dataArray(), removeCol.colDef.cellRendererParams.deleteDataArray());
              });
            }

            break;

          case 'F3': // e.event.originalEvent.keyCode = 0;

          default:
            break;
        }
      } //     suppressKeyboardEvent: (params) => {
      //         var KEY_A = 65;
      //         var KEY_C = 67;
      //         var KEY_V = 86;
      //         var KEY_D = 68;
      //         var KEY_PAGE_UP = 33;
      //         var KEY_PAGE_DOWN = 34;
      //         var KEY_TAB = 9;
      //         var KEY_LEFT = 37;
      //         var KEY_UP = 38;
      //         var KEY_RIGHT = 39;
      //         var KEY_DOWN = 40;
      //         var KEY_F2 = 113;
      //         var KEY_F3 = 114;
      //         var KEY_BACKSPACE = 8;
      //         var KEY_ESCAPE = 27;
      //         var KEY_SPACE = 32;
      //         var KEY_DELETE = 46;
      //         var KEY_PAGE_HOME = 36;
      //         var KEY_PAGE_END = 35;
      //         var event = params.event;
      //         var key = event.which;
      //         var keysToSuppress = [
      //           KEY_PAGE_UP,
      //           KEY_PAGE_DOWN,
      //           KEY_TAB,
      //           KEY_F3,
      //           KEY_ESCAPE,
      //         ];
      //         var editingKeys = [
      //           KEY_LEFT,
      //           KEY_RIGHT,
      //           KEY_UP,
      //           KEY_DOWN,
      //           KEY_BACKSPACE,
      //           KEY_DELETE,
      //           KEY_SPACE,
      //           KEY_PAGE_HOME,
      //           KEY_PAGE_END,
      //         ];
      //         if (event.ctrlKey || event.metaKey) {
      //           keysToSuppress.push(KEY_A);
      //           keysToSuppress.push(KEY_V);
      //           keysToSuppress.push(KEY_C);
      //           keysToSuppress.push(KEY_D);
      //         }
      //         if (!params.editing) {
      //           keysToSuppress = keysToSuppress.concat(editingKeys);
      //         }
      //         var suppress = keysToSuppress.indexOf(key) >= 0;
      //         return suppress;
      //     },

    };
  },
  methods: {
    refreshCells: function refreshCells() {
      this.gridOptions.api.refreshCells(); // this.gridOptions.api.redrawRows();
    },
    redrawRows: function redrawRows() {
      this.gridOptions.api.redrawRows();
    },
    dateComparator: function dateComparator(date1, date2) {
      var date1Number = this.monthToComparableNumber(date1);
      var date2Number = this.monthToComparableNumber(date2);

      if (date1Number === null && date2Number === null) {
        return 0;
      }

      if (date1Number === null) {
        return -1;
      }

      if (date2Number === null) {
        return 1;
      }

      return date1Number - date2Number;
    },
    monthToComparableNumber: function monthToComparableNumber(date) {
      if (date === undefined || date === null || !this.$moment(date).isValid()) {
        return null;
      }

      var yearNumber = this.$moment(date).year();
      var monthNumber = this.$moment(date).month();
      var dayNumber = this.$moment(date).date();
      var result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
      return result;
    },
    updateSeqNumber: function updateSeqNumber() {
      var _this2 = this;

      this.$nextTick(function () {
        var itemsToUpdate = [];

        _this2.gridOptions.api.forEachNodeAfterFilterAndSort(function (rowNode, index) {
          var data = rowNode.data;
          data.seq_no = index + 1;
          itemsToUpdate.push(data);
        });

        var res = _this2.gridOptions.api.applyTransaction({
          update: itemsToUpdate
        });
      });
    },
    numberFormatter: function numberFormatter(params) {
      if (params.value === null || params.value === '' || params.value === undefined) {
        return '-';
      }

      return numeral__WEBPACK_IMPORTED_MODULE_1___default()(params.value).format();
    }
  },
  beforeDestroy: function beforeDestroy() {
    if (this.gridOptions.api) {
      this.gridOptions.api.destroy();
    }
  }
});

/***/ }),

/***/ "../coreui/src/views/purchase/ShowPurchase.vue":
/*!*****************************************************!*\
  !*** ../coreui/src/views/purchase/ShowPurchase.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowPurchase_vue_vue_type_template_id_f171671c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowPurchase.vue?vue&type=template&id=f171671c& */ "../coreui/src/views/purchase/ShowPurchase.vue?vue&type=template&id=f171671c&");
/* harmony import */ var _ShowPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowPurchase.vue?vue&type=script&lang=js& */ "../coreui/src/views/purchase/ShowPurchase.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowPurchase_vue_vue_type_template_id_f171671c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowPurchase_vue_vue_type_template_id_f171671c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/purchase/ShowPurchase.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/purchase/ShowPurchase.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ../coreui/src/views/purchase/ShowPurchase.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowPurchase.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/ShowPurchase.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchase_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/purchase/ShowPurchase.vue?vue&type=template&id=f171671c&":
/*!************************************************************************************!*\
  !*** ../coreui/src/views/purchase/ShowPurchase.vue?vue&type=template&id=f171671c& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchase_vue_vue_type_template_id_f171671c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowPurchase.vue?vue&type=template&id=f171671c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/ShowPurchase.vue?vue&type=template&id=f171671c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchase_vue_vue_type_template_id_f171671c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchase_vue_vue_type_template_id_f171671c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/ShowPurchase.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/purchase/ShowPurchase.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchPo__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchPo */ "../coreui/src/views/components/SearchPo.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchPo: _components_SearchPo__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    return {
      form: {
        // add more form attribute here
        po: {},
        detail: [],
        purchase_date: this.$moment().format('YYYY-MM-DD')
      },
      errors: {},
      disableButton: false,
      locationList: [],
      supplierList: [],
      scanCode: '',
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 120
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50
      }, {
        headerName: 'Harga',
        field: 'unit_purchase_price',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'PPN',
        field: 'tax_value',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Subtotal',
        field: 'subtotal',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  watch: {
    detail: {
      handler: function handler(val) {
        this.calculateDetail();
      },
      deep: true
    }
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/purchase'})
    },
    update: function update() {
      var _this = this;

      this.disableButton = true;
      this.$notify({
        type: 'info',
        title: 'Saving...',
        duration: 3000
      });
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put('/api/purchase/' + this.$route.params.id, this.form).then(function (response) {
        _this.$notify({
          type: 'success',
          title: 'Success!',
          text: 'This data has been saved successfully.'
        });

        _this.forceRerender();
      })["catch"](function (error) {
        _this.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this.errors = error.response.data.errors;
        }
      }).then(function () {
        _this.disableButton = false;
      });
    },
    openSearchPoModal: function openSearchPoModal() {
      this.$refs['searchPo'].openModal();
    },
    retrievePo: function retrievePo(poCode) {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("/api/po/".concat(poCode, "/get-by-code")).then(function (response) {
        _this2.usePo(response.data);
      });
    },
    calculateItemTaxValue: function calculateItemTaxValue(item) {
      return item.unit_purchase_price * item.tax_percent / 100;
    },
    calculateItemNetPrice: function calculateItemNetPrice(item) {
      return Number(item.unit_purchase_price) + Number(item.tax_value);
    },
    calculateItemSubtotal: function calculateItemSubtotal(item) {
      return item.qty * item.net_purchase_price;
    },
    calculateDetail: function calculateDetail() {
      var _this3 = this;

      this.form.net_total = 0;
      this.form.tax_total = 0;
      this.form.grand_total = 0;

      _.forEach(this.form.detail, function (item, index) {
        item.tax_value = _this3.calculateItemTaxValue(item);
        item.net_purchase_price = _this3.calculateItemNetPrice(item);
        item.subtotal = _this3.calculateItemSubtotal(item);
        _this3.form.net_total += item.net_purchase_price;
        _this3.form.tax_total += item.tax_value;
        _this3.form.grand_total += item.subtotal;
      });
    },
    onChangeTaxable: function onChangeTaxable() {
      var _this4 = this;

      _.forEach(this.form.detail, function (item, index) {
        item.tax_percent = _this4.form.is_taxable ? 10 : 0;
      });
    }
  },
  mounted: function mounted() {
    var _this5 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/purchase/' + this.$route.params.id).then(function (response) {
      _this5.form = response.data.purchase;
      _this5.supplierList = Object.freeze(response.data.supplier);
      _this5.locationList = Object.freeze(response.data.location);
    })["catch"](function (error) {
      console.log(error);

      _this5.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
    this.$refs['searchPo'].$on('po-selected', function (eventData) {
      _this5.usePo(eventData);
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase/ShowPurchase.vue?vue&type=template&id=f171671c&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/purchase/ShowPurchase.vue?vue&type=template&id=f171671c& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Show Purchase")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Kode Hutang")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.code,
                                        expression: "form.code"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      readonly: "",
                                      placeholder: "(autogenerate)",
                                      readonly: ""
                                    },
                                    domProps: { value: _vm.form.code },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.code, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Pilih PO")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.po.code,
                                        expression: "form.po.code"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.po_id ? " is-invalid" : ""
                                    ],
                                    attrs: { type: "text", readonly: "" },
                                    domProps: { value: _vm.form.po.code },
                                    on: {
                                      keypress: function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "enter",
                                            13,
                                            $event.key,
                                            "Enter"
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.retrievePo(_vm.form.po.code)
                                      },
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form.po,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.po_id, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal Hutang*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.purchase_date
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.purchase_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "purchase_date", $$v)
                                      },
                                      expression: "form.purchase_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.purchase_date, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal Jatuh Tempo*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.due_date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.due_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "due_date", $$v)
                                      },
                                      expression: "form.due_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.due_date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Nomor Nota Supplier *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.supplier_invoice_no,
                                        expression: "form.supplier_invoice_no"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.supplier_invoice_no
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: { type: "text", readonly: "" },
                                    domProps: {
                                      value: _vm.form.supplier_invoice_no
                                    },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "supplier_invoice_no",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(
                                    _vm.errors.supplier_invoice_no,
                                    function(item) {
                                      return _c(
                                        "div",
                                        { staticClass: "text-danger small" },
                                        [_vm._v(_vm._s(item))]
                                      )
                                    }
                                  )
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c(
                              "div",
                              { staticClass: "form-group row" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Keterangan")]
                                ),
                                _vm._v(" "),
                                _c("span", { staticClass: "col-sm-8" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.remarks,
                                        expression: "form.remarks"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.remarks ? " is-invalid" : ""
                                    ],
                                    attrs: { cols: "30", readonly: "" },
                                    domProps: { value: _vm.form.remarks },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "remarks",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.errors.remarks, function(item) {
                                  return _c(
                                    "div",
                                    { staticClass: "text-danger small" },
                                    [_vm._v(_vm._s(item))]
                                  )
                                })
                              ],
                              2
                            )
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.locationList,
                                      reduce: function(location) {
                                        return location.id
                                      },
                                      label: "name",
                                      name: "location",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.location_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "location_id", $$v)
                                      },
                                      expression: "form.location_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Supplier *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.supplier_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.supplierList,
                                      reduce: function(supplier) {
                                        return supplier.id
                                      },
                                      label: "name",
                                      name: "supplier",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.supplier_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "supplier_id", $$v)
                                      },
                                      expression: "form.supplier_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.supplier_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("PPN")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("div", { staticClass: "form-check" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.is_taxable,
                                          expression: "form.is_taxable"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: {
                                        type: "checkbox",
                                        id: "is_taxable",
                                        disabled: ""
                                      },
                                      domProps: {
                                        checked: Array.isArray(
                                          _vm.form.is_taxable
                                        )
                                          ? _vm._i(_vm.form.is_taxable, null) >
                                            -1
                                          : _vm.form.is_taxable
                                      },
                                      on: {
                                        change: [
                                          function($event) {
                                            var $$a = _vm.form.is_taxable,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = null,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "is_taxable",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "is_taxable",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(
                                                _vm.form,
                                                "is_taxable",
                                                $$c
                                              )
                                            }
                                          },
                                          function($event) {
                                            return _vm.onChangeTaxable()
                                          }
                                        ]
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "label",
                                      {
                                        staticClass: "form-check-label",
                                        attrs: { for: "is_taxable" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                      Ya\n                    "
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.is_taxable, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        [
                          _c(
                            "CCol",
                            {
                              staticClass: "offset-sm-6 offset-lg-8",
                              attrs: { col: "6", lg: "4" }
                            },
                            [
                              _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Grand Total")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "col-sm-8" },
                                  [
                                    _c("cleave", {
                                      staticClass:
                                        "form-control-plaintext form-control-lg text-right",
                                      attrs: {
                                        options: {
                                          numeral: true,
                                          numeralDecimalScale: 0
                                        },
                                        readonly: ""
                                      },
                                      model: {
                                        value: _vm.form.grand_total,
                                        callback: function($$v) {
                                          _vm.$set(_vm.form, "grand_total", $$v)
                                        },
                                        expression: "form.grand_total"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _vm._l(_vm.errors.grand_total, function(
                                      item
                                    ) {
                                      return _c(
                                        "div",
                                        { staticClass: "text-danger small" },
                                        [_vm._v(_vm._s(item))]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      ),
                      _vm._v(" "),
                      _c("div", { staticClass: "btn-group" }, [
                        _c(
                          "div",
                          [
                            _c(
                              "CDropdown",
                              {
                                staticClass: "m-2",
                                attrs: {
                                  "toggler-text": "Dropdown Button",
                                  color: "primary"
                                }
                              },
                              [
                                _c("CDropdownItem", [_vm._v("First Action")]),
                                _vm._v(" "),
                                _c("CDropdownItem", [_vm._v("Second Action")]),
                                _vm._v(" "),
                                _c("CDropdownItem", [_vm._v("Third Action")]),
                                _vm._v(" "),
                                _c("CDropdownDivider"),
                                _vm._v(" "),
                                _c("CDropdownItem", [
                                  _vm._v("Something else here...")
                                ]),
                                _vm._v(" "),
                                _c(
                                  "CDropdownItem",
                                  { attrs: { disabled: "" } },
                                  [_vm._v("Disabled action")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchPo", { ref: "searchPo" })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);