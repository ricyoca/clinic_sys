(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[157],{

/***/ "../coreui/src/views/ag-component/RemoveButton.vue":
/*!*********************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=template&id=030e58b5& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony import */ var _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=script&lang=js& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/ag-component/RemoveButton.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!****************************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=template&id=030e58b5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/stock_transfer/ShowStockTransfer.vue":
/*!****************************************************************!*\
  !*** ../coreui/src/views/stock_transfer/ShowStockTransfer.vue ***!
  \****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowStockTransfer_vue_vue_type_template_id_b0bb2c9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a& */ "../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a&");
/* harmony import */ var _ShowStockTransfer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowStockTransfer.vue?vue&type=script&lang=js& */ "../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowStockTransfer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowStockTransfer_vue_vue_type_template_id_b0bb2c9a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowStockTransfer_vue_vue_type_template_id_b0bb2c9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/stock_transfer/ShowStockTransfer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************!*\
  !*** ../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowStockTransfer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowStockTransfer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowStockTransfer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a&":
/*!***********************************************************************************************!*\
  !*** ../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a& ***!
  \***********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowStockTransfer_vue_vue_type_template_id_b0bb2c9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowStockTransfer_vue_vue_type_template_id_b0bb2c9a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowStockTransfer_vue_vue_type_template_id_b0bb2c9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "../coreui/node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
  name: 'RemoveButton',
  data: function data() {
    return {// dataArray: this.params.dataArray(),
      // deleteDataArray: this.params.deleteDataArray(),
    };
  },
  mounted: function mounted() {
    console.log(this.params);
  },
  methods: {
    removeItem: function removeItem() {
      this.params.removeItemCallback(this.params.data, this.params.node.id, this.params.dataArray(), this.params.deleteDataArray()); // this.$eventHub.$emit('remove-item',this.params.data, this.params.node.id)
    }
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    return {
      form: {
        // add more form attribute here
        date: this.$moment().format('YYYY-MM-DD'),
        detail: []
      },
      errors: {},
      disableButton: false,
      locationList: [],
      scanCode: '',
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 120
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty Asal',
        field: 'from_qty',
        width: 100
      }, {
        headerName: 'Qty Tujuan',
        field: 'to_qty',
        width: 100
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50
      }]
    };
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/stock-transfer'})
    },
    update: function update() {
      var _this = this;

      this.disableButton = true;
      this.$notify({
        type: 'info',
        title: 'Saving...',
        duration: 3000
      });
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put('/api/stock-transfer/' + this.$route.params.id, this.form).then(function (response) {
        _this.$notify({
          type: 'success',
          title: 'Success!',
          text: 'This data has been saved successfully.'
        });
      })["catch"](function (error) {
        _this.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this.errors = error.response.data.errors;
        }
      }).then(function () {
        _this.disableButton = false;
      });
    },
    process: function process() {
      var _this2 = this;

      this.$dialog.confirm('Are you sure you want to process this data?').then(function (dialog) {
        _this2.disableButton = true;

        _this2.$notify({
          type: 'info',
          title: 'Saving...',
          duration: 3000
        });

        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/stock-transfer/' + _this2.$route.params.id + '/process', _this2.form).then(function (response) {
          _this2.$notify({
            type: 'success',
            title: 'Success!',
            text: 'This data has been saved successfully.'
          });

          _this2.$router.push({
            path: '/api/stock-transfer/' + _this2.$route.params.id
          });
        })["catch"](function (error) {
          _this2.$notify({
            type: 'error',
            title: 'Oh no!',
            text: error.response.data.message
          });

          if (error.response.status == 422) {
            _this2.errors = error.response.data.errors;
          }
        }).then(function () {
          _this2.disableButton = false;
        });
      })["catch"](function () {});
    },
    openSearchProductModal: function openSearchProductModal() {
      this.$refs['searchProduct'].openModal();
    },
    getProductByCode: function getProductByCode() {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("api/product/get-by-code/".concat(this.scanCode)).then(function (response) {
        if (!_.isEmpty(response.data)) {
          _this3.addDetail(response.data);
        } else {
          _this3.$notify({
            type: 'error',
            title: 'Oh no!',
            text: 'Kode tidak ditemukan'
          });
        }
      })["catch"](function (error) {
        _this3.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.data
        });
      }).then(function () {
        _this3.scanCode = '';
      });
    }
  },
  mounted: function mounted() {
    var _this4 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/stock-transfer/' + this.$route.params.id).then(function (response) {
      _this4.form = response.data.stockTransfer;
      _this4.form.delete_detail = [];
      _this4.locationList = Object.freeze(response.data.location);
    })["catch"](function (error) {
      console.log(error);

      _this4.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
    this.$refs['searchProduct'].$on('product-selected', function (eventData) {
      _.forEach(eventData, function (item, index) {
        _this4.addDetail(item);
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "button",
    {
      staticClass: "btn btn-sm btn-danger",
      attrs: { type: "button" },
      on: { click: _vm.removeItem }
    },
    [_c("i", { staticClass: "fas fa-times" })]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a&":
/*!*****************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/stock_transfer/ShowStockTransfer.vue?vue&type=template&id=b0bb2c9a& ***!
  \*****************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Show Stock Transfer")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "12", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Kode Mutasi")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.code,
                                        expression: "form.code"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      readonly: "",
                                      placeholder: "(autogenerate)",
                                      readonly: ""
                                    },
                                    domProps: { value: _vm.form.code },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.code, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "date", $$v)
                                      },
                                      expression: "form.date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi*")]
                              ),
                              _vm._v(" "),
                              _c("span", { staticClass: "col-sm-8" }, [
                                _c("div", { staticClass: "row" }, [
                                  _c(
                                    "span",
                                    { staticClass: "col-sm-5" },
                                    [
                                      _c("v-select", {
                                        class: [
                                          _vm.errors.from_location_id
                                            ? " is-invalid"
                                            : ""
                                        ],
                                        attrs: {
                                          options: _vm.locationList,
                                          reduce: function(location) {
                                            return location.id
                                          },
                                          label: "name",
                                          name: "location",
                                          placeholder: "- Asal -",
                                          disabled: ""
                                        },
                                        model: {
                                          value: _vm.form.from_location_id,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "from_location_id",
                                              $$v
                                            )
                                          },
                                          expression: "form.from_location_id"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        _vm.errors.from_location_id,
                                        function(item) {
                                          return _c(
                                            "div",
                                            {
                                              staticClass: "text-danger small"
                                            },
                                            [_vm._v(_vm._s(item))]
                                          )
                                        }
                                      )
                                    ],
                                    2
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "label",
                                    { staticClass: "col-form-label col-sm-2" },
                                    [_vm._v("Ke*")]
                                  ),
                                  _vm._v(" "),
                                  _c(
                                    "span",
                                    { staticClass: "col-sm-5" },
                                    [
                                      _c("v-select", {
                                        class: [
                                          _vm.errors.to_location_id
                                            ? " is-invalid"
                                            : ""
                                        ],
                                        attrs: {
                                          options: _vm.locationList,
                                          reduce: function(location) {
                                            return location.id
                                          },
                                          label: "name",
                                          name: "location",
                                          placeholder: "- Tujuan -",
                                          disabled: ""
                                        },
                                        model: {
                                          value: _vm.form.to_location_id,
                                          callback: function($$v) {
                                            _vm.$set(
                                              _vm.form,
                                              "to_location_id",
                                              $$v
                                            )
                                          },
                                          expression: "form.to_location_id"
                                        }
                                      }),
                                      _vm._v(" "),
                                      _vm._l(
                                        _vm.errors.to_location_id,
                                        function(item) {
                                          return _c(
                                            "div",
                                            {
                                              staticClass: "text-danger small"
                                            },
                                            [_vm._v(_vm._s(item))]
                                          )
                                        }
                                      )
                                    ],
                                    2
                                  )
                                ])
                              ])
                            ])
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "12", lg: "6" } }, [
                            _c(
                              "div",
                              { staticClass: "form-group row" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Keterangan")]
                                ),
                                _vm._v(" "),
                                _c("span", { staticClass: "col-sm-8" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.remarks,
                                        expression: "form.remarks"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.remarks ? " is-invalid" : ""
                                    ],
                                    attrs: { cols: "30", readonly: "" },
                                    domProps: { value: _vm.form.remarks },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "remarks",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.errors.code, function(item) {
                                  return _c(
                                    "div",
                                    { staticClass: "text-danger small" },
                                    [_vm._v(_vm._s(item))]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);