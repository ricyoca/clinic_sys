(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[44],{

/***/ "../coreui/src/services/satusehatmaster.js":
/*!*************************************************!*\
  !*** ../coreui/src/services/satusehatmaster.js ***!
  \*************************************************/
/*! exports provided: getConfig, sendBundle, generateToken, getToken, sendNewPatient, getConditionClinicalStatus, getConditionCategory, getEncounterStatus, getEncounterClass, getLocations */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getConfig", function() { return getConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendBundle", function() { return sendBundle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "generateToken", function() { return generateToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getToken", function() { return getToken; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sendNewPatient", function() { return sendNewPatient; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getConditionClinicalStatus", function() { return getConditionClinicalStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getConditionCategory", function() { return getConditionCategory; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEncounterStatus", function() { return getEncounterStatus; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getEncounterClass", function() { return getEncounterClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getLocations", function() { return getLocations; });
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "../coreui/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var uuid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! uuid */ "../coreui/node_modules/uuid/dist/esm-browser/index.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

var satusehatMasterAPIUrl = "http://127.0.0.1:8001/api";
var solamedicUsername = "appdr";
var solamedicPassword = "appdrpw10";

function getConfig() {
  var token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
  return {
    headers: {
      Authorization: "Bearer ".concat(token)
    }
  };
}
function sendBundle(_x) {
  return _sendBundle.apply(this, arguments);
}

function _sendBundle() {
  _sendBundle = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee(data) {
    var config, practitionerUuidResponse, practitionerUuid, encounterUUid, conditionUUid, response, _response;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            config = getConfig();
            _context.next = 3;
            return axios.get('/api/config/get-value', {
              params: {
                key: 'practitioner_ihs_number'
              }
            });

          case 3:
            practitionerUuidResponse = _context.sent;
            practitionerUuid = practitionerUuidResponse.data;
            console.log(satusehatMasterAPIUrl);
            encounterUUid = Object(uuid__WEBPACK_IMPORTED_MODULE_1__["v4"])();
            conditionUUid = Object(uuid__WEBPACK_IMPORTED_MODULE_1__["v4"])();
            _context.next = 10;
            return axios.post("".concat(satusehatMasterAPIUrl, "/bundle/send-bundle"), {
              'entries': [{
                "resource_type": "Encounter",
                "uuid": encounterUUid,
                "period": {
                  "start": data.period_start,
                  "end": data.period_end
                },
                "encounter_class_id": data.encounter_class_id,
                "encounter_status_id": data.encounter_status_id,
                "patient_mr_number": data.patient.mr_number,
                "participant_uuid": practitionerUuid,
                "location_uuid": data.location_uuid,
                "status_history": data.encounter_status_history,
                "diagnosises": [{
                  "encounter_diagnosis_use_id": 2,
                  //DD / Discharge Diagnosis
                  "encounter_diagnosis_uuid": conditionUUid,
                  "encounter_diagnosis_display": data.icd10_diagnosis_description,
                  "encounter_diagnosis_rank": 1
                }]
              }, {
                "resource_type": "Condition",
                "uuid": conditionUUid,
                "condition_clinicalstatus_id": data.condition_clinicalstatus_id,
                "condition_category_id": data.condition_category_id,
                "icd10_code": data.icd10_diagnosis_code,
                "patient_mr_number": data.patient.mr_number
              }]
            }, config);

          case 10:
            response = _context.sent;

            if (!(response.data.status == 'success')) {
              _context.next = 16;
              break;
            }

            _context.next = 14;
            return axios.post("api/exam/".concat(data.id, "/satusehat-integrated"), {});

          case 14:
            _response = _context.sent;
            console.log(_response);

          case 16:
            return _context.abrupt("return", response);

          case 17:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));
  return _sendBundle.apply(this, arguments);
}

function generateToken() {
  return _generateToken.apply(this, arguments);
}

function _generateToken() {
  _generateToken = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
    var response;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            _context2.next = 2;
            return axios.post("".concat(satusehatMasterAPIUrl, "/spa-login"), {
              'username': solamedicUsername,
              'password': solamedicPassword
            });

          case 2:
            response = _context2.sent;

            if (!(response.status == 200)) {
              _context2.next = 10;
              break;
            }

            localStorage.setItem('satusehatBearerToken', JSON.stringify(response.data.token));
            _context2.next = 7;
            return axios.post("api/token/save-solamedic-token", {
              solamedicToken: response.data.token
            });

          case 7:
            return _context2.abrupt("return", response.data.token);

          case 10:
            return _context2.abrupt("return", "error");

          case 11:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _generateToken.apply(this, arguments);
}

function getToken() {
  return _getToken.apply(this, arguments);
}

function _getToken() {
  _getToken = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
    var response;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
      while (1) {
        switch (_context3.prev = _context3.next) {
          case 0:
            _context3.next = 2;
            return axios.get("api/token/get-solamedic-token");

          case 2:
            response = _context3.sent;

            if (!(response.status == 200)) {
              _context3.next = 8;
              break;
            }

            localStorage.setItem('satusehatBearerToken', JSON.stringify(response.data));
            return _context3.abrupt("return", response.data);

          case 8:
            return _context3.abrupt("return", "error");

          case 9:
          case "end":
            return _context3.stop();
        }
      }
    }, _callee3);
  }));
  return _getToken.apply(this, arguments);
}

function sendNewPatient(_x2) {
  return _sendNewPatient.apply(this, arguments);
}

function _sendNewPatient() {
  _sendNewPatient = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4(data) {
    var config, payload, response, _response2;

    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
      while (1) {
        switch (_context4.prev = _context4.next) {
          case 0:
            config = getConfig();
            payload = {
              "is_newborn": false,
              //karena case klinik og jadi tidak ada kasus pendaftaran pasien yang baru lahir
              "nik": data.nik,
              "gender": "female",
              "active": true,
              "name": data.name,
              "mr_number": data.mr_number,
              "birthdate": data.birth_date
            };
            _context4.next = 4;
            return axios.post("".concat(satusehatMasterAPIUrl, "/patient/register-patient"), payload, config);

          case 4:
            response = _context4.sent;

            if (!(response.data.status == 'success')) {
              _context4.next = 10;
              break;
            }

            _context4.next = 8;
            return axios.post("api/patient/".concat(data.id, "/satusehat-integrated"), {});

          case 8:
            _response2 = _context4.sent;
            console.log(_response2);

          case 10:
            return _context4.abrupt("return", response);

          case 11:
          case "end":
            return _context4.stop();
        }
      }
    }, _callee4);
  }));
  return _sendNewPatient.apply(this, arguments);
}

function getConditionClinicalStatus() {
  return _getConditionClinicalStatus.apply(this, arguments);
}

function _getConditionClinicalStatus() {
  _getConditionClinicalStatus = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee5() {
    var token, config;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee5$(_context5) {
      while (1) {
        switch (_context5.prev = _context5.next) {
          case 0:
            token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
            config = {
              headers: {
                Authorization: "Bearer ".concat(token)
              }
            };
            return _context5.abrupt("return", axios.get("".concat(satusehatMasterAPIUrl, "/condition/get-condition-clinical-status"), config));

          case 3:
          case "end":
            return _context5.stop();
        }
      }
    }, _callee5);
  }));
  return _getConditionClinicalStatus.apply(this, arguments);
}

;
function getConditionCategory() {
  return _getConditionCategory.apply(this, arguments);
}

function _getConditionCategory() {
  _getConditionCategory = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee6() {
    var token, config;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee6$(_context6) {
      while (1) {
        switch (_context6.prev = _context6.next) {
          case 0:
            token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
            config = {
              headers: {
                Authorization: "Bearer ".concat(token)
              }
            };
            return _context6.abrupt("return", axios.get("".concat(satusehatMasterAPIUrl, "/condition/get-condition-category"), config));

          case 3:
          case "end":
            return _context6.stop();
        }
      }
    }, _callee6);
  }));
  return _getConditionCategory.apply(this, arguments);
}

;
function getEncounterStatus() {
  return _getEncounterStatus.apply(this, arguments);
}

function _getEncounterStatus() {
  _getEncounterStatus = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee7() {
    var token, config;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee7$(_context7) {
      while (1) {
        switch (_context7.prev = _context7.next) {
          case 0:
            token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
            config = {
              headers: {
                Authorization: "Bearer ".concat(token)
              }
            };
            return _context7.abrupt("return", axios.get("".concat(satusehatMasterAPIUrl, "/encounter/get-encounter-status"), config));

          case 3:
          case "end":
            return _context7.stop();
        }
      }
    }, _callee7);
  }));
  return _getEncounterStatus.apply(this, arguments);
}

;
function getEncounterClass() {
  return _getEncounterClass.apply(this, arguments);
}

function _getEncounterClass() {
  _getEncounterClass = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee8() {
    var token, config;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee8$(_context8) {
      while (1) {
        switch (_context8.prev = _context8.next) {
          case 0:
            token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
            config = {
              headers: {
                Authorization: "Bearer ".concat(token)
              }
            };
            return _context8.abrupt("return", axios.get("".concat(satusehatMasterAPIUrl, "/encounter/get-encounter-class"), config));

          case 3:
          case "end":
            return _context8.stop();
        }
      }
    }, _callee8);
  }));
  return _getEncounterClass.apply(this, arguments);
}

;
function getLocations() {
  return _getLocations.apply(this, arguments);
}

function _getLocations() {
  _getLocations = _asyncToGenerator(
  /*#__PURE__*/
  _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee9() {
    var token, config;
    return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee9$(_context9) {
      while (1) {
        switch (_context9.prev = _context9.next) {
          case 0:
            token = localStorage.getItem('satusehatBearerToken').slice(1, -1);
            config = {
              headers: {
                Authorization: "Bearer ".concat(token)
              }
            };
            return _context9.abrupt("return", axios.get("".concat(satusehatMasterAPIUrl, "/location"), config));

          case 3:
          case "end":
            return _context9.stop();
        }
      }
    }, _callee9);
  }));
  return _getLocations.apply(this, arguments);
}

;

/***/ })

}]);