(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[151],{

/***/ "../coreui/src/views/ag-component/RemoveButton.vue":
/*!*********************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=template&id=030e58b5& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony import */ var _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=script&lang=js& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/ag-component/RemoveButton.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!****************************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=template&id=030e58b5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/sales/CreateSales.vue":
/*!*************************************************!*\
  !*** ../coreui/src/views/sales/CreateSales.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateSales_vue_vue_type_template_id_8da26b9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateSales.vue?vue&type=template&id=8da26b9a& */ "../coreui/src/views/sales/CreateSales.vue?vue&type=template&id=8da26b9a&");
/* harmony import */ var _CreateSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateSales.vue?vue&type=script&lang=js& */ "../coreui/src/views/sales/CreateSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CreateSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateSales_vue_vue_type_template_id_8da26b9a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateSales_vue_vue_type_template_id_8da26b9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/sales/CreateSales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/sales/CreateSales.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ../coreui/src/views/sales/CreateSales.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./CreateSales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/CreateSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/sales/CreateSales.vue?vue&type=template&id=8da26b9a&":
/*!********************************************************************************!*\
  !*** ../coreui/src/views/sales/CreateSales.vue?vue&type=template&id=8da26b9a& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateSales_vue_vue_type_template_id_8da26b9a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./CreateSales.vue?vue&type=template&id=8da26b9a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/CreateSales.vue?vue&type=template&id=8da26b9a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateSales_vue_vue_type_template_id_8da26b9a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateSales_vue_vue_type_template_id_8da26b9a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "../coreui/node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
  name: 'RemoveButton',
  data: function data() {
    return {// dataArray: this.params.dataArray(),
      // deleteDataArray: this.params.deleteDataArray(),
    };
  },
  mounted: function mounted() {
    console.log(this.params);
  },
  methods: {
    removeItem: function removeItem() {
      this.params.removeItemCallback(this.params.data, this.params.node.id, this.params.dataArray(), this.params.deleteDataArray()); // this.$eventHub.$emit('remove-item',this.params.data, this.params.node.id)
    }
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/CreateSales.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/sales/CreateSales.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchCustomer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchCustomer */ "../coreui/src/views/components/SearchCustomer.vue");
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
/* harmony import */ var _components_SalesCashPayment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/SalesCashPayment */ "../coreui/src/views/components/SalesCashPayment.vue");
/* harmony import */ var _components_SalesReceivablePayment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/SalesReceivablePayment */ "../coreui/src/views/components/SalesReceivablePayment.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchCustomer: _components_SearchCustomer__WEBPACK_IMPORTED_MODULE_1__["default"],
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_2__["default"],
    SalesCashPayment: _components_SalesCashPayment__WEBPACK_IMPORTED_MODULE_3__["default"],
    SalesReceivablePayment: _components_SalesReceivablePayment__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_5__["default"]],
  data: function data() {
    var _this = this;

    return {
      form: {
        // add more form attribute here
        customer: {},
        detail: [],
        sales_date: this.$moment().format('YYYY-MM-DD'),
        discount_total: 0,
        net_total: 0,
        grand_total: 0,
        print_receipt: true
      },
      errors: {},
      disableButton: false,
      mechanicList: [],
      locationList: [],
      scanCode: null,
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: '⌧',
        colId: 'removeButton',
        width: 35,
        cellRendererFramework: _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_6__["default"],
        cellRendererParams: {
          removeItemCallback: this.removeItem,
          dataArray: function dataArray() {
            return _this.form.detail;
          },
          deleteDataArray: function deleteDataArray() {
            return _this.form.delete_detail;
          }
        }
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 100
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50,
        editable: true,
        valueSetter: function valueSetter(params) {
          params.data['qty'] = Number(params.newValue);
        }
      }, {
        headerName: 'Harga',
        field: 'unit_sales_price',
        width: 90,
        editable: true,
        cellClass: ['text-right'],
        valueSetter: function valueSetter(params) {
          params.data['unit_sales_price'] = Number(params.newValue);
        },
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Diskon [%]',
        field: 'discount_percent',
        width: 75,
        editable: true,
        cellClass: ['text-right'],
        valueSetter: function valueSetter(params) {
          params.data['discount_percent'] = Number(params.newValue);
        },
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'PPN',
        field: 'tax_value',
        width: 90,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Net',
        field: 'net_sales_price',
        width: 90,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Subtotal',
        field: 'subtotal',
        width: 90,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  watch: {
    detail: {
      handler: function handler(val) {
        this.calculateDetail();
        this.refreshCells();
      },
      deep: true
    }
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/sales'})
    },
    store: function store() {
      var _this2 = this;

      this.disableButton = true;
      this.$notify({
        type: 'info',
        title: 'Saving...',
        duration: 3000
      });
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/sales', this.form).then(function (response) {
        _this2.$notify({
          type: 'success',
          title: 'Success!',
          text: 'This data has been saved successfully.'
        });

        _this2.$router.push({
          path: "/sales/".concat(response.data.id)
        });
      })["catch"](function (error) {
        _this2.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this2.errors = error.response.data.errors;
        }
      }).then(function () {
        _this2.disableButton = false;
      });
    },
    openSearchProductModal: function openSearchProductModal() {
      this.$refs['searchProduct'].openModal();
    },
    getProductByCode: function getProductByCode() {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("api/product/get-by-code/".concat(this.scanCode)).then(function (response) {
        if (!_.isEmpty(response.data)) {
          _this3.addDetail(response.data);
        } else {
          _this3.$notify({
            type: 'error',
            title: 'Oh no!',
            text: 'Kode tidak ditemukan'
          });
        }
      })["catch"](function (error) {
        _this3.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.data
        });
      }).then(function () {
        _this3.scanCode = '';
      });
    },
    addDetail: function addDetail(product) {
      var foundRow = this.form.detail.find(function (x) {
        return x.product_id == product.id;
      });

      if (foundRow) {
        return;
      }

      this.form.detail.push({
        seq_no: this.form.detail.length + 1,
        product_id: product.id,
        product_code: product.sku_code,
        product_name: product.name,
        qty: 1,
        unit_sales_price: Number(product.sales_price),
        discount_percent: 0,
        discount_value: 0,
        tax_percent: 10,
        tax_value: 0,
        subtotal: 0
      });
    },
    calculateItemDiscount: function calculateItemDiscount(item) {
      return Number(item.discount_percent) / 100 * Number(item.unit_sales_price);
    },
    calculateItemTaxValue: function calculateItemTaxValue(item) {
      return Number(item.net_sales_price) * Number(item.tax_percent) / 100;
    },
    calculateItemNetPrice: function calculateItemNetPrice(item) {
      return Number(item.net_sales_price) + Number(item.tax_value);
    },
    calculateItemSubtotal: function calculateItemSubtotal(item) {
      return Number(item.qty) * Number(item.net_sales_price);
    },
    calculateDetail: function calculateDetail() {
      var _this4 = this;

      this.form.gross_total = 0;
      this.form.net_total = 0;
      this.form.tax_total = 0;
      this.form.grand_total = 0;
      this.form.discount_total = 0;

      _.forEach(this.form.detail, function (item, index) {
        item.discount_value = _this4.calculateItemDiscount(item);
        item.net_sales_price = item.unit_sales_price - item.discount_value;
        item.tax_value = _this4.calculateItemTaxValue(item);
        item.net_sales_price = _this4.calculateItemNetPrice(item);
        item.subtotal = _this4.calculateItemSubtotal(item);
        _this4.form.gross_total += item.unit_sales_price * item.qty;
        _this4.form.net_total += item.subtotal;
        _this4.form.tax_total += item.tax_value * item.qty;
        _this4.form.discount_total += item.discount_value * item.qty;
      });

      this.form.grand_total = this.form.net_total;
    },
    removeItem: function removeItem(item, index, arr) {
      var arrDelete = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      if (item.id && arrDelete) {
        arrDelete.push(item.id);
      }

      arr.splice(index, 1);
      this.updateSeqNumber();
    },
    openSearchCustomerModal: function openSearchCustomerModal() {
      this.$refs['searchCustomer'].openModal();
    },
    selectCustomer: function selectCustomer(customer) {
      this.form.customer = customer;
      this.form.customer_id = customer.id;
    },
    onChangeDiscount: function onChangeDiscount() {
      this.form.grand_total = this.form.net_total - this.form.discount_total;
    },
    openCashPaymentModal: function openCashPaymentModal() {
      this.$refs['salesCashPayment'].openModal();
    },
    openReceivablePaymentModal: function openReceivablePaymentModal() {
      this.$refs['salesReceivablePayment'].openModal();
    },
    print: function print() {
      this.$eventHub.$emit('send-to-print', {
        url: "api/sales/".concat(this.$route.params.id, "/print-invoice"),
        type: 'GET'
      });
    }
  },
  mounted: function mounted() {
    var _this5 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/sales/create').then(function (response) {
      _this5.mechanicList = response.data.mechanic;
      _this5.locationList = Object.freeze(response.data.location);
      _this5.form.location_id = _this5.locationList[0].id;
    })["catch"](function (error) {
      console.log(error);

      _this5.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
    this.$refs['searchCustomer'].$on('customer-selected', function (eventData) {
      if (_this5.form.customer_id == eventData.id) {
        return;
      }

      _this5.selectCustomer(eventData);
    });
    this.$refs['searchProduct'].$on('product-selected', function (eventData) {
      _.forEach(eventData, function (item, index) {
        _this5.addDetail(item);
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "button",
    {
      staticClass: "btn btn-sm btn-danger",
      attrs: { type: "button" },
      on: { click: _vm.removeItem }
    },
    [_c("i", { staticClass: "fas fa-times" })]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/CreateSales.vue?vue&type=template&id=8da26b9a&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/sales/CreateSales.vue?vue&type=template&id=8da26b9a& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Create Sales")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Customer")]
                              ),
                              _vm._v(" "),
                              _c("span", { staticClass: "col-sm-8" }, [
                                _c("div", { staticClass: "input-group" }, [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.customer.name,
                                        expression: "form.customer.name"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.customer_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: { type: "text", readonly: "" },
                                    domProps: { value: _vm.form.customer.name },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form.customer,
                                          "name",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "div",
                                    { staticClass: "input-group-append" },
                                    [
                                      _c(
                                        "button",
                                        {
                                          staticClass:
                                            "btn btn-outline-primary",
                                          attrs: { type: "button" },
                                          on: {
                                            click: _vm.openSearchCustomerModal
                                          }
                                        },
                                        [_vm._v("Cari")]
                                      )
                                    ]
                                  )
                                ])
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Mekanik *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.mechanicList,
                                      reduce: function(mechanic) {
                                        return mechanic.id
                                      },
                                      label: "name",
                                      name: "mechanic"
                                    },
                                    model: {
                                      value: _vm.form.mechanic_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "mechanic_id", $$v)
                                      },
                                      expression: "form.mechanic_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.locationList,
                                      reduce: function(location) {
                                        return location.id
                                      },
                                      label: "name",
                                      name: "location"
                                    },
                                    model: {
                                      value: _vm.form.location_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "location_id", $$v)
                                      },
                                      expression: "form.location_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.sales_date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY"
                                    },
                                    model: {
                                      value: _vm.form.sales_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "sales_date", $$v)
                                      },
                                      expression: "form.sales_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.sales_date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        { staticClass: "mb-2" },
                        [
                          _c("CCol", { attrs: { col: "6", lg: "4" } }, [
                            _c("div", { staticClass: "input-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.scanCode,
                                    expression: "scanCode"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder:
                                    "Scan / masukkan kode SKU barang..."
                                },
                                domProps: { value: _vm.scanCode },
                                on: {
                                  keypress: function($event) {
                                    if (
                                      !$event.type.indexOf("key") &&
                                      _vm._k(
                                        $event.keyCode,
                                        "enter",
                                        13,
                                        $event.key,
                                        "Enter"
                                      )
                                    ) {
                                      return null
                                    }
                                    $event.preventDefault()
                                    return _vm.getProductByCode($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.scanCode = $event.target.value
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "input-group-append" },
                                [
                                  _c(
                                    "CButton",
                                    {
                                      attrs: {
                                        color: "primary",
                                        disabled: _vm.disableButton
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openSearchProductModal()
                                        }
                                      }
                                    },
                                    [
                                      _c("i", { staticClass: "fas fa-plus" }),
                                      _vm._v(" Product\n                  ")
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "12", lg: "4" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-sm-4 col-form-label text-right"
                                },
                                [_vm._v("Total Gros")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.gross_total,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "gross_total", $$v)
                                      },
                                      expression: "form.gross_total"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-sm-4 col-form-label text-right"
                                },
                                [_vm._v("Total Diskon")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.discount_total,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.form,
                                          "discount_total",
                                          $$v
                                        )
                                      },
                                      expression: "form.discount_total"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-sm-4 col-form-label text-right"
                                },
                                [_vm._v("Total PPN")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.tax_total,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "tax_total", $$v)
                                      },
                                      expression: "form.tax_total"
                                    }
                                  })
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "12", lg: "4" } }),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "12", lg: "4" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-form-label col-sm-4 text-right"
                                },
                                [_vm._v("Grand Total")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext form-control-lg text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.grand_total,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "grand_total", $$v)
                                      },
                                      expression: "form.grand_total"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.grand_total, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.openCashPaymentModal }
                        },
                        [_vm._v("Bayar Tunai")]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton || !_vm.form.customer_id
                          },
                          on: { click: _vm.openReceivablePaymentModal }
                        },
                        [_vm._v("Bayar Piutang")]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Batal")]
                      ),
                      _vm._v(" "),
                      _c("span", { staticClass: "float-right" }, [
                        _c(
                          "div",
                          {
                            staticClass: "btn-group-toggle",
                            attrs: { "data-toggle": "buttons" }
                          },
                          [
                            _c(
                              "label",
                              {
                                class: [
                                  "btn",
                                  _vm.form.print_receipt
                                    ? "btn-primary"
                                    : "btn-outline-dark"
                                ]
                              },
                              [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.form.print_receipt,
                                      expression: "form.print_receipt"
                                    }
                                  ],
                                  attrs: { type: "checkbox" },
                                  domProps: {
                                    checked: Array.isArray(
                                      _vm.form.print_receipt
                                    )
                                      ? _vm._i(_vm.form.print_receipt, null) >
                                        -1
                                      : _vm.form.print_receipt
                                  },
                                  on: {
                                    change: function($event) {
                                      var $$a = _vm.form.print_receipt,
                                        $$el = $event.target,
                                        $$c = $$el.checked ? true : false
                                      if (Array.isArray($$a)) {
                                        var $$v = null,
                                          $$i = _vm._i($$a, $$v)
                                        if ($$el.checked) {
                                          $$i < 0 &&
                                            _vm.$set(
                                              _vm.form,
                                              "print_receipt",
                                              $$a.concat([$$v])
                                            )
                                        } else {
                                          $$i > -1 &&
                                            _vm.$set(
                                              _vm.form,
                                              "print_receipt",
                                              $$a
                                                .slice(0, $$i)
                                                .concat($$a.slice($$i + 1))
                                            )
                                        }
                                      } else {
                                        _vm.$set(_vm.form, "print_receipt", $$c)
                                      }
                                    }
                                  }
                                }),
                                _vm._v(" "),
                                _c("i", {
                                  class: [
                                    "fas",
                                    _vm.form.print_receipt
                                      ? "fa-check-square"
                                      : "fa-square"
                                  ]
                                }),
                                _vm._v(" Cetak Nota\n              ")
                              ]
                            )
                          ]
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchCustomer", { ref: "searchCustomer" }),
      _vm._v(" "),
      _c("SearchProduct", {
        ref: "searchProduct",
        attrs: { allowSelectMultiple: true }
      }),
      _vm._v(" "),
      _c("SalesCashPayment", {
        ref: "salesCashPayment",
        attrs: { formData: _vm.form }
      }),
      _vm._v(" "),
      _c("SalesReceivablePayment", {
        ref: "salesReceivablePayment",
        attrs: { formData: _vm.form }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);