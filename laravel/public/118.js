(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[118],{

/***/ "../coreui/src/views/purchase_return/ShowPurchaseReturn.vue":
/*!******************************************************************!*\
  !*** ../coreui/src/views/purchase_return/ShowPurchaseReturn.vue ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowPurchaseReturn_vue_vue_type_template_id_8e28b1ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce& */ "../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce&");
/* harmony import */ var _ShowPurchaseReturn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowPurchaseReturn.vue?vue&type=script&lang=js& */ "../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowPurchaseReturn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowPurchaseReturn_vue_vue_type_template_id_8e28b1ce___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowPurchaseReturn_vue_vue_type_template_id_8e28b1ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/purchase_return/ShowPurchaseReturn.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************!*\
  !*** ../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchaseReturn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowPurchaseReturn.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchaseReturn_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce&":
/*!*************************************************************************************************!*\
  !*** ../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce& ***!
  \*************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchaseReturn_vue_vue_type_template_id_8e28b1ce___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchaseReturn_vue_vue_type_template_id_8e28b1ce___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPurchaseReturn_vue_vue_type_template_id_8e28b1ce___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
/* harmony import */ var _components_SearchPurchase__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/SearchPurchase */ "../coreui/src/views/components/SearchPurchase.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__["default"],
    SearchPurchase: _components_SearchPurchase__WEBPACK_IMPORTED_MODULE_2__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_3__["default"]],
  data: function data() {
    return {
      form: {
        // add more form attribute here
        purchase: {},
        detail: []
      },
      errors: {},
      disableButton: false,
      locationList: [],
      supplierList: [],
      scanCode: '',
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 120
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50
      }, {
        headerName: 'Harga',
        field: 'unit_purchase_price',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Subtotal',
        field: 'subtotal',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  watch: {
    detail: {
      handler: function handler(val) {
        this.calculateDetail();
        this.refreshCells();
      },
      deep: true
    }
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/purchase-return'})
    },
    update: function update() {
      var _this = this;

      this.disableButton = true;
      this.$notify({
        type: 'info',
        title: 'Saving...',
        duration: 3000
      });
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put('/api/purchase-return/' + this.$route.params.id, this.form).then(function (response) {
        _this.$notify({
          type: 'success',
          title: 'Success!',
          text: 'This data has been saved successfully.'
        });

        _this.forceRerender();
      })["catch"](function (error) {
        _this.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this.errors = error.response.data.errors;
        }
      }).then(function () {
        _this.disableButton = false;
      });
    },
    openSearchProductModal: function openSearchProductModal() {
      this.$refs['searchProduct'].openModal();
    },
    openSearchPurchaseModal: function openSearchPurchaseModal() {
      this.$refs['searchPurchase'].openModal();
    },
    retrievePurchase: function retrievePurchase(purchaseCode) {
      var _this2 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("/api/purchase/".concat(purchaseCode, "/get-by-code")).then(function (response) {
        _this2.usePurchase(response.data);
      });
    },
    usePurchase: function usePurchase(purchase) {
      var _this3 = this;

      console.log(purchase);
      this.form.purchase_id = purchase.id;
      this.form.location_id = purchase.location_id;
      this.form.supplier_id = purchase.supplier_id;
      this.form.purchase = purchase;
      this.form.detail = [];

      _.forEach(purchase.detail, function (item, index) {
        _this3.form.detail.push({
          seq_no: _this3.form.detail.length + 1,
          purchase_detail_id: item.id,
          product_id: item.product_id,
          product_code: item.product_code,
          product_name: item.product_name,
          qty: item.qty,
          unit_purchase_price: item.unit_purchase_price,
          subtotal: item.subtotal
        });
      });
    },
    calculateItemSubtotal: function calculateItemSubtotal(item) {
      return item.qty * item.unit_purchase_price;
    },
    calculateDetail: function calculateDetail() {
      var _this4 = this;

      this.form.grand_total = 0;

      _.forEach(this.form.detail, function (item, index) {
        item.subtotal = _this4.calculateItemSubtotal(item);
        _this4.form.grand_total += item.subtotal;
      });
    },
    process: function process() {
      var _this5 = this;

      this.$dialog.confirm('Are you sure you want to process this data?').then(function (dialog) {
        _this5.disableButton = true;

        _this5.$notify({
          type: 'info',
          title: 'Saving...',
          duration: 3000
        });

        axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/purchase-return/' + _this5.$route.params.id + '/process', _this5.form).then(function (response) {
          _this5.$notify({
            type: 'success',
            title: 'Success!',
            text: 'This data has been saved successfully.'
          });

          _this5.$router.push({
            path: '/purchase-return/' + _this5.$route.params.id
          });
        })["catch"](function (error) {
          _this5.$notify({
            type: 'error',
            title: 'Oh no!',
            text: error.response.data.message
          });

          if (error.response.status == 422) {
            _this5.errors = error.response.data.errors;
          }
        }).then(function () {
          _this5.disableButton = false;
        });
      })["catch"](function () {});
    }
  },
  mounted: function mounted() {
    var _this6 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/purchase-return/' + this.$route.params.id + '/edit').then(function (response) {
      _this6.form = response.data.purchaseReturn;
      _this6.supplierList = Object.freeze(response.data.supplier);
      _this6.locationList = Object.freeze(response.data.location);
    })["catch"](function (error) {
      console.log(error);

      _this6.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce&":
/*!*******************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/purchase_return/ShowPurchaseReturn.vue?vue&type=template&id=8e28b1ce& ***!
  \*******************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Show Purchase Return")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Kode Retur")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.code,
                                        expression: "form.code"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      readonly: "",
                                      placeholder: "(autogenerate)"
                                    },
                                    domProps: { value: _vm.form.code },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.code, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal Retur *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.purchase_return_date
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.purchase_return_date,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.form,
                                          "purchase_return_date",
                                          $$v
                                        )
                                      },
                                      expression: "form.purchase_return_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(
                                    _vm.errors.purchase_return_date,
                                    function(item) {
                                      return _c(
                                        "div",
                                        { staticClass: "text-danger small" },
                                        [_vm._v(_vm._s(item))]
                                      )
                                    }
                                  )
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Pilih Pembelian")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.purchase.code,
                                        expression: "form.purchase.code"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.po_id ? " is-invalid" : ""
                                    ],
                                    attrs: { type: "text", readonly: "" },
                                    domProps: { value: _vm.form.purchase.code },
                                    on: {
                                      keypress: function($event) {
                                        if (
                                          !$event.type.indexOf("key") &&
                                          _vm._k(
                                            $event.keyCode,
                                            "enter",
                                            13,
                                            $event.key,
                                            "Enter"
                                          )
                                        ) {
                                          return null
                                        }
                                        $event.preventDefault()
                                        return _vm.retrievePurchase(
                                          _vm.form.purchase.code
                                        )
                                      },
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form.purchase,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.po_id, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.locationList,
                                      reduce: function(location) {
                                        return location.id
                                      },
                                      label: "name",
                                      name: "location",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.location_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "location_id", $$v)
                                      },
                                      expression: "form.location_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Supplier *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.supplier_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.supplierList,
                                      reduce: function(supplier) {
                                        return supplier.id
                                      },
                                      label: "name",
                                      name: "supplier",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.supplier_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "supplier_id", $$v)
                                      },
                                      expression: "form.supplier_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.supplier_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c(
                            "CCol",
                            { attrs: { col: "6", lg: "6" } },
                            [
                              _c(
                                "CCard",
                                [
                                  _c("CCardHeader", [
                                    _vm._v(
                                      "\n                  Info Nota Pembelian\n                "
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _c("CCardBody", [
                                    _c(
                                      "div",
                                      { staticClass: "form-group row" },
                                      [
                                        _c(
                                          "label",
                                          {
                                            staticClass:
                                              "col-sm-4 col-form-label",
                                            attrs: { for: "name" }
                                          },
                                          [_vm._v("Total Pembelian")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "col-sm-8" },
                                          [
                                            _c("cleave", {
                                              class: [
                                                "form-control",
                                                "text-right"
                                              ],
                                              attrs: {
                                                options: {
                                                  numeral: true,
                                                  numeralDecimalScale: 0
                                                },
                                                readonly: ""
                                              },
                                              model: {
                                                value:
                                                  _vm.form.purchase.grand_total,
                                                callback: function($$v) {
                                                  _vm.$set(
                                                    _vm.form.purchase,
                                                    "grand_total",
                                                    $$v
                                                  )
                                                },
                                                expression:
                                                  "form.purchase.grand_total"
                                              }
                                            })
                                          ],
                                          1
                                        )
                                      ]
                                    ),
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      { staticClass: "form-group row" },
                                      [
                                        _c(
                                          "label",
                                          {
                                            staticClass:
                                              "col-form-label col-sm-4"
                                          },
                                          [_vm._v("Keterangan")]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "span",
                                          { staticClass: "col-sm-8" },
                                          [
                                            _c("textarea", {
                                              directives: [
                                                {
                                                  name: "model",
                                                  rawName: "v-model",
                                                  value: _vm.form.remarks,
                                                  expression: "form.remarks"
                                                }
                                              ],
                                              class: [
                                                "form-control",
                                                _vm.errors.remarks
                                                  ? " is-invalid"
                                                  : ""
                                              ],
                                              attrs: {
                                                cols: "30",
                                                readonly: ""
                                              },
                                              domProps: {
                                                value: _vm.form.remarks
                                              },
                                              on: {
                                                input: function($event) {
                                                  if ($event.target.composing) {
                                                    return
                                                  }
                                                  _vm.$set(
                                                    _vm.form,
                                                    "remarks",
                                                    $event.target.value
                                                  )
                                                }
                                              }
                                            })
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _vm._l(_vm.errors.remarks, function(
                                          item
                                        ) {
                                          return _c(
                                            "div",
                                            {
                                              staticClass: "text-danger small"
                                            },
                                            [_vm._v(_vm._s(item))]
                                          )
                                        })
                                      ],
                                      2
                                    )
                                  ])
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchPurchase", { ref: "searchPurchase" }),
      _vm._v(" "),
      _c("SearchProduct", {
        ref: "searchProduct",
        attrs: { allowSelectMultiple: true }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);