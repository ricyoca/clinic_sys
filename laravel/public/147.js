(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[147],{

/***/ "../coreui/src/mixins/ag-grid-options.js":
/*!***********************************************!*\
  !*** ../coreui/src/mixins/ag-grid-options.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ag-grid-vue */ "../coreui/node_modules/ag-grid-vue/main.js");
/* harmony import */ var ag_grid_vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! numeral */ "../coreui/node_modules/numeral/numeral.js");
/* harmony import */ var numeral__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(numeral__WEBPACK_IMPORTED_MODULE_1__);


/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    AgGridVue: ag_grid_vue__WEBPACK_IMPORTED_MODULE_0__["AgGridVue"]
  },
  data: function data() {
    return {
      gridOptions: null
    };
  },
  beforeMount: function beforeMount() {
    var _this = this;

    this.gridOptions = {
      headerHeight: 35,
      rowHeight: 30,
      rowSelection: 'multiple',
      stopEditingWhenGridLosesFocus: true,
      suppressScrollOnNewData: true,
      defaultColDef: {
        tooltipValueGetter: function tooltipValueGetter(params) {
          if (_this.errors && _this.errors['detail.' + params.node.rowIndex + '.' + params.colDef.field]) {
            return _this.errors['detail.' + params.node.rowIndex + '.' + params.colDef.field][0];
          }

          if (params.valueFormatted === '-' && params.colDef.editable) {
            return '(Click to Edit)';
          }

          if (params.valueFormatted !== null) return params.valueFormatted;
          if (params.value !== undefined && params.value !== null) return params.value.toString();
          return ' ';
        },
        cellClassRules: {
          'editable-grid-cell': function editableGridCell(params) {
            return params.colDef.editable == true || typeof params.colDef.editable == 'function' && params.colDef.editable(params) == true;
          },
          'bg-danger': function bgDanger(params) {
            return _this.errors && _this.errors['detail.' + params.rowIndex + '.' + params.colDef.field];
          }
        },
        valueFormatter: function valueFormatter(params) {
          if (!params.value) {
            return '-';
          }

          return params.value;
        },
        singleClickEdit: true,
        resizable: true
      },
      enterMovesDownAfterEdit: true,
      rowClassRules: {
        'locked-price-row': function lockedPriceRow(params) {
          return params.data.hasOwnProperty('will_update_sales_price') && params.data.will_update_sales_price == false;
        }
      },
      // onRowDataChanged: (event) => {
      //     let lastIndex = event.api.getModel().allChildrenCount - 1;
      //     event.api.ensureIndexVisible(lastIndex,'bottom');
      // },
      context: {
        componentParent: this
      },
      navigateToNextCell: function navigateToNextCell(params) {
        var previousCell = params.previousCellPosition;
        var suggestedNextCell = params.nextCellPosition;
        var KEY_UP = 38;
        var KEY_DOWN = 40;
        var KEY_LEFT = 37;
        var KEY_RIGHT = 39;

        switch (params.key) {
          case KEY_DOWN:
            var previousNode = previousCell.column.gridApi.getRowNode(previousCell.rowIndex);
            previousCell = params.previousCellPosition; // set selected cell on current cell + 1

            previousCell.column.gridApi.forEachNode(function (node) {
              if (previousCell.rowIndex + 1 === node.rowIndex) {
                if (params.event.shiftKey) {
                  if (node.isSelected()) {
                    previousNode.setSelected(false);
                  } else {
                    node.setSelected(true);
                  }
                } else node.setSelected(true, true);
              }
            });
            return suggestedNextCell;

          case KEY_UP:
            var previousNode = previousCell.column.gridApi.getRowNode(previousCell.rowIndex);
            previousCell = params.previousCellPosition; // set selected cell on current cell - 1

            previousCell.column.gridApi.forEachNode(function (node) {
              if (previousCell.rowIndex - 1 === node.rowIndex) {
                if (params.event.shiftKey) {
                  if (node.isSelected()) {
                    previousNode.setSelected(false);
                  } else {
                    node.setSelected(true);
                  }
                } else node.setSelected(true, true);
              }
            });
            return suggestedNextCell;

          case KEY_LEFT:
          case KEY_RIGHT:
            return suggestedNextCell;

          default:
            throw "this will never happen, navigation is always one of the 4 keys above";
        }
      },
      onCellKeyDown: function onCellKeyDown(e) {
        e.event.preventDefault();
        var keyPressed = e.event.key;

        switch (keyPressed) {
          case 'Delete':
            var removeCol = e.columnApi.getColumn('removeButton');
            if (!removeCol) return;

            if (typeof removeCol.colDef.cellRendererParams.removeItemCallback == 'function') {
              // if(!confirm("Are you sure you want to delete the selected rows?")) return;
              var selectedNodes = e.api.getSelectedNodes().reverse();

              _.forEach(selectedNodes, function (item, index) {
                removeCol.colDef.cellRendererParams.removeItemCallback(item.data, item.id, removeCol.colDef.cellRendererParams.dataArray(), removeCol.colDef.cellRendererParams.deleteDataArray());
              });
            }

            break;

          case 'F3': // e.event.originalEvent.keyCode = 0;

          default:
            break;
        }
      } //     suppressKeyboardEvent: (params) => {
      //         var KEY_A = 65;
      //         var KEY_C = 67;
      //         var KEY_V = 86;
      //         var KEY_D = 68;
      //         var KEY_PAGE_UP = 33;
      //         var KEY_PAGE_DOWN = 34;
      //         var KEY_TAB = 9;
      //         var KEY_LEFT = 37;
      //         var KEY_UP = 38;
      //         var KEY_RIGHT = 39;
      //         var KEY_DOWN = 40;
      //         var KEY_F2 = 113;
      //         var KEY_F3 = 114;
      //         var KEY_BACKSPACE = 8;
      //         var KEY_ESCAPE = 27;
      //         var KEY_SPACE = 32;
      //         var KEY_DELETE = 46;
      //         var KEY_PAGE_HOME = 36;
      //         var KEY_PAGE_END = 35;
      //         var event = params.event;
      //         var key = event.which;
      //         var keysToSuppress = [
      //           KEY_PAGE_UP,
      //           KEY_PAGE_DOWN,
      //           KEY_TAB,
      //           KEY_F3,
      //           KEY_ESCAPE,
      //         ];
      //         var editingKeys = [
      //           KEY_LEFT,
      //           KEY_RIGHT,
      //           KEY_UP,
      //           KEY_DOWN,
      //           KEY_BACKSPACE,
      //           KEY_DELETE,
      //           KEY_SPACE,
      //           KEY_PAGE_HOME,
      //           KEY_PAGE_END,
      //         ];
      //         if (event.ctrlKey || event.metaKey) {
      //           keysToSuppress.push(KEY_A);
      //           keysToSuppress.push(KEY_V);
      //           keysToSuppress.push(KEY_C);
      //           keysToSuppress.push(KEY_D);
      //         }
      //         if (!params.editing) {
      //           keysToSuppress = keysToSuppress.concat(editingKeys);
      //         }
      //         var suppress = keysToSuppress.indexOf(key) >= 0;
      //         return suppress;
      //     },

    };
  },
  methods: {
    refreshCells: function refreshCells() {
      this.gridOptions.api.refreshCells(); // this.gridOptions.api.redrawRows();
    },
    redrawRows: function redrawRows() {
      this.gridOptions.api.redrawRows();
    },
    dateComparator: function dateComparator(date1, date2) {
      var date1Number = this.monthToComparableNumber(date1);
      var date2Number = this.monthToComparableNumber(date2);

      if (date1Number === null && date2Number === null) {
        return 0;
      }

      if (date1Number === null) {
        return -1;
      }

      if (date2Number === null) {
        return 1;
      }

      return date1Number - date2Number;
    },
    monthToComparableNumber: function monthToComparableNumber(date) {
      if (date === undefined || date === null || !this.$moment(date).isValid()) {
        return null;
      }

      var yearNumber = this.$moment(date).year();
      var monthNumber = this.$moment(date).month();
      var dayNumber = this.$moment(date).date();
      var result = yearNumber * 10000 + monthNumber * 100 + dayNumber;
      return result;
    },
    updateSeqNumber: function updateSeqNumber() {
      var _this2 = this;

      this.$nextTick(function () {
        var itemsToUpdate = [];

        _this2.gridOptions.api.forEachNodeAfterFilterAndSort(function (rowNode, index) {
          var data = rowNode.data;
          data.seq_no = index + 1;
          itemsToUpdate.push(data);
        });

        var res = _this2.gridOptions.api.applyTransaction({
          update: itemsToUpdate
        });
      });
    },
    numberFormatter: function numberFormatter(params) {
      if (params.value === null || params.value === '' || params.value === undefined) {
        return '-';
      }

      return numeral__WEBPACK_IMPORTED_MODULE_1___default()(params.value).format();
    }
  },
  beforeDestroy: function beforeDestroy() {
    if (this.gridOptions.api) {
      this.gridOptions.api.destroy();
    }
  }
});

/***/ }),

/***/ "../coreui/src/views/components/SearchProduct.vue":
/*!********************************************************!*\
  !*** ../coreui/src/views/components/SearchProduct.vue ***!
  \********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _SearchProduct_vue_vue_type_template_id_bee44550___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./SearchProduct.vue?vue&type=template&id=bee44550& */ "../coreui/src/views/components/SearchProduct.vue?vue&type=template&id=bee44550&");
/* harmony import */ var _SearchProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./SearchProduct.vue?vue&type=script&lang=js& */ "../coreui/src/views/components/SearchProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _SearchProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _SearchProduct_vue_vue_type_template_id_bee44550___WEBPACK_IMPORTED_MODULE_0__["render"],
  _SearchProduct_vue_vue_type_template_id_bee44550___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/components/SearchProduct.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/components/SearchProduct.vue?vue&type=script&lang=js&":
/*!*********************************************************************************!*\
  !*** ../coreui/src/views/components/SearchProduct.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./SearchProduct.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/components/SearchProduct.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchProduct_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/components/SearchProduct.vue?vue&type=template&id=bee44550&":
/*!***************************************************************************************!*\
  !*** ../coreui/src/views/components/SearchProduct.vue?vue&type=template&id=bee44550& ***!
  \***************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchProduct_vue_vue_type_template_id_bee44550___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./SearchProduct.vue?vue&type=template&id=bee44550& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/components/SearchProduct.vue?vue&type=template&id=bee44550&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchProduct_vue_vue_type_template_id_bee44550___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_SearchProduct_vue_vue_type_template_id_bee44550___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/components/SearchProduct.vue?vue&type=script&lang=js&":
/*!*****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/components/SearchProduct.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
var _methods;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_1__["default"]],
  data: function data() {
    return {
      myName: "",
      myMinHeight: "",
      myMinWidth: "",
      myHeight: "",
      myWidth: "",
      myClickToClose: true,
      productList: [],
      params: {
        search_text: ''
      },
      columnDefs: [],
      selectedRowsCount: 0,
      locationList: []
    };
  },
  props: {
    name: {
      type: String,
      "default": "searchProduct"
    },
    minHeight: {
      type: Number,
      "default": 150
    },
    minWidth: {
      type: Number,
      "default": 700
    },
    height: {
      type: String,
      "default": "auto"
    },
    width: {
      type: String,
      "default": "80%"
    },
    clickToClose: {
      type: Boolean,
      "default": false
    },
    title: {
      type: String,
      "default": "Cari Product"
    },
    viewOnly: {
      type: Boolean,
      "default": false
    },
    allowSelectMultiple: {
      type: Boolean,
      "default": false
    }
  },
  computed: {
    selectedProducts: function selectedProducts() {
      return this.productList.filter(function (item) {
        return item.is_selected;
      });
    }
  },
  created: function created() {
    this.myName = this.name;
    this.myMinHeight = this.minHeight;
    this.myMinWidth = this.minWidth;
    this.myHeight = this.height;
    this.myWidth = this.width;
    this.myClickToClose = this.clickToClose;
  },
  methods: (_methods = {
    hideModal: function hideModal() {
      this.$emit("hide-modal");
      this.$modal.hide(this.myName);
    },
    openModal: function openModal() {
      this.myIsLoading = false;
      this.$modal.show(this.myName);
      this.$emit("open-modal");
    },
    beforeOpen: function beforeOpen() {
      var _this = this;

      this.$emit("before-open");
      this.columnDefs = [{
        headerName: 'Kode Produk',
        field: 'sku_code',
        width: 90
      }, {
        headerName: 'Nama Produk',
        field: 'name',
        width: 150
      }, {
        headerName: 'Kategori',
        field: 'subcategory.long_name',
        width: 160
      }, {
        headerName: 'Part Number',
        field: 'part_number',
        width: 100
      }, {
        headerName: 'Merk',
        field: 'brand',
        width: 100
      }, {
        headerName: 'H. Beli',
        field: 'purchase_price',
        width: 140
      }];
      this.fetchLocation().then(function (response) {
        _this.locationList = response.data.rows;

        _.forEach(_this.locationList, function (item, index) {
          var col = {
            headerName: 'Stok ' + item.name,
            width: 100,
            valueGetter: function valueGetter(params) {
              var currentStock = params.data.current_stock.find(function (x) {
                return x.location_id == item.id;
              });
              if (currentStock) return currentStock.qty;
              return null;
            }
          };

          _this.columnDefs.push(col);
        });
      });
      this.gridOptions.rowMultiSelectWithClick = this.allowSelectMultiple;

      this.gridOptions.onSelectionChanged = function (params) {
        _this.selectedRowsCount = _this.gridOptions.api.getSelectedRows().length;
      };
    },
    opened: function opened() {// this.retrieveProduct();
    }
  }, _defineProperty(_methods, "hideModal", function hideModal() {
    this.$emit("hide-modal");
    this.$modal.hide(this.myName);
  }), _defineProperty(_methods, "fetchLocation", function fetchLocation() {
    return axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/location');
  }), _defineProperty(_methods, "retrieveProduct", function retrieveProduct() {
    var _this2 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("api/product", {
      params: this.params
    }).then(function (response) {
      _this2.productList = response.data.rows;
    });
  }), _defineProperty(_methods, "selectProduct", function selectProduct(product) {
    // let selectedProducts = this.productList.filter((item) => item.is_selected)
    if (!this.allowSelectMultiple) this.$emit('product-selected', this.gridOptions.api.getSelectedRows()[0]);else this.$emit('product-selected', this.gridOptions.api.getSelectedRows()); // this.$emit('product-selected', selectedProducts);

    this.$modal.hide(this.myName);
  }), _defineProperty(_methods, "toggleSelectAllItem", function toggleSelectAllItem() {
    var flag = this.productList.length == this.selectedProducts.length ? false : true;

    for (var i = 0; i < this.productList.length; i++) {
      this.$set(this.productList[i], 'is_selected', flag);
    }
  }), _methods)
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/components/SearchProduct.vue?vue&type=template&id=bee44550&":
/*!*********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/components/SearchProduct.vue?vue&type=template&id=bee44550& ***!
  \*********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "modal",
    {
      attrs: {
        name: _vm.myName,
        draggable: ".card-header",
        resizable: true,
        minHeight: _vm.myMinHeight,
        minWidth: _vm.myMinWidth,
        height: _vm.myHeight,
        clickToClose: _vm.myClickToClose,
        width: _vm.myWidth
      },
      on: { "before-open": _vm.beforeOpen, opened: _vm.opened }
    },
    [
      _c(
        "CCard",
        [
          _c(
            "CCardHeader",
            [
              _c("CCardTitle", { staticClass: "mb-0" }, [
                _vm._v(_vm._s(_vm.title) + "\n        "),
                _c("div", { staticClass: "float-right" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-sm btn-danger",
                      attrs: { type: "button" },
                      on: {
                        click: function($event) {
                          return _vm.hideModal()
                        }
                      }
                    },
                    [_c("i", { staticClass: "fas fa-times" })]
                  )
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "CCardBody",
            [
              _c(
                "CRow",
                [
                  _c("CCol", { attrs: { col: "4" } }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.params.search_text,
                          expression: "params.search_text"
                        }
                      ],
                      staticClass: "form-control",
                      attrs: { type: "text", placeholder: "Cari product..." },
                      domProps: { value: _vm.params.search_text },
                      on: {
                        keypress: function($event) {
                          if (
                            !$event.type.indexOf("key") &&
                            _vm._k(
                              $event.keyCode,
                              "enter",
                              13,
                              $event.key,
                              "Enter"
                            )
                          ) {
                            return null
                          }
                          $event.preventDefault()
                          return _vm.retrieveProduct($event)
                        },
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.$set(
                            _vm.params,
                            "search_text",
                            $event.target.value
                          )
                        }
                      }
                    })
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CRow",
                { staticClass: "mt-1" },
                [
                  _c(
                    "CCol",
                    { attrs: { col: "12" } },
                    [
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "400px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.productList,
                          gridOptions: _vm.gridOptions
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm.viewOnly == false
            ? _c("CCardFooter", [
                _c("div", { staticClass: "float-right" }, [
                  _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      attrs: { disabled: !_vm.selectedRowsCount },
                      on: { click: _vm.selectProduct }
                    },
                    [
                      _vm._v(
                        "Select " + _vm._s(_vm.selectedRowsCount) + " Products"
                      )
                    ]
                  )
                ])
              ])
            : _vm._e()
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);