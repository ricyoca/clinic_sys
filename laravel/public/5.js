(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "../coreui/src/components/forms/PatientForm.vue":
/*!******************************************************!*\
  !*** ../coreui/src/components/forms/PatientForm.vue ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _PatientForm_vue_vue_type_template_id_d4a69b0e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./PatientForm.vue?vue&type=template&id=d4a69b0e& */ "../coreui/src/components/forms/PatientForm.vue?vue&type=template&id=d4a69b0e&");
/* harmony import */ var _PatientForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./PatientForm.vue?vue&type=script&lang=js& */ "../coreui/src/components/forms/PatientForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _PatientForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _PatientForm_vue_vue_type_template_id_d4a69b0e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _PatientForm_vue_vue_type_template_id_d4a69b0e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/components/forms/PatientForm.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/components/forms/PatientForm.vue?vue&type=script&lang=js&":
/*!*******************************************************************************!*\
  !*** ../coreui/src/components/forms/PatientForm.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_PatientForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./PatientForm.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/components/forms/PatientForm.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_PatientForm_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/components/forms/PatientForm.vue?vue&type=template&id=d4a69b0e&":
/*!*************************************************************************************!*\
  !*** ../coreui/src/components/forms/PatientForm.vue?vue&type=template&id=d4a69b0e& ***!
  \*************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_PatientForm_vue_vue_type_template_id_d4a69b0e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./PatientForm.vue?vue&type=template&id=d4a69b0e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/components/forms/PatientForm.vue?vue&type=template&id=d4a69b0e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_PatientForm_vue_vue_type_template_id_d4a69b0e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_PatientForm_vue_vue_type_template_id_d4a69b0e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/mixins/forms.js":
/*!*************************************!*\
  !*** ../coreui/src/mixins/forms.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    dataModel: Object,
    errorModel: {
      type: Object,
      "default": function _default() {
        return {};
      }
    },
    readOnly: {
      type: Boolean,
      "default": false
    },
    labelWidth: {
      type: Number,
      "default": 3
    }
  },
  watch: {
    // dataModel: {
    //   immediate: true,
    //   handler() {
    //     this.form = this.dataModel;
    //   }
    // },
    errorModel: {
      immediate: true,
      handler: function handler() {
        this.errors = this.errorModel;
      }
    }
  },
  data: function data() {
    return {
      form: {},
      errors: {}
    };
  },
  methods: {
    setFormData: function setFormData(data) {
      this.form = Object.assign({}, data);
    },
    getFormData: function getFormData() {
      return this.form;
    },
    clearFormData: function clearFormData() {
      this.form = {};
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/components/forms/PatientForm.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/components/forms/PatientForm.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "../coreui/node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _mixins_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/mixins/forms */ "../coreui/src/mixins/forms.js");
/* harmony import */ var _services_satusehatmaster__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/services/satusehatmaster */ "../coreui/src/services/satusehatmaster.js");
/* harmony import */ var _components_modals_LoadingModal__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/modals/LoadingModal */ "../coreui/src/components/modals/LoadingModal.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    LoadingModal: _components_modals_LoadingModal__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  mixins: [_mixins_forms__WEBPACK_IMPORTED_MODULE_1__["default"]],
  data: function data() {
    return {
      diseaseList: [],
      alertModalMessage: "",
      isSendDataButtonVisible: false
    };
  },
  props: {
    partial: {
      type: Boolean,
      "default": false
    }
  },
  methods: {
    openLoadingModal: function openLoadingModal() {
      this.$refs['LoadingModal'].openModal();
    },
    hideLoadingModal: function hideLoadingModal() {
      this.$refs['LoadingModal'].hideModal();
    },
    fetchDisease: function fetchDisease() {
      return axios.get("api/disease");
    },
    setFormData: function setFormData(data) {
      this.form = Object.assign({}, data);

      if (this.form && this.form.disease_history) {
        this.form.disease_history = this.form.disease_history.slice().map(function (val) {
          return val.id;
        });
      } else {
        this.form.disease_history = [];
      }

      if (this.form && this.form.family_disease_history) {
        this.form.family_disease_history = this.form.family_disease_history.slice().map(function (val) {
          return val.id;
        });
      } else {
        this.form.family_disease_history = [];
      }
    },
    getFormData: function getFormData() {
      return this.form;
    },
    checkPatientWithSatusehat: function () {
      var _checkPatientWithSatusehat = _asyncToGenerator(
      /*#__PURE__*/
      _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var patientExistResponse, patientExist;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                //check NIK in local database
                //check patient with satusehat, saves patient to solamedic if patient is found and not yet registered in solamedic database
                //open loading modal
                this.openLoadingModal();
                _context.next = 3;
                return axios.get("api/patient/".concat(this.form.nik, "/check-nik"));

              case 3:
                patientExistResponse = _context.sent;
                patientExist = patientExistResponse.data.patient_exist;

                if (!patientExist) {
                  this.hideLoadingModal();
                  this.isSendDataButtonVisible = true;
                  this.$modal.show('AlertModal');
                  this.alertModalMessage = 'NIK ini belum tercatat pada database! Kirim data ke Satusehat sekarang?';
                } else {
                  this.hideLoadingModal();
                  this.$modal.show('AlertModal');
                  this.alertModalMessage = 'NIK ini sudah tercatat pada database!';
                }

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, this);
      }));

      function checkPatientWithSatusehat() {
        return _checkPatientWithSatusehat.apply(this, arguments);
      }

      return checkPatientWithSatusehat;
    }(),
    sendNewPatientToSatusehat: function sendNewPatientToSatusehat() {
      var _this = this;

      this.$modal.hide('AlertModal');
      this.isSendDataButtonVisible = false;
      this.openLoadingModal();
      Object(_services_satusehatmaster__WEBPACK_IMPORTED_MODULE_2__["sendNewPatient"])(this.form).then(function (response) {
        _this.hideLoadingModal();

        _this.$modal.show('AlertModal');

        if (response.data.status == 'error') {
          _this.alertModalMessage = 'Gagal sinkronasi data pasien! Mohon cek kembali NIK pasien';
        } else {
          _this.alertModalMessage = 'Berhasil sinkronasi data pasien!';
        }
      });
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    this.fetchDisease().then(function (response) {
      _this2.diseaseList = Object.freeze(response.data);
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/components/forms/PatientForm.vue?vue&type=template&id=d4a69b0e&":
/*!*******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/components/forms/PatientForm.vue?vue&type=template&id=d4a69b0e& ***!
  \*******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    [
      _c("CRow", { staticClass: "form-group" }, [
        _c(
          "label",
          {
            class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
            attrs: { for: "mr_number" }
          },
          [_vm._v("No. MR")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { class: ["col-sm-" + (12 - _vm.labelWidth)] },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.mr_number,
                  expression: "form.mr_number"
                }
              ],
              class: [
                "form-control",
                _vm.errors.mr_number ? " is-invalid" : ""
              ],
              attrs: {
                type: "text",
                placeholder: "No MR (autogenerate)",
                readonly: ""
              },
              domProps: { value: _vm.form.mr_number },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "mr_number", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _vm._l(_vm.errors.mr_number, function(item) {
              return _c("div", { staticClass: "text-danger small" }, [
                _vm._v(_vm._s(item))
              ])
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("CRow", { staticClass: "form-group" }, [
        _c(
          "label",
          {
            class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
            attrs: { for: "name" }
          },
          [_vm._v("Nama *")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { class: ["col-sm-" + (12 - _vm.labelWidth)] },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.name,
                  expression: "form.name"
                }
              ],
              class: ["form-control", _vm.errors.name ? " is-invalid" : ""],
              attrs: {
                type: "text",
                placeholder: "Nama...",
                disabled: _vm.readOnly
              },
              domProps: { value: _vm.form.name },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "name", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _vm._l(_vm.errors.name, function(item) {
              return _c("div", { staticClass: "text-danger small" }, [
                _vm._v(_vm._s(item))
              ])
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("CRow", { staticClass: "form-group" }, [
        _c(
          "label",
          {
            class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
            attrs: { for: "nik" }
          },
          [_vm._v("NIK *")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { class: ["col-sm-" + (12 - _vm.labelWidth)] },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.nik,
                  expression: "form.nik"
                }
              ],
              class: ["form-control", _vm.errors.nik ? " is-invalid" : ""],
              attrs: {
                type: "text",
                placeholder: "NIK...",
                disabled: _vm.readOnly
              },
              domProps: { value: _vm.form.nik },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "nik", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _vm._l(_vm.errors.nik, function(item) {
              return _c("div", { staticClass: "text-danger small" }, [
                _vm._v(_vm._s(item))
              ])
            }),
            _vm._v(" "),
            _c(
              "button",
              {
                staticClass: "btn btn-primary",
                attrs: {
                  disabled:
                    !_vm.form.nik || _vm.form.satusehat_integration_datetime
                },
                on: {
                  click: function($event) {
                    return _vm.checkPatientWithSatusehat()
                  }
                }
              },
              [_vm._v("Cek NIK")]
            )
          ],
          2
        )
      ]),
      _vm._v(" "),
      _c("CRow", { staticClass: "form-group" }, [
        _c(
          "label",
          {
            class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
            attrs: { for: "name" }
          },
          [_vm._v("Tgl Lahir *")]
        ),
        _vm._v(" "),
        _c(
          "div",
          { class: ["col-sm-" + (12 - _vm.labelWidth)] },
          [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.form.birth_date,
                  expression: "form.birth_date"
                }
              ],
              class: [
                "form-control",
                _vm.errors.birth_date ? " is-invalid" : ""
              ],
              attrs: {
                type: "date",
                width: "100%",
                lang: "en",
                disabled: _vm.readOnly
              },
              domProps: { value: _vm.form.birth_date },
              on: {
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.$set(_vm.form, "birth_date", $event.target.value)
                }
              }
            }),
            _vm._v(" "),
            _vm._l(_vm.errors.birth_date, function(item) {
              return _c("div", { staticClass: "text-danger small" }, [
                _vm._v(_vm._s(item))
              ])
            })
          ],
          2
        )
      ]),
      _vm._v(" "),
      !_vm.partial
        ? [
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("Nama Pasangan *")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.spouse_name,
                        expression: "form.spouse_name"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.spouse_name ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "Nama pasangan...",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.spouse_name },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "spouse_name", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.spouse_name, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("Alamat *")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.address,
                        expression: "form.address"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.address ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "Alamat...",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.address },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "address", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.address, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("No Telp/HP *")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.phone,
                        expression: "form.phone"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.phone ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "08xxxxxxxxx",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.phone },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "phone", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.phone, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("Gol. Darah")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.blood_type,
                        expression: "form.blood_type"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.blood_type ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "A/B/AB/O +/-",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.blood_type },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "blood_type", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.blood_type, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("Gol. Darah Pasangan")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.spouse_blood_type,
                        expression: "form.spouse_blood_type"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.spouse_blood_type ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "A/B/AB/O +/-",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.spouse_blood_type },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.form,
                          "spouse_blood_type",
                          $event.target.value
                        )
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.spouse_blood_type, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("Riwayat KB")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.kb_history,
                        expression: "form.kb_history"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.kb_history ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "Riwayat KB...",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.kb_history },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(_vm.form, "kb_history", $event.target.value)
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.kb_history, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", { staticClass: "form-group" }, [
              _c(
                "label",
                {
                  class: ["col-sm-" + _vm.labelWidth, "col-form-label"],
                  attrs: { for: "name" }
                },
                [_vm._v("Alergi Obat")]
              ),
              _vm._v(" "),
              _c(
                "div",
                { class: ["col-sm-" + (12 - _vm.labelWidth)] },
                [
                  _c("input", {
                    directives: [
                      {
                        name: "model",
                        rawName: "v-model",
                        value: _vm.form.medicine_allergy,
                        expression: "form.medicine_allergy"
                      }
                    ],
                    class: [
                      "form-control",
                      _vm.errors.medicine_allergy ? " is-invalid" : ""
                    ],
                    attrs: {
                      type: "text",
                      placeholder: "Alergi Obat...",
                      disabled: _vm.readOnly
                    },
                    domProps: { value: _vm.form.medicine_allergy },
                    on: {
                      input: function($event) {
                        if ($event.target.composing) {
                          return
                        }
                        _vm.$set(
                          _vm.form,
                          "medicine_allergy",
                          $event.target.value
                        )
                      }
                    }
                  }),
                  _vm._v(" "),
                  _vm._l(_vm.errors.medicine_allergy, function(item) {
                    return _c("div", { staticClass: "text-danger small" }, [
                      _vm._v(_vm._s(item))
                    ])
                  })
                ],
                2
              )
            ]),
            _vm._v(" "),
            _c("CRow", [
              _c("table", { staticClass: "table table-sm table-bordered" }, [
                _c("thead", [
                  _c("tr", [
                    _c("th", [_vm._v("Penyakit Dahulu")]),
                    _vm._v(" "),
                    _c("th", [_vm._v("Penyakit Keluarga")])
                  ])
                ]),
                _vm._v(" "),
                _c("tbody", [
                  _c("tr", [
                    _c(
                      "td",
                      [
                        _c("b-form-checkbox-group", {
                          attrs: {
                            stacked: "",
                            name: "disease_history",
                            "text-field": "name",
                            "value-field": "id",
                            options: _vm.diseaseList,
                            disabled: _vm.readOnly
                          },
                          model: {
                            value: _vm.form.disease_history,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "disease_history", $$v)
                            },
                            expression: "form.disease_history"
                          }
                        })
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c(
                      "td",
                      [
                        _c("b-form-checkbox-group", {
                          attrs: {
                            stacked: "",
                            name: "family_disease_history",
                            "text-field": "name",
                            "value-field": "id",
                            options: _vm.diseaseList,
                            disabled: _vm.readOnly
                          },
                          model: {
                            value: _vm.form.family_disease_history,
                            callback: function($$v) {
                              _vm.$set(_vm.form, "family_disease_history", $$v)
                            },
                            expression: "form.family_disease_history"
                          }
                        })
                      ],
                      1
                    )
                  ])
                ])
              ])
            ])
          ]
        : _vm._e(),
      _vm._v(" "),
      _c("LoadingModal", { ref: "LoadingModal" }),
      _vm._v(" "),
      [
        _c(
          "modal",
          {
            attrs: {
              name: "AlertModal",
              color: "primary",
              height: "20%",
              width: "30%"
            }
          },
          [
            _c("div", { staticClass: "m-5" }, [
              _c("h3", [_vm._v("Informasi")]),
              _vm._v(" "),
              _c("p", [_vm._v(_vm._s(_vm.alertModalMessage))]),
              _vm._v(" "),
              _vm.isSendDataButtonVisible
                ? _c(
                    "button",
                    {
                      staticClass: "btn btn-primary",
                      on: { click: _vm.sendNewPatientToSatusehat }
                    },
                    [_vm._v("Kirim data\n          pasien ke Satusehat")]
                  )
                : _vm._e()
            ])
          ]
        )
      ]
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);