(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[149],{

/***/ "../coreui/src/views/ag-component/RemoveButton.vue":
/*!*********************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=template&id=030e58b5& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony import */ var _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=script&lang=js& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/ag-component/RemoveButton.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!****************************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=template&id=030e58b5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/po/ShowPo.vue":
/*!*****************************************!*\
  !*** ../coreui/src/views/po/ShowPo.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowPo_vue_vue_type_template_id_62f71bf2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowPo.vue?vue&type=template&id=62f71bf2& */ "../coreui/src/views/po/ShowPo.vue?vue&type=template&id=62f71bf2&");
/* harmony import */ var _ShowPo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowPo.vue?vue&type=script&lang=js& */ "../coreui/src/views/po/ShowPo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowPo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowPo_vue_vue_type_template_id_62f71bf2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowPo_vue_vue_type_template_id_62f71bf2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/po/ShowPo.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/po/ShowPo.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ../coreui/src/views/po/ShowPo.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowPo.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/po/ShowPo.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPo_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/po/ShowPo.vue?vue&type=template&id=62f71bf2&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/po/ShowPo.vue?vue&type=template&id=62f71bf2& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPo_vue_vue_type_template_id_62f71bf2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowPo.vue?vue&type=template&id=62f71bf2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/po/ShowPo.vue?vue&type=template&id=62f71bf2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPo_vue_vue_type_template_id_62f71bf2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowPo_vue_vue_type_template_id_62f71bf2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "../coreui/node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
  name: 'RemoveButton',
  data: function data() {
    return {// dataArray: this.params.dataArray(),
      // deleteDataArray: this.params.deleteDataArray(),
    };
  },
  mounted: function mounted() {
    console.log(this.params);
  },
  methods: {
    removeItem: function removeItem() {
      this.params.removeItemCallback(this.params.data, this.params.node.id, this.params.dataArray(), this.params.deleteDataArray()); // this.$eventHub.$emit('remove-item',this.params.data, this.params.node.id)
    }
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/po/ShowPo.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/po/ShowPo.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    return {
      form: {
        // add more form attribute here
        detail: []
      },
      errors: {},
      disableButton: false,
      supplierList: [],
      locationList: [],
      scanCode: "",
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 120
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50
      }, {
        headerName: 'Harga',
        field: 'unit_purchase_price',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'PPN',
        field: 'tax_value',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Subtotal',
        field: 'subtotal',
        width: 120,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/po'})
    }
  },
  mounted: function mounted() {
    var _this = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/po/' + this.$route.params.id).then(function (response) {
      _this.form = response.data.po;
      _this.supplierList = Object.freeze(response.data.supplier);
      _this.locationList = Object.freeze(response.data.location);
    })["catch"](function (error) {
      _this.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "button",
    {
      staticClass: "btn btn-sm btn-danger",
      attrs: { type: "button" },
      on: { click: _vm.removeItem }
    },
    [_c("i", { staticClass: "fas fa-times" })]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/po/ShowPo.vue?vue&type=template&id=62f71bf2&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/po/ShowPo.vue?vue&type=template&id=62f71bf2& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [_vm._v("Show PO")])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Nomor PO")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.code,
                                        expression: "form.code"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      readonly: "",
                                      placeholder: "(autogenerate)",
                                      readonly: ""
                                    },
                                    domProps: { value: _vm.form.code },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.code, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.order_date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.order_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "order_date", $$v)
                                      },
                                      expression: "form.order_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.order_date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Supplier *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.supplier_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.supplierList,
                                      reduce: function(supplier) {
                                        return supplier.id
                                      },
                                      label: "name",
                                      name: "supplier",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.supplier_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "supplier_id", $$v)
                                      },
                                      expression: "form.supplier_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.supplier_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.locationList,
                                      reduce: function(location) {
                                        return location.id
                                      },
                                      label: "name",
                                      name: "location",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.location_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "location_id", $$v)
                                      },
                                      expression: "form.location_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("PPN")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("div", { staticClass: "form-check" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.is_taxable,
                                          expression: "form.is_taxable"
                                        }
                                      ],
                                      staticClass: "form-check-input",
                                      attrs: {
                                        type: "checkbox",
                                        id: "is_taxable",
                                        disabled: ""
                                      },
                                      domProps: {
                                        checked: Array.isArray(
                                          _vm.form.is_taxable
                                        )
                                          ? _vm._i(_vm.form.is_taxable, null) >
                                            -1
                                          : _vm.form.is_taxable
                                      },
                                      on: {
                                        change: [
                                          function($event) {
                                            var $$a = _vm.form.is_taxable,
                                              $$el = $event.target,
                                              $$c = $$el.checked ? true : false
                                            if (Array.isArray($$a)) {
                                              var $$v = null,
                                                $$i = _vm._i($$a, $$v)
                                              if ($$el.checked) {
                                                $$i < 0 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "is_taxable",
                                                    $$a.concat([$$v])
                                                  )
                                              } else {
                                                $$i > -1 &&
                                                  _vm.$set(
                                                    _vm.form,
                                                    "is_taxable",
                                                    $$a
                                                      .slice(0, $$i)
                                                      .concat(
                                                        $$a.slice($$i + 1)
                                                      )
                                                  )
                                              }
                                            } else {
                                              _vm.$set(
                                                _vm.form,
                                                "is_taxable",
                                                $$c
                                              )
                                            }
                                          },
                                          function($event) {
                                            return _vm.onChangeTaxable()
                                          }
                                        ]
                                      }
                                    }),
                                    _vm._v(" "),
                                    _c(
                                      "label",
                                      {
                                        staticClass: "form-check-label",
                                        attrs: { for: "is_taxable" }
                                      },
                                      [
                                        _vm._v(
                                          "\n                      Ya\n                    "
                                        )
                                      ]
                                    )
                                  ]),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.is_taxable, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c(
                              "div",
                              { staticClass: "form-group row" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Keterangan")]
                                ),
                                _vm._v(" "),
                                _c("span", { staticClass: "col-sm-8" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.remarks,
                                        expression: "form.remarks"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.remarks ? " is-invalid" : ""
                                    ],
                                    attrs: { cols: "30", readonly: "" },
                                    domProps: { value: _vm.form.remarks },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "remarks",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.errors.code, function(item) {
                                  return _c(
                                    "div",
                                    { staticClass: "text-danger small" },
                                    [_vm._v(_vm._s(item))]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        { staticClass: "mt-2" },
                        [
                          _c(
                            "CCol",
                            {
                              staticClass: "offset-sm-6 offset-lg-8",
                              attrs: { col: "6", lg: "4" }
                            },
                            [
                              _c("div", { staticClass: "form-group row" }, [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Grand Total")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "span",
                                  { staticClass: "col-sm-8" },
                                  [
                                    _c("cleave", {
                                      staticClass:
                                        "form-control-plaintext form-control-lg text-right",
                                      attrs: {
                                        options: {
                                          numeral: true,
                                          numeralDecimalScale: 0
                                        },
                                        readonly: ""
                                      },
                                      model: {
                                        value: _vm.form.grand_total,
                                        callback: function($$v) {
                                          _vm.$set(_vm.form, "grand_total", $$v)
                                        },
                                        expression: "form.grand_total"
                                      }
                                    }),
                                    _vm._v(" "),
                                    _vm._l(_vm.errors.grand_total, function(
                                      item
                                    ) {
                                      return _c(
                                        "div",
                                        { staticClass: "text-danger small" },
                                        [_vm._v(_vm._s(item))]
                                      )
                                    })
                                  ],
                                  2
                                )
                              ])
                            ]
                          )
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("hr"),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchProduct", { ref: "searchProduct" })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);