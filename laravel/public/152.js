(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[152],{

/***/ "../coreui/src/views/ag-component/RemoveButton.vue":
/*!*********************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue ***!
  \*********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=template&id=030e58b5& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony import */ var _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./RemoveButton.vue?vue&type=script&lang=js& */ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"],
  _RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/ag-component/RemoveButton.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!**********************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!****************************************************************************************!*\
  !*** ../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \****************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./RemoveButton.vue?vue&type=template&id=030e58b5& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_RemoveButton_vue_vue_type_template_id_030e58b5___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "../coreui/src/views/stock_opname/CreateStockOpname.vue":
/*!**************************************************************!*\
  !*** ../coreui/src/views/stock_opname/CreateStockOpname.vue ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _CreateStockOpname_vue_vue_type_template_id_b64fd8d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./CreateStockOpname.vue?vue&type=template&id=b64fd8d4& */ "../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=template&id=b64fd8d4&");
/* harmony import */ var _CreateStockOpname_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./CreateStockOpname.vue?vue&type=script&lang=js& */ "../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _CreateStockOpname_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _CreateStockOpname_vue_vue_type_template_id_b64fd8d4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _CreateStockOpname_vue_vue_type_template_id_b64fd8d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/stock_opname/CreateStockOpname.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=script&lang=js&":
/*!***************************************************************************************!*\
  !*** ../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStockOpname_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./CreateStockOpname.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStockOpname_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=template&id=b64fd8d4&":
/*!*********************************************************************************************!*\
  !*** ../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=template&id=b64fd8d4& ***!
  \*********************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStockOpname_vue_vue_type_template_id_b64fd8d4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./CreateStockOpname.vue?vue&type=template&id=b64fd8d4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=template&id=b64fd8d4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStockOpname_vue_vue_type_template_id_b64fd8d4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_CreateStockOpname_vue_vue_type_template_id_b64fd8d4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "../coreui/node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = (vue__WEBPACK_IMPORTED_MODULE_0___default.a.extend({
  name: 'RemoveButton',
  data: function data() {
    return {// dataArray: this.params.dataArray(),
      // deleteDataArray: this.params.deleteDataArray(),
    };
  },
  mounted: function mounted() {
    console.log(this.params);
  },
  methods: {
    removeItem: function removeItem() {
      this.params.removeItemCallback(this.params.data, this.params.node.id, this.params.dataArray(), this.params.deleteDataArray()); // this.$eventHub.$emit('remove-item',this.params.data, this.params.node.id)
    }
  }
}));

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
/* harmony import */ var _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/ag-component/RemoveButton */ "../coreui/src/views/ag-component/RemoveButton.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_2__["default"]],
  data: function data() {
    var _this = this;

    return {
      form: {
        // add more form attribute here
        detail: [],
        date: this.$moment().format('YYYY-MM-DD')
      },
      errors: {},
      disableButton: false,
      locationList: [],
      scanCode: "",
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: '⌧',
        colId: 'removeButton',
        width: 35,
        cellRendererFramework: _ag_component_RemoveButton__WEBPACK_IMPORTED_MODULE_3__["default"],
        cellRendererParams: {
          removeItemCallback: this.removeItem,
          dataArray: function dataArray() {
            return _this.form.detail;
          },
          deleteDataArray: function deleteDataArray() {
            return _this.form.delete_detail;
          }
        }
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 120
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty Sistem',
        field: 'system_qty',
        width: 90
      }, {
        headerName: 'Qty Hitung',
        field: 'count_qty',
        width: 90,
        editable: true
      }, {
        headerName: 'Selisih',
        field: 'adjust_qty',
        width: 90
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  watch: {
    detail: {
      handler: function handler(val) {
        this.calculateDetail();
        this.refreshCells();
      },
      deep: true
    }
  },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/stock-opname'})
    },
    store: function store() {
      var _this2 = this;

      this.disableButton = true;
      this.$notify({
        type: 'info',
        title: 'Saving...',
        duration: 3000
      });
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.post('/api/stock-opname', this.form).then(function (response) {
        _this2.$notify({
          type: 'success',
          title: 'Success!',
          text: 'This data has been saved successfully.'
        });

        _this2.$router.push({
          path: "/stock-opname/".concat(response.data.id, "/edit")
        });
      })["catch"](function (error) {
        _this2.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this2.errors = error.response.data.errors;
        }
      }).then(function () {
        _this2.disableButton = false;
      });
    },
    openSearchProductModal: function openSearchProductModal() {
      this.$refs['searchProduct'].openModal();
    },
    getProductByCode: function getProductByCode() {
      var _this3 = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("api/product/get-by-code/".concat(this.scanCode)).then(function (response) {
        if (!_.isEmpty(response.data)) {
          _this3.addDetail(response.data);
        } else {
          _this3.$notify({
            type: 'error',
            title: 'Oh no!',
            text: 'Kode tidak ditemukan'
          });
        }
      })["catch"](function (error) {
        _this3.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.data
        });
      }).then(function () {
        _this3.scanCode = '';
      });
    },
    addDetail: function addDetail(product) {
      var _this4 = this;

      var foundRow = this.form.detail.find(function (x) {
        return x.product_id == product.id;
      });

      if (foundRow) {
        return;
      }

      var currentStock = product.current_stock.find(function (x) {
        return x.location_id == _this4.form.location_id;
      });
      this.form.detail.push({
        seq_no: this.form.detail.length + 1,
        product_id: product.id,
        product_code: product.sku_code,
        product_name: product.name,
        system_qty: currentStock ? currentStock.qty : 0,
        count_qty: 0,
        adjust_qty: 0
      });
    },
    removeItem: function removeItem(item, index, arr) {
      var arrDelete = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;

      if (item.id && arrDelete) {
        arrDelete.push(item.id);
      }

      arr.splice(index, 1);
      this.updateSeqNumber();
    },
    calculateDetail: function calculateDetail() {
      _.forEach(this.form.detail, function (item, index) {
        item.adjust_qty = item.count_qty - item.system_qty;
      });
    },
    onChangeLocation: function onChangeLocation() {
      this.form.detail = [];
    }
  },
  mounted: function mounted() {
    var _this5 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/stock-opname/create').then(function (response) {
      _this5.locationList = Object.freeze(response.data.location);
    })["catch"](function (error) {
      console.log(error);

      _this5.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
    this.$refs['searchProduct'].$on("product-selected", function (eventData) {
      _.forEach(eventData, function (item, index) {
        _this5.addDetail(item);
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5&":
/*!**********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/ag-component/RemoveButton.vue?vue&type=template&id=030e58b5& ***!
  \**********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "button",
    {
      staticClass: "btn btn-sm btn-danger",
      attrs: { type: "button" },
      on: { click: _vm.removeItem }
    },
    [_c("i", { staticClass: "fas fa-times" })]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=template&id=b64fd8d4&":
/*!***************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/stock_opname/CreateStockOpname.vue?vue&type=template&id=b64fd8d4& ***!
  \***************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Create Stock Opname")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "12", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Kode")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.code,
                                        expression: "form.code"
                                      }
                                    ],
                                    staticClass: "form-control",
                                    attrs: {
                                      type: "text",
                                      readonly: "",
                                      placeholder: "(autogenerate)"
                                    },
                                    domProps: { value: _vm.form.code },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "code",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.code, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY"
                                    },
                                    model: {
                                      value: _vm.form.date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "date", $$v)
                                      },
                                      expression: "form.date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi*")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("v-select", {
                                    class: [
                                      _vm.errors.location_id
                                        ? " is-invalid"
                                        : ""
                                    ],
                                    attrs: {
                                      options: _vm.locationList,
                                      reduce: function(location) {
                                        return location.id
                                      },
                                      label: "name",
                                      name: "location"
                                    },
                                    on: { input: _vm.onChangeLocation },
                                    model: {
                                      value: _vm.form.location_id,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "location_id", $$v)
                                      },
                                      expression: "form.location_id"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.location_id, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "12", lg: "6" } }, [
                            _c(
                              "div",
                              { staticClass: "form-group row" },
                              [
                                _c(
                                  "label",
                                  { staticClass: "col-form-label col-sm-4" },
                                  [_vm._v("Keterangan")]
                                ),
                                _vm._v(" "),
                                _c("span", { staticClass: "col-sm-8" }, [
                                  _c("textarea", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.form.remarks,
                                        expression: "form.remarks"
                                      }
                                    ],
                                    class: [
                                      "form-control",
                                      _vm.errors.remarks ? " is-invalid" : ""
                                    ],
                                    attrs: { cols: "30" },
                                    domProps: { value: _vm.form.remarks },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.$set(
                                          _vm.form,
                                          "remarks",
                                          $event.target.value
                                        )
                                      }
                                    }
                                  })
                                ]),
                                _vm._v(" "),
                                _vm._l(_vm.errors.code, function(item) {
                                  return _c(
                                    "div",
                                    { staticClass: "text-danger small" },
                                    [_vm._v(_vm._s(item))]
                                  )
                                })
                              ],
                              2
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        { staticClass: "mb-2" },
                        [
                          _c("CCol", { attrs: { col: "6", lg: "4" } }, [
                            _c("div", { staticClass: "input-group" }, [
                              _c("input", {
                                directives: [
                                  {
                                    name: "model",
                                    rawName: "v-model",
                                    value: _vm.scanCode,
                                    expression: "scanCode"
                                  }
                                ],
                                staticClass: "form-control",
                                attrs: {
                                  type: "text",
                                  placeholder:
                                    "Scan / masukkan kode SKU barang...",
                                  disabled: !this.form.location_id
                                },
                                domProps: { value: _vm.scanCode },
                                on: {
                                  keypress: function($event) {
                                    if (
                                      !$event.type.indexOf("key") &&
                                      _vm._k(
                                        $event.keyCode,
                                        "enter",
                                        13,
                                        $event.key,
                                        "Enter"
                                      )
                                    ) {
                                      return null
                                    }
                                    $event.preventDefault()
                                    return _vm.getProductByCode($event)
                                  },
                                  input: function($event) {
                                    if ($event.target.composing) {
                                      return
                                    }
                                    _vm.scanCode = $event.target.value
                                  }
                                }
                              }),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "input-group-append" },
                                [
                                  _c(
                                    "CButton",
                                    {
                                      attrs: {
                                        color: "primary",
                                        disabled:
                                          _vm.disableButton ||
                                          !this.form.location_id
                                      },
                                      on: {
                                        click: function($event) {
                                          return _vm.openSearchProductModal()
                                        }
                                      }
                                    },
                                    [
                                      _c("i", { staticClass: "fas fa-plus" }),
                                      _vm._v(" Product\n                  ")
                                    ]
                                  )
                                ],
                                1
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: {
                            click: function($event) {
                              return _vm.store()
                            }
                          }
                        },
                        [
                          _c("i", { staticClass: "fas fa-save" }),
                          _vm._v(" Save\n          ")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Back")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchProduct", {
        ref: "searchProduct",
        attrs: { allowSelectMultiple: true }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);