(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[45],{

/***/ "../coreui/src/views/patient/IndexPatient.vue":
/*!****************************************************!*\
  !*** ../coreui/src/views/patient/IndexPatient.vue ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _IndexPatient_vue_vue_type_template_id_459ec0c7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./IndexPatient.vue?vue&type=template&id=459ec0c7& */ "../coreui/src/views/patient/IndexPatient.vue?vue&type=template&id=459ec0c7&");
/* harmony import */ var _IndexPatient_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./IndexPatient.vue?vue&type=script&lang=js& */ "../coreui/src/views/patient/IndexPatient.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _IndexPatient_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _IndexPatient_vue_vue_type_template_id_459ec0c7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _IndexPatient_vue_vue_type_template_id_459ec0c7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/patient/IndexPatient.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/patient/IndexPatient.vue?vue&type=script&lang=js&":
/*!*****************************************************************************!*\
  !*** ../coreui/src/views/patient/IndexPatient.vue?vue&type=script&lang=js& ***!
  \*****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPatient_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./IndexPatient.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/patient/IndexPatient.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPatient_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/patient/IndexPatient.vue?vue&type=template&id=459ec0c7&":
/*!***********************************************************************************!*\
  !*** ../coreui/src/views/patient/IndexPatient.vue?vue&type=template&id=459ec0c7& ***!
  \***********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPatient_vue_vue_type_template_id_459ec0c7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./IndexPatient.vue?vue&type=template&id=459ec0c7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/patient/IndexPatient.vue?vue&type=template&id=459ec0c7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPatient_vue_vue_type_template_id_459ec0c7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_IndexPatient_vue_vue_type_template_id_459ec0c7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/patient/IndexPatient.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/patient/IndexPatient.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_forms_PatientForm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/components/forms/PatientForm */ "../coreui/src/components/forms/PatientForm.vue");
/* harmony import */ var _components_modals_SearchPatientModal__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/modals/SearchPatientModal */ "../coreui/src/components/modals/SearchPatientModal.vue");
/* harmony import */ var _components_modals_ObstetryFormModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/modals/ObstetryFormModal */ "../coreui/src/components/modals/ObstetryFormModal.vue");
/* harmony import */ var _components_DataTable__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/DataTable */ "../coreui/src/components/DataTable.vue");
/* harmony import */ var _services_satusehatmaster__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/services/satusehatmaster */ "../coreui/src/services/satusehatmaster.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    PatientForm: _components_forms_PatientForm__WEBPACK_IMPORTED_MODULE_0__["default"],
    SearchPatientModal: _components_modals_SearchPatientModal__WEBPACK_IMPORTED_MODULE_1__["default"],
    ObstetryFormModal: _components_modals_ObstetryFormModal__WEBPACK_IMPORTED_MODULE_2__["default"],
    DataTable: _components_DataTable__WEBPACK_IMPORTED_MODULE_3__["default"]
  },
  data: function data() {
    return {
      loading: false,
      form: null,
      errors: {},
      searchMrNumber: '',
      obstetryApiUrl: null,
      obstetryFields: [{
        name: 'year',
        title: 'Tahun'
      }, {
        name: 'paraabortus',
        title: 'Para Abortus'
      }, {
        name: 'sex',
        title: 'Jenis Kelamin'
      }, {
        name: 'childbirth_method',
        title: 'Cara Persalinan & Indikasi'
      }, {
        name: 'birth_weight',
        title: 'Berat Lahir'
      }, {
        name: 'complication',
        title: 'Kelainan/Komplikasi'
      }, {
        name: 'actions'
      }],
      activeTab: 0,
      examApiUrl: null,
      examFields: [{
        name: 'exam_date',
        title: 'Tgl Periksa'
      }, {
        name: 'diagnosis',
        title: 'Diagnosa'
      }, {
        name: 'actions'
      }]
    };
  },
  methods: {
    save: function save() {
      var _this = this;

      this.loading = true;
      var ax;
      this.form = this.$refs['PatientForm'].getFormData();

      if (this.form.id) {
        ax = axios.put("/api/patient/".concat(this.form.id), this.form);
      } else {
        ax = axios.post("/api/patient", this.form);
      }

      ax.then(function (response) {
        _this.form = response.data;
        Object(_services_satusehatmaster__WEBPACK_IMPORTED_MODULE_4__["sendNewPatient"])(_this.form).then(function (response) {
          if (response.data.status == 'error') {
            _this.$notify({
              type: 'error',
              title: 'Oh no!',
              text: response.data.message
            });
          } else {
            console.log(response.data);
          }
        });

        _this.$notify({
          type: 'success',
          title: 'Success!'
        });

        _this.obstetryApiUrl = "api/obstetry?patient_id=".concat(_this.form.id);
        _this.examApiUrl = "api/exam?patient_id=".concat(_this.form.id, "&complete_only=true");
        _this.errors = {};
      })["catch"](function (error) {
        _this.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        if (error.response.status == 422) {
          _this.errors = error.response.data.errors;
        }
      });
    },
    getByNumber: function getByNumber(number) {
      var _this2 = this;

      axios.get("/api/patient/".concat(number)).then(function (response) {
        _this2.form = response.data;
        _this2.obstetryApiUrl = "api/obstetry?patient_id=".concat(_this2.form.id);
        _this2.examApiUrl = "api/exam?patient_id=".concat(_this2.form.id, "&complete_only=true");

        _this2.refreshData();

        _this2.activeTab = 0;
      })["catch"](function (error) {
        console.log(error);

        _this2.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.message
        });

        _this2.form = null;
        _this2.errors = {};
      });
    },
    deleteData: function deleteData() {
      var _this3 = this;

      this.$dialog.confirm('Are you sure you want to delete this data?').then(function (dialog) {
        axios["delete"]("/api/patient/".concat(_this3.form.id)).then(function (response) {
          _this3.$notify({
            type: 'success',
            title: 'Success!',
            text: 'Patient deleted successfully.'
          });

          _this3.form = {};

          _this3.$nextTick(function () {
            _this3.$refs['PatientForm'].setForm(null);
          });
        })["catch"](function (error) {
          console.log(error);
        });
      })["catch"](function () {});
    },
    openSearchModal: function openSearchModal() {
      this.$refs['SearchPatientModal'].openModal();
    },
    clearForm: function clearForm() {
      this.form = {};
      this.errors = {};
      this.refreshData();
      this.activeTab = 0;
    },
    refreshData: function refreshData() {
      var _this4 = this;

      this.$nextTick(function () {
        _this4.$refs['PatientForm'].setFormData(_this4.form); // if (this.form && this.form.id) {
        //   this.$refs['ObstetryDataTable'].refreshTable();
        //   this.$refs['ExamDataTable'].refreshTable();
        // }

      });
    },
    createNewObstetry: function createNewObstetry() {
      this.$refs['ObstetryFormModal'].clearFormData();
      this.$refs['ObstetryFormModal'].openModal();
    },
    editObstetryData: function editObstetryData(data) {
      this.$refs['ObstetryFormModal'].setFormData(Object.assign({}, data));
      this.$refs['ObstetryFormModal'].openModal();
    },
    deleteObstetryData: function deleteObstetryData(data) {
      var _this5 = this;

      this.$dialog.confirm('Are you sure you want to delete this data?').then(function (dialog) {
        axios["delete"]("/api/obstetry/".concat(data.id)).then(function (response) {
          _this5.$notify({
            type: 'success',
            title: 'Success!',
            text: 'Data deleted successfully.'
          });

          _this5.$refs['ObstetryDataTable'].refreshTable();
        })["catch"](function (error) {
          console.log(error);
        });
      })["catch"](function () {});
    },
    goToExam: function goToExam(examId) {
      this.$router.push({
        path: "/exam/".concat(examId, "/edit")
      });
    }
  },
  mounted: function mounted() {
    var _this6 = this;

    this.$refs['SearchPatientModal'].$on("patient-selected", function (data) {
      _this6.getByNumber(data.mr_number);
    });
    this.$refs['ObstetryFormModal'].$on('data-saved', function (data) {
      _this6.$refs['ObstetryDataTable'].refreshTable();
    });
  },
  beforeDestroy: function beforeDestroy() {
    this.$refs['SearchPatientModal'].$off("patient-selected");
    this.$refs['ObstetryFormModal'].$off('data-saved');
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/patient/IndexPatient.vue?vue&type=template&id=459ec0c7&":
/*!*****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/patient/IndexPatient.vue?vue&type=template&id=459ec0c7& ***!
  \*****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { sm: "12" } },
        [
          _c(
            "CCard",
            [
              _c("CCardHeader", [_c("h3", [_vm._v("Data Pasien")])]),
              _vm._v(" "),
              _c(
                "CCardHeader",
                [
                  _c(
                    "CRow",
                    [
                      _c("CCol", { attrs: { sm: "6" } }, [
                        _c("div", { staticClass: "input-group" }, [
                          _c("div", { staticClass: "input-group-prepend" }, [
                            _c("span", { staticClass: "input-group-text" }, [
                              _c("i", { staticClass: "fas fa-search" })
                            ])
                          ]),
                          _vm._v(" "),
                          _c("input", {
                            directives: [
                              {
                                name: "model",
                                rawName: "v-model",
                                value: _vm.searchMrNumber,
                                expression: "searchMrNumber"
                              }
                            ],
                            class: [
                              "form-control",
                              _vm.errors.mr_number ? " is-invalid" : ""
                            ],
                            attrs: {
                              type: "text",
                              placeholder: "Masukkan No MR lalu Enter"
                            },
                            domProps: { value: _vm.searchMrNumber },
                            on: {
                              keypress: function($event) {
                                if (
                                  !$event.type.indexOf("key") &&
                                  _vm._k(
                                    $event.keyCode,
                                    "enter",
                                    13,
                                    $event.key,
                                    "Enter"
                                  )
                                ) {
                                  return null
                                }
                                return _vm.getByNumber(_vm.searchMrNumber)
                              },
                              input: function($event) {
                                if ($event.target.composing) {
                                  return
                                }
                                _vm.searchMrNumber = $event.target.value
                              }
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "input-group-append" },
                            [
                              _c(
                                "CButton",
                                {
                                  attrs: { color: "primary" },
                                  on: { click: _vm.openSearchModal }
                                },
                                [
                                  _c("i", { staticClass: "fas fa-search" }),
                                  _vm._v(" Cari...")
                                ]
                              )
                            ],
                            1
                          )
                        ])
                      ]),
                      _vm._v(" "),
                      _c("CCol", { attrs: { sm: "6" } }, [
                        _c(
                          "span",
                          { staticClass: "float-right" },
                          [
                            _c(
                              "CButton",
                              {
                                attrs: {
                                  color: "primary",
                                  disable: _vm.loading
                                },
                                on: { click: _vm.clearForm }
                              },
                              [
                                _c("i", { staticClass: "fas fa-plus" }),
                                _vm._v(" Buat Baru\n              ")
                              ]
                            )
                          ],
                          1
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _vm.form
                ? _c(
                    "CCardBody",
                    [
                      _c(
                        "CTabs",
                        {
                          ref: "tabs",
                          attrs: { activeTab: _vm.activeTab },
                          on: {
                            "update:activeTab": function($event) {
                              _vm.activeTab = $event
                            },
                            "update:active-tab": function($event) {
                              _vm.activeTab = $event
                            }
                          }
                        },
                        [
                          _c(
                            "CTab",
                            {
                              staticClass: "pt-5",
                              attrs: { title: "Data Pasien" }
                            },
                            [
                              _c("PatientForm", {
                                ref: "PatientForm",
                                attrs: { "error-model": _vm.errors }
                              }),
                              _vm._v(" "),
                              _c("hr"),
                              _vm._v(" "),
                              _c(
                                "CButton",
                                {
                                  attrs: {
                                    color: "primary",
                                    disable: _vm.loading
                                  },
                                  on: { click: _vm.save }
                                },
                                [
                                  _c("i", { staticClass: "fas fa-save" }),
                                  _vm._v(" Simpan")
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "float-right" },
                                [
                                  _vm.form.id
                                    ? _c(
                                        "CButton",
                                        {
                                          attrs: {
                                            color: "danger",
                                            disable: _vm.loading
                                          },
                                          on: { click: _vm.deleteData }
                                        },
                                        [
                                          _c("i", {
                                            staticClass: "fas fa-trash"
                                          }),
                                          _vm._v(" Hapus")
                                        ]
                                      )
                                    : _vm._e()
                                ],
                                1
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _vm.form.id
                            ? _c(
                                "CTab",
                                {
                                  staticClass: "pt-5",
                                  attrs: { title: "Riwayat Obstetri" }
                                },
                                [
                                  _c(
                                    "CButton",
                                    {
                                      attrs: {
                                        color: "primary",
                                        disable: _vm.loading
                                      },
                                      on: { click: _vm.createNewObstetry }
                                    },
                                    [
                                      _c("i", { staticClass: "fas fa-plus" }),
                                      _vm._v(
                                        " Buat\n              Obstetri Baru"
                                      )
                                    ]
                                  ),
                                  _vm._v(" "),
                                  _c("DataTable", {
                                    ref: "ObstetryDataTable",
                                    attrs: {
                                      fields: _vm.obstetryFields,
                                      apiUrl: _vm.obstetryApiUrl
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "actions",
                                          fn: function(props) {
                                            return [
                                              _c(
                                                "CButton",
                                                {
                                                  attrs: {
                                                    size: "sm",
                                                    color: "primary"
                                                  },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.editObstetryData(
                                                        props.rowData
                                                      )
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("i", {
                                                    staticClass: "fas fa-edit"
                                                  })
                                                ]
                                              ),
                                              _vm._v(" "),
                                              _c(
                                                "CButton",
                                                {
                                                  attrs: {
                                                    size: "sm",
                                                    color: "danger"
                                                  },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.deleteObstetryData(
                                                        props.rowData
                                                      )
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("i", {
                                                    staticClass: "fas fa-trash"
                                                  })
                                                ]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      false,
                                      3397772992
                                    )
                                  })
                                ],
                                1
                              )
                            : _vm._e(),
                          _vm._v(" "),
                          _vm.form.id
                            ? _c(
                                "CTab",
                                {
                                  staticClass: "pt-5",
                                  attrs: { title: "Riwayat Kontrol" }
                                },
                                [
                                  _c("DataTable", {
                                    ref: "ExamDataTable",
                                    attrs: {
                                      fields: _vm.examFields,
                                      apiUrl: _vm.examApiUrl
                                    },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "actions",
                                          fn: function(props) {
                                            return [
                                              _c(
                                                "CButton",
                                                {
                                                  attrs: {
                                                    size: "sm",
                                                    color: "primary"
                                                  },
                                                  on: {
                                                    click: function($event) {
                                                      return _vm.goToExam(
                                                        props.rowData.id
                                                      )
                                                    }
                                                  }
                                                },
                                                [
                                                  _c("i", {
                                                    staticClass: "fas fa-edit"
                                                  })
                                                ]
                                              )
                                            ]
                                          }
                                        }
                                      ],
                                      null,
                                      false,
                                      2891858944
                                    )
                                  })
                                ],
                                1
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ],
                    1
                  )
                : _vm._e()
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("SearchPatientModal", { ref: "SearchPatientModal" }),
      _vm._v(" "),
      _c("ObstetryFormModal", {
        ref: "ObstetryFormModal",
        attrs: { patient: _vm.form }
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);