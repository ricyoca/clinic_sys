(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[125],{

/***/ "../coreui/src/views/sales/ShowSales.vue":
/*!***********************************************!*\
  !*** ../coreui/src/views/sales/ShowSales.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ShowSales_vue_vue_type_template_id_139fd012___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ShowSales.vue?vue&type=template&id=139fd012& */ "../coreui/src/views/sales/ShowSales.vue?vue&type=template&id=139fd012&");
/* harmony import */ var _ShowSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ShowSales.vue?vue&type=script&lang=js& */ "../coreui/src/views/sales/ShowSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../laravel/node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_laravel_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ShowSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ShowSales_vue_vue_type_template_id_139fd012___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ShowSales_vue_vue_type_template_id_139fd012___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "coreui/src/views/sales/ShowSales.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "../coreui/src/views/sales/ShowSales.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ../coreui/src/views/sales/ShowSales.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/babel-loader/lib??ref--4-0!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowSales.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/ShowSales.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_laravel_node_modules_babel_loader_lib_index_js_ref_4_0_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowSales_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "../coreui/src/views/sales/ShowSales.vue?vue&type=template&id=139fd012&":
/*!******************************************************************************!*\
  !*** ../coreui/src/views/sales/ShowSales.vue?vue&type=template&id=139fd012& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowSales_vue_vue_type_template_id_139fd012___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../laravel/node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../laravel/node_modules/vue-loader/lib??vue-loader-options!./ShowSales.vue?vue&type=template&id=139fd012& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/ShowSales.vue?vue&type=template&id=139fd012&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowSales_vue_vue_type_template_id_139fd012___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _laravel_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_laravel_node_modules_vue_loader_lib_index_js_vue_loader_options_ShowSales_vue_vue_type_template_id_139fd012___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/ShowSales.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/sales/ShowSales.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "../coreui/node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_SearchCustomer__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/components/SearchCustomer */ "../coreui/src/views/components/SearchCustomer.vue");
/* harmony import */ var _components_SearchProduct__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/components/SearchProduct */ "../coreui/src/views/components/SearchProduct.vue");
/* harmony import */ var _components_SalesCashPayment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/components/SalesCashPayment */ "../coreui/src/views/components/SalesCashPayment.vue");
/* harmony import */ var _components_SalesReceivablePayment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/components/SalesReceivablePayment */ "../coreui/src/views/components/SalesReceivablePayment.vue");
/* harmony import */ var _mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../mixins/ag-grid-options */ "../coreui/src/mixins/ag-grid-options.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    SearchCustomer: _components_SearchCustomer__WEBPACK_IMPORTED_MODULE_1__["default"],
    SearchProduct: _components_SearchProduct__WEBPACK_IMPORTED_MODULE_2__["default"],
    SalesCashPayment: _components_SalesCashPayment__WEBPACK_IMPORTED_MODULE_3__["default"],
    SalesReceivablePayment: _components_SalesReceivablePayment__WEBPACK_IMPORTED_MODULE_4__["default"]
  },
  mixins: [_mixins_ag_grid_options__WEBPACK_IMPORTED_MODULE_5__["default"]],
  data: function data() {
    return {
      form: {
        // add more form attribute here
        customer: {},
        mechanic: {},
        location: {},
        detail: [],
        sales_date: this.$moment().format('YYYY-MM-DD'),
        discount_total: 0,
        net_total: 0,
        grand_total: 0
      },
      errors: {},
      disableButton: false,
      scanCode: null,
      columnDefs: [{
        headerName: 'No',
        field: 'seq_no',
        width: 35
      }, {
        headerName: 'Kode Barang',
        field: 'product_code',
        width: 100
      }, {
        headerName: 'Nama Barang',
        field: 'product_name',
        width: 150
      }, {
        headerName: 'Qty',
        field: 'qty',
        width: 50,
        valueSetter: function valueSetter(params) {
          params.data['qty'] = Number(params.newValue);
        }
      }, {
        headerName: 'Harga',
        field: 'unit_sales_price',
        width: 90,
        cellClass: ['text-right'],
        valueSetter: function valueSetter(params) {
          params.data['unit_sales_price'] = Number(params.newValue);
        },
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Diskon [%]',
        field: 'discount_percent',
        width: 75,
        cellClass: ['text-right'],
        valueSetter: function valueSetter(params) {
          params.data['discount_percent'] = Number(params.newValue);
        },
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'PPN',
        field: 'tax_value',
        width: 90,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Net',
        field: 'net_sales_price',
        width: 90,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }, {
        headerName: 'Subtotal',
        field: 'subtotal',
        width: 90,
        cellClass: ['text-right'],
        valueFormatter: this.numberFormatter
      }]
    };
  },
  computed: {
    detail: function detail() {
      return this.form.detail;
    }
  },
  // watch: {
  //   detail: {
  //     handler(val) {
  //       this.calculateDetail();
  //     },
  //     deep: true
  //   }
  // },
  methods: {
    goBack: function goBack() {
      this.$router.go(-1); // this.$router.replace({path: '/sales'})
    },
    openSearchProductModal: function openSearchProductModal() {
      this.$refs['searchProduct'].openModal();
    },
    getProductByCode: function getProductByCode() {
      var _this = this;

      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("api/product/get-by-code/".concat(this.scanCode)).then(function (response) {
        if (!_.isEmpty(response.data)) {
          _this.addDetail(response.data);
        } else {
          _this.$notify({
            type: 'error',
            title: 'Oh no!',
            text: 'Kode tidak ditemukan'
          });
        }
      })["catch"](function (error) {
        _this.$notify({
          type: 'error',
          title: 'Oh no!',
          text: error.response.data.data
        });
      }).then(function () {
        _this.scanCode = '';
      });
    },
    // calculateItemDiscount(item) {
    //   return item.discount_percent / 100 * item.unit_sales_price
    // },
    // calculateItemTaxValue(item) {
    //   return item.net_sales_price * item.tax_percent / 100;
    // },
    // calculateItemNetPrice(item) {
    //   return Number(item.net_sales_price) + Number(item.tax_value);
    // },
    // calculateItemSubtotal(item) {
    //   return item.qty * item.net_sales_price;
    // },
    // calculateDetail() {
    //   this.form.gross_total = 0;
    //   this.form.net_total = 0;
    //   this.form.tax_total = 0;
    //   this.form.grand_total = 0;
    //   this.form.discount_total = 0;
    //   _.forEach(this.form.detail, (item, index) => {
    //     item.discount_value = this.calculateItemDiscount(item);
    //     item.net_sales_price = item.unit_sales_price - item.discount_value;
    //     item.tax_value = this.calculateItemTaxValue(item);
    //     item.net_sales_price = this.calculateItemNetPrice(item);
    //     item.subtotal = this.calculateItemSubtotal(item);
    //     this.form.gross_total += (item.unit_sales_price * item.qty);
    //     this.form.net_total += item.subtotal;
    //     this.form.tax_total += (item.tax_value * item.qty);
    //     this.form.discount_total += (item.discount_value * item.qty);
    //   })
    //   this.form.grand_total = this.form.net_total;
    // },
    openSearchCustomerModal: function openSearchCustomerModal() {
      this.$refs['searchCustomer'].openModal();
    },
    selectCustomer: function selectCustomer(customer) {
      this.form.customer = customer;
      this.form.customer_id = customer.id;
    },
    onChangeDiscount: function onChangeDiscount() {
      this.form.grand_total = this.form.net_total - this.form.discount_total;
    },
    print: function print() {
      this.$eventHub.$emit('send-to-print', {
        url: "api/sales/".concat(this.$route.params.id, "/print-invoice"),
        type: 'GET'
      });
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    axios__WEBPACK_IMPORTED_MODULE_0___default.a.get("/api/sales/".concat(this.$route.params.id)).then(function (response) {
      _this2.form = response.data.sales;
    })["catch"](function (error) {
      console.log(error);

      _this2.$notify({
        type: 'error',
        title: 'Oh no!',
        text: error.response.data.message
      });
    });
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!../coreui/src/views/sales/ShowSales.vue?vue&type=template&id=139fd012&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!../coreui/src/views/sales/ShowSales.vue?vue&type=template&id=139fd012& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "CRow",
    [
      _c(
        "CCol",
        { attrs: { col: "12", lg: "12" } },
        [
          _c(
            "CCard",
            [
              _c(
                "CCardHeader",
                [
                  _c("CCardTitle", { staticClass: "mb-0" }, [
                    _vm._v("Show Sales")
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "CCardBody",
                [
                  _c(
                    "CForm",
                    [
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Customer")]
                              ),
                              _vm._v(" "),
                              _c("span", { staticClass: "col-sm-8" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.form.customer.name,
                                      expression: "form.customer.name"
                                    }
                                  ],
                                  class: [
                                    "form-control",
                                    _vm.errors.customer_id ? " is-invalid" : ""
                                  ],
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: _vm.form.customer.name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.form.customer,
                                        "name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _vm.form.mechanic
                              ? _c("div", { staticClass: "form-group row" }, [
                                  _c(
                                    "label",
                                    { staticClass: "col-form-label col-sm-4" },
                                    [_vm._v("Mekanik *")]
                                  ),
                                  _vm._v(" "),
                                  _c("span", { staticClass: "col-sm-8" }, [
                                    _c("input", {
                                      directives: [
                                        {
                                          name: "model",
                                          rawName: "v-model",
                                          value: _vm.form.mechanic.name,
                                          expression: "form.mechanic.name"
                                        }
                                      ],
                                      class: [
                                        "form-control",
                                        _vm.errors.mechanic_id
                                          ? " is-invalid"
                                          : ""
                                      ],
                                      attrs: { type: "text", readonly: "" },
                                      domProps: {
                                        value: _vm.form.mechanic.name
                                      },
                                      on: {
                                        input: function($event) {
                                          if ($event.target.composing) {
                                            return
                                          }
                                          _vm.$set(
                                            _vm.form.mechanic,
                                            "name",
                                            $event.target.value
                                          )
                                        }
                                      }
                                    })
                                  ])
                                ])
                              : _vm._e()
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "6", lg: "6" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Lokasi *")]
                              ),
                              _vm._v(" "),
                              _c("span", { staticClass: "col-sm-8" }, [
                                _c("input", {
                                  directives: [
                                    {
                                      name: "model",
                                      rawName: "v-model",
                                      value: _vm.form.location.name,
                                      expression: "form.location.name"
                                    }
                                  ],
                                  class: [
                                    "form-control",
                                    _vm.errors.location_id ? " is-invalid" : ""
                                  ],
                                  attrs: { type: "text", readonly: "" },
                                  domProps: { value: _vm.form.location.name },
                                  on: {
                                    input: function($event) {
                                      if ($event.target.composing) {
                                        return
                                      }
                                      _vm.$set(
                                        _vm.form.location,
                                        "name",
                                        $event.target.value
                                      )
                                    }
                                  }
                                })
                              ])
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                { staticClass: "col-form-label col-sm-4" },
                                [_vm._v("Tanggal *")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("date-picker", {
                                    class: [
                                      _vm.errors.sales_date ? " is-invalid" : ""
                                    ],
                                    attrs: {
                                      width: "100%",
                                      lang: "en",
                                      "value-type": "YYYY-MM-DD",
                                      format: "DD-MM-YYYY",
                                      disabled: ""
                                    },
                                    model: {
                                      value: _vm.form.sales_date,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "sales_date", $$v)
                                      },
                                      expression: "form.sales_date"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.sales_date, function(item) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _vm.errors.detail
                        ? _c(
                            "div",
                            {
                              staticClass: "alert alert-danger p-1",
                              attrs: { role: "alert" }
                            },
                            [
                              _c(
                                "ul",
                                { staticClass: "mb-0" },
                                _vm._l(_vm.errors.detail, function(item) {
                                  return _c("li", [_vm._v(_vm._s(item))])
                                }),
                                0
                              )
                            ]
                          )
                        : _vm._e(),
                      _vm._v(" "),
                      _c("ag-grid-vue", {
                        staticClass: "ag-theme-alpine",
                        staticStyle: { height: "350px" },
                        attrs: {
                          columnDefs: _vm.columnDefs,
                          rowData: _vm.form.detail,
                          gridOptions: _vm.gridOptions
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "CRow",
                        [
                          _c("CCol", { attrs: { col: "12", lg: "4" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-sm-4 col-form-label text-right"
                                },
                                [_vm._v("Total Gros")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.gross_total,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "gross_total", $$v)
                                      },
                                      expression: "form.gross_total"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-sm-4 col-form-label text-right"
                                },
                                [_vm._v("Total Diskon")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.discount_total,
                                      callback: function($$v) {
                                        _vm.$set(
                                          _vm.form,
                                          "discount_total",
                                          $$v
                                        )
                                      },
                                      expression: "form.discount_total"
                                    }
                                  })
                                ],
                                1
                              )
                            ]),
                            _vm._v(" "),
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-sm-4 col-form-label text-right"
                                },
                                [_vm._v("Total PPN")]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.tax_total,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "tax_total", $$v)
                                      },
                                      expression: "form.tax_total"
                                    }
                                  })
                                ],
                                1
                              )
                            ])
                          ]),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "12", lg: "4" } }),
                          _vm._v(" "),
                          _c("CCol", { attrs: { col: "12", lg: "4" } }, [
                            _c("div", { staticClass: "form-group row" }, [
                              _c(
                                "label",
                                {
                                  staticClass:
                                    "col-form-label col-sm-4 text-right"
                                },
                                [_vm._v("Grand Total")]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticClass: "col-sm-8" },
                                [
                                  _c("cleave", {
                                    staticClass:
                                      "form-control-plaintext form-control-lg text-right",
                                    attrs: {
                                      options: {
                                        numeral: true,
                                        numeralDecimalScale: 0
                                      },
                                      readonly: ""
                                    },
                                    model: {
                                      value: _vm.form.grand_total,
                                      callback: function($$v) {
                                        _vm.$set(_vm.form, "grand_total", $$v)
                                      },
                                      expression: "form.grand_total"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _vm._l(_vm.errors.grand_total, function(
                                    item
                                  ) {
                                    return _c(
                                      "div",
                                      { staticClass: "text-danger small" },
                                      [_vm._v(_vm._s(item))]
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: { color: "primary" },
                          on: { click: _vm.print }
                        },
                        [
                          _c("i", { staticClass: "fas fa-print" }),
                          _vm._v(" Cetak")
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "CButton",
                        {
                          attrs: {
                            color: "primary",
                            disabled: _vm.disableButton
                          },
                          on: { click: _vm.goBack }
                        },
                        [_vm._v("Batal")]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ })

}]);