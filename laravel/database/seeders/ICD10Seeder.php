<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use JeroenZwart\CsvSeeder\CsvSeeder;

class ICD10Seeder extends CsvSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function __construct(){

        $this->file = base_path().'/database/seeders/csv/icd10.csv';
        $this->tablename = 'icd';
        $this->delimiter = ';';
    }
    
     public function run(): void
    {
        DB::disableQueryLog();

		parent::run();
    }
}
