<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('config')->insert([
            "key" => "practitioner_ihs_number",
            "value" => "10009880728",
            "created_at" => now(),
        ]);

        DB::table('config')->insert([
            "key" => "encounter_location_uuid",
            "value" => "",
            "created_at" => now(),
        ]);

        DB::table('config')->insert([
            "key" => "condition_clinicalstatus_id",
            "value" => "",
            "created_at" => now(),
        ]);

        DB::table('config')->insert([
            "key" => "condition_category_id",
            "value" => "",
            "created_at" => now(),
        ]);

        DB::table('config')->insert([
            "key" => "encounter_status_id",
            "value" => "",
            "created_at" => now(),
        ]);

        DB::table('config')->insert([
            "key" => "encounter_class_id",
            "value" => "",
            "created_at" => now(),
        ]);
    }
}
