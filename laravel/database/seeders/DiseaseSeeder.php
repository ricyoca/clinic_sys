<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Disease;

class DiseaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $diseaseList = [
            'Hipertensi',
            'Diabetes Mellitus',
            'Tuberkolosis',
            'Kelainan Jantung',
            'Kelainan Darah',
            'Alergi',
            'Tiroid',
            'Asthma'
        ];
        foreach ($diseaseList as $diseaseName) {
            $disease = Disease::create([ 
                'name' => $diseaseName,
            ]);
            # code...
        }
    }
}
