<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam', function (Blueprint $table) {
            $table->id();
            $table->string('number')->nullable();
            $table->unsignedInteger('patient_id')->nullable();
            $table->string('appointment_id')->nullable();
            $table->date('exam_date')->nullable();
            $table->string('status')->nullable();
            $table->string('blood_pressure')->nullable();
            $table->string('heart_rate')->nullable();
            $table->string('body_temperature')->nullable();
            $table->string('respiratory_rate')->nullable();
            $table->boolean('fall_history')->nullable();
            $table->string('gestational_age')->nullable();
            $table->string('height')->nullable();
            $table->string('weight')->nullable();

            $table->string('vulva_vagina')->nullable();
            $table->string('porsio')->nullable();
            $table->string('corpusuteri')->nullable();
            $table->string('adnexad')->nullable();
            $table->string('adnexas')->nullable();
            $table->string('cavumdouglas')->nullable();
            
            $table->string('subjective_symptoms')->nullable();
            $table->string('head')->nullable();
            $table->string('eye')->nullable();
            $table->string('otology')->nullable();
            $table->string('neck')->nullable();
            $table->string('oral')->nullable();
            $table->string('cardiology')->nullable();
            $table->string('thorax_chest')->nullable();
            $table->string('abdomen')->nullable();
            $table->string('dermatology')->nullable();
            $table->string('backbone_anatomy')->nullable();
            $table->string('neurology')->nullable();
            $table->string('genital')->nullable();
            $table->string('extremity')->nullable();

            $table->string('vagina_toucher')->nullable();
            $table->string('hodge_height')->nullable();
            $table->string('effice')->nullable();
            $table->string('bloodslem')->nullable();
            $table->string('fetal_membrane')->nullable();
            $table->string('obstetric_notes')->nullable();

            // $table->string('icd_code')->nullable();
            $table->string('diagnosis')->nullable();
            $table->string('planning')->nullable();
            $table->string('monitoring')->nullable();
            $table->string('next_appointment')->nullable();

            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam');
    }
}
