<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usg', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('exam_id')->nullable();
            $table->unsignedInteger('patient_id')->nullable();
            $table->text('biometry')->nullable();
            $table->text('fetal_weight_est')->nullable();
            $table->text('fetal_heart_beat')->nullable();
            $table->text('amnion')->nullable();
            $table->text('placenta')->nullable();
            $table->text('fetal_movement')->nullable();
            $table->text('fetal_survey')->nullable();
            $table->text('screening')->nullable();
            $table->text('adnexa')->nullable();
            $table->text('others')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usg');
    }
}
