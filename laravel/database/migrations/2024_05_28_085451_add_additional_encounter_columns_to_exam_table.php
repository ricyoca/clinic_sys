<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalEncounterColumnsToExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->unsignedBigInteger('encounter_status_id')->nullable();
            $table->unsignedBigInteger('encounter_class_id')->nullable();
            $table->unsignedBigInteger('encounter_internal_id')->nullable();
            $table->string('location_uuid')->nullable();
            $table->datetime('period_start')->nullable();
            $table->datetime('period_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->dropColumn('location_uuid');
            $table->dropColumn('period_start');
            $table->dropColumn('period_end');
        });
    }
}
