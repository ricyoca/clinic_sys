<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAdditionalConditionColumnsToExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->unsignedBigInteger('conditon_internal_id')->nullable();
            $table->unsignedBigInteger('condition_clinicalstatus_id')->nullable();
            $table->unsignedBigInteger('condition_category_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->dropColumn('condition_clinicalstatus_id');
            $table->dropColumn('condition_category_id');
        });
    }
}
