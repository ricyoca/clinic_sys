<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateObstetryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('obstetry', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('patient_id');
            $table->unsignedInteger('year')->nullable();
            $table->char('sex',1)->nullable();
            $table->unsignedInteger('birth_weight')->nullable();
            $table->unsignedInteger('birth_length')->nullable();
            $table->string('paraabortus')->nullable();
            $table->string('childbirth_method')->nullable();
            $table->string('complication')->nullable();
            $table->string('birth_place')->nullable();
            $table->string('first_day_of_last_period')->nullable();
            $table->date('pregnancy_estimate')->nullable();
            $table->unsignedSmallInteger('g')->nullable();
            $table->unsignedSmallInteger('p')->nullable();
            $table->unsignedSmallInteger('a')->nullable();
            $table->string('pregnancy_type')->nullable();
            $table->string('chroniocity')->nullable();
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->unsignedInteger('deleted_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('obstetry');
    }
}
