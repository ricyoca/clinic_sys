<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeHPHTTypeInObstetryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('obstetry', function (Blueprint $table) {
            $table->date('first_day_of_last_period')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('obstetry', function (Blueprint $table) {
            $table->string('first_day_of_last_period')->nullable()->change();
            
        });
    }
}
