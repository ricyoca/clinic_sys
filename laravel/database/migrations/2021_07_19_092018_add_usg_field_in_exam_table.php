<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsgFieldInExamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->after('next_appointment', function ($table) {
                $table->string('usg_fetal_weight_estimate')->nullable();
                $table->string('usg_biometry')->nullable();
                $table->string('usg_fetal_heart_beat')->nullable();
                $table->string('usg_amnion')->nullable();
                $table->string('usg_placenta')->nullable();
                $table->string('usg_fetal_movement')->nullable();
                $table->string('usg_fetal_survey')->nullable();
                $table->string('usg_screening')->nullable();
                $table->string('usg_others')->nullable();
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam', function (Blueprint $table) {
            $table->dropColumn('usg_fetal_weight_estimate');
            $table->dropColumn('usg_biometry');
            $table->dropColumn('usg_fetal_heart_beat');
            $table->dropColumn('usg_amnion');
            $table->dropColumn('usg_placenta');
            $table->dropColumn('usg_fetal_movement');
            $table->dropColumn('usg_fetal_survey');
            $table->dropColumn('usg_screening');
            $table->dropColumn('usg_others');
        });
    }
}
