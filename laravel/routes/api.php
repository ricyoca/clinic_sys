<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api'], function ($router) {
    Route::get('menu', 'MenuController@index');

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('register', 'AuthController@register');


    Route::resource('patient',     'PatientController');
    Route::post('patient/{id}/satusehat-integrated',     'PatientController@updateSatusehatIntegrationDateTime');
    Route::get('patient/{nik}/check-nik',     'PatientController@checkNIK');
    Route::get('integration/integrate-all-patient',     'PatientController@integrateAllPatient');
    Route::get('integration/integrate-all-exam',     'PatientController@integrateAllExam');

    Route::resource('disease',     'DiseaseController');
    Route::resource('obstetry',     'ObstetryController');
    Route::resource('appointment',     'AppointmentController');
    Route::resource('role',     'RoleController');

    Route::post('exam/{id}/usg-file',     'ExamController@uploadUsgFile');
    Route::post('exam/{id}/support-file',     'ExamController@uploadSupportFile');
    Route::get('exam/{id}/download-report',     'ExamController@downloadReport');
    Route::get('exam/{id}/download-invoice',     'ExamController@downloadInvoice');
    Route::post('exam/{id}/invoice',     'ExamController@saveInvoice');
    Route::get('exam/{id}/invoice',     'ExamController@getInvoice');
    Route::post('exam/{id}/satusehat-integrated',     'ExamController@updateSatusehatIntegrationDateTime');
    Route::resource('exam',     'ExamController');


    Route::get('config/get-all-config', 'ConfigController@getAllConfig');
    Route::get('config/get-value', 'ConfigController@getValue');
    Route::get('config/set-value', 'ConfigController@setValue');
    Route::post('config/mass-set-value', 'ConfigController@massSetValue');

    Route::resource('icd',     'IcdController');

    Route::get('token/get-solamedic-token', 'TokenController@getSolamedicToken');
    Route::post('token/save-solamedic-token', 'TokenController@saveSolamedicToken');
    Route::post('user/{id}/change-password', 'UsersController@changePassword');
    Route::resource('user', 'UsersController');

    Route::group(['middleware' => 'admin'], function ($router) {});
});
