@php
use Carbon\Carbon;
$diseaseList = \App\Models\Disease::all();
$sex = [
'M' => "Laki-laki",
'F' => "Perempuan",
]
@endphp

<html>
<style>
  table {
    margin: 0;
    padding: 0;
    border: 0;
  }

  .logo {
    width: 20%;
  }

  .judul {
    width: 50%;
    font-size: xx-large;
    text-align: center;
  }

  .section-title {
    text-align: center;
    font-size: x-large;
  }

  .labelpasien {
    width: 30%;
    font-size: small;
  }

  .labelpasien td {
    font-size: x-small;
  }

  .textkecil {
    font-size: small;
  }

  .textbesar {
    font-size: x-large;
  }

  .datapasien {
    text-align: left;
  }

  .riwayatpenyakit {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  .riwayatpenyakit tr td {
    width: 30%;
    border: 1px solid black;
    padding: 2px;
  }

  .riwayatpenyakit th {
    text-align: center;
    border: 1px solid black;
    background-color: grey;
    color: white;
  }

  .riwayatobstetri {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    border: 1px solid black;
    padding: 2px;
    width: 100%;
  }

  .riwayatobstetri th {
    text-align: center;
    border: 1px solid black;
    background-color: grey;
    color: white;
  }

  .riwayatobstetri tr td {
    border: 1px solid black;
    padding: 2px;
  }

  .detailobstetri table {
    border: none;
    width: 100%;
    border-collapse: collapse;
  }

  .detailobstetri tr td {
    border: none;
    border-collapse: collapse;
  }

  .tandavital {
    font-family: Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
  }

  .tandavital tr td {
    width: 30%;
    border: 1px solid black;
    padding: 2px;
  }

  .tandavital th {
    text-align: center;
    border: 1px solid black;
    background-color: grey;
    color: white;
  }

  .pemfisik {
    font-family: Arial, Helvetica, sans-serif;
    width: 100%;
  }

  .section {
    page-break-inside: avoid;
  }
</style>
<table style=" width:100%; border-bottom: 2px solid black;">
  <tr style="border: 1px;">
    <td class="logo">KLINIK dr. PUTRI</td>
    <td class="judul">
      <h3>RESUME MEDIS PASIEN</h3>
    </td>
    <td class="labelpasien">
      <table>
        <tr>
          <td>NO RM</td>
          <td>:</td>
          <td>{{ $exam->patient->mr_number }}</td>
        </tr>
        <tr>
          <td>NAMA</td>
          <td>:</td>
          <td>{{ $exam->patient->name }}</td>
        </tr>
        <tr>
          <td>ALAMAT</td>
          <td>:</td>
          <td>{{ $exam->patient->address }}</td>
        </tr>
        <tr>
          <td>TGL LAHIR</td>
          <td>:</td>
          <td>{{ Carbon::parse($exam->patient->birth_date)->isoFormat('DD MMMM YYYY') }}</td>
        </tr>
        <tr>
          <td>TGL PERIKSA</td>
          <td>:</td>
          <td>{{ Carbon::parse($exam->exam_date)->isoFormat('DD MMMM YYYY') }}</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<div class="section">
  <h1 class="section-title">DATA PASIEN</h1>
  <table class="datapasien">
    <tbody>
      <tr>
        <th>Nama</th>
        <td>:</td>
        <td>{{ $exam->patient->name }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Tgl Lahir</th>
        <td>:</td>
        <td>{{ Carbon::parse($exam->patient->birth_date)->isoFormat('DD MMMM YYYY') }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Nomor MR</th>
        <td>:</td>
        <td>{{ $exam->patient->mr_number }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Nama Suami</th>
        <td>:</td>
        <td>{{ $exam->patient->spouse_name }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Alamat</th>
        <td>:</td>
        <td>{{ $exam->patient->address }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Telepon</th>
        <td>:</td>
        <td>{{ $exam->patient->phone }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Golongan Darah</th>
        <td>:</td>
        <td>{{ $exam->patient->blood_type }}</td>
        <td></td>
      </tr>
      <tr>
        <th>Berat Badan</th>
        <td>:</td>
        <td>{{ $exam->weight }} kg</td>
        <td></td>
      </tr>
      <tr>
        <th>Tinggi Badan</th>
        <td>:</td>
        <td>{{ $exam->height }} cm</td>
        <td></td>
      </tr>
      <tr>
        <th>Golongan Darah Suami</th>
        <td>:</td>
        <td>{{ $exam->patient->blood_type }}</td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">RIWAYAT PENYAKIT</h1>
  <table class="riwayatpenyakit">
    <thead>
      <tr>
        <th>Riwayat Penyakit</th>
        <th>Dahulu</th>
        <th>Keluarga</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($diseaseList as $disease)
      <tr>
        <td>{{ $disease->name }}</td>
        <td>{{ $exam->patient->diseaseHistory->contains($disease->id) ? 'Ada' : '-' }}</td>
        <td>{{ $exam->patient->familyDiseaseHistory->contains($disease->id) ? 'Ada' : '-' }}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">RIWAYAT OBSTETRI</h1>
  <table class="riwayatobstetri">
    <thead>
      <tr>
        <th style="width: 3%;">NO</th>
        <th>RIWAYAT</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($exam->patient->obstetry as $index => $item)
      <tr>
        <td>{{ $index+1 }}</td>
        <td>
          <table class="detailobstetri" style="width: 100%;">
            <tr>
              <td style="width: 20%;">Para abortus :</td>
              <td style="width: 30%;">{{ $item->paraabortus }}</td>
              <td style="width: 20%;">Jenis Kelamin :</td>
              <td style="width: 30%;">{{ $sex[$item->sex] }}</td>
            </tr>
            <tr>
              <td style="width: 20%;">Berat Bayi :</td>
              <td style="width: 30%;">{{ $item->birth_weight }}</td>
              <td style="width: 20%;">Panjang Bayi:</td>
              <td style="width: 30%;">{{ $item->birth_length }}</td>
            </tr>
            <tr>
              <td style="width: 20%;">Tahun :</td>
              <td style="width: 30%;">{{ $item->year }}</td>
              <td style="width: 20%;">Cara Persalinan dan Indikasi:</td>
              <td style="width: 30%;">{{ $item->childbirth_method }}</td>
            </tr>
            <tr>
              <td style="width: 20%;">Paritas :</td>
              <td style="width: 30%;">G{{ $item->g }}P{{ $item->p }}A{{ $item->a }}</td>
              <td style="width: 20%;">Kelainan / Komplikasi:</td>
              <td style="width: 30%;">{{ $item->complication }}</td>
            </tr>
            <tr>
              <td style="width: 20%;">HPHT :</td>
              <td style="width: 30%;">{{ $item->first_day_of_last_period }}</td>
              <td style="width: 20%;">Estimasi Kelahiran:</td>
              <td style="width: 30%;">{{ $item->pregnancy_estimate }}</td>
            </tr>
            <tr>
              <td style="width: 20%;">Kehamilan (Tunggal/Kembar):</td>
              <td style="width: 30%;">{{ $item->pregnancy_type }}</td>
              <td style="width: 20%;">Khorionisitas:</td>
              <td style="width: 30%;">{{ $item->chroniocity }}</td>
            </tr>
          </table>
        </td>
      </tr>
      {{-- <tr>
      <td>{{ $index+1 }}</td>
      <td>{{ $item->paraabortus }}</td>
      <td>{{ $sex[$item->sex] }}</td>
      <td>{{ $item->year }}</td>
      <td>{{ $item->childbirth_method }}</td>
      <td>{{ $item->birth_weight }}</td>
      <td>{{ $item->complication }}</td>
      <td>{{ $item->birth_place }}</td>
      </tr> --}}
      @endforeach
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">TANDA VITAL</h1>
  <table class="tandavital">
    <thead>
      <tr>
        <th>Tanda Vital</th>
        <th>Nutrisi</th>
        <th>Riwayat Jatuh</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>Tekanan Darah : {{ $exam->blood_pressure }}</td>
        <td>Berat Badan : {{ $exam->weight }}</td>
        <td>Riwayat Jatuh 3 Bulan Terakhir :</td>
      </tr>
      <tr>
        <td>Frekuensi Nadi : {{ $exam->heart_rate }}</td>
        <td>Tinggi Badan : {{ $exam->height }}</td>
        <td rowspan="3">{{ $exam->fall_history }}</td>
      </tr>
      <tr>
        <td>Suhu : {{ $exam->body_temperature }}</td>
        <td></td>
      </tr>
      <tr>
        <td>Frekuensi Nafas : {{ $exam->respiratory_rate }}</td>
        <td></td>
      </tr>
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">PEMERIKSAAN FISIK</h1>
  <table class="pemfisik">
    <tbody>
      <tr>
        <td style="width: 25%;">Keluhan Utama</td>
        <td style="width: 2%;">:</td>
        <td> {{ $exam->subjective_symptoms }}</td>
      </tr>
      <tr>
        <td>Kepala</td>
        <td>:</td>
        <td> {{ $exam->head }}</td>
      </tr>
      <tr>
        <td>Mata</td>
        <td>:</td>
        <td> {{ $exam->eye }}</td>
      </tr>
      <tr>
        <td>THT</td>
        <td>:</td>
        <td> {{ $exam->otology }}</td>
      </tr>
      <tr>
        <td>Leher</td>
        <td>:</td>
        <td> {{ $exam->neck }}</td>
      </tr>
      <tr>
        <td>Mulut</td>
        <td>:</td>
        <td> {{ $exam->oral }}</td>
      </tr>
      <tr>
        <td>Jantung &amp; Pembuluh Darah</td>
        <td>:</td>
        <td> {{ $exam->cardiology }}</td>
      </tr>
      <tr>
        <td>Thoraks, Paru,Payudara</td>
        <td>:</td>
        <td> {{ $exam->thorax_chest }}</td>
      </tr>
      <tr>
        <td>Adomen</td>
        <td>:</td>
        <td> {{ $exam->abdomen }}</td>
      </tr>
      <tr>
        <td>Kulit &amp; Sistem Limfatik</td>
        <td>:</td>
        <td> {{ $exam->dermatology }}</td>
      </tr>
      <tr>
        <td>Tulang Belakang&amp; Anggota Tubuh</td>
        <td>:</td>
        <td> {{ $exam->backbone_anatomy }}</td>
      </tr>
      <tr>
        <td>Sistem Saraf</td>
        <td>:</td>
        <td> {{ $exam->neurology }}</td>
      </tr>
      <tr>
        <td>Genitalia, Anus &amp; Rectum</td>
        <td>:</td>
        <td> {{ $exam->genital }}</td>
      </tr>
      <tr>
        <td>Extrimitas</td>
        <td>:</td>
        <td> {{ $exam->extremity }}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">PEMERIKSAAN KEHAMILAN</h1>
  <table class="pemfisik">
    <thead></thead>
    <tbody>
      <tr>
        <td>Taksiran Partus</td>
        <td>:</td>
        <td></td>
      </tr>
      <tr>
        <td>Vagina Toucher</td>
        <td>:</td>
        <td>{{ $exam->vagina_toucher }}</td>
      </tr>
      <tr>
        <td>Tinggi Hodge</td>
        <td>:</td>
        <td>{{ $exam->hodge_height }}</td>
      </tr>
      <tr>
        <td>Effice</td>
        <td>:</td>
        <td>{{ $exam->effice }}</td>
      </tr>
      <tr>
        <td>Blood Slem</td>
        <td>:</td>
        <td>{{ $exam->blood_slem }}</td>
      </tr>
      <tr>
        <td>Ketuban</td>
        <td>:</td>
        <td>{{ $exam->fetal_membrane }}</td>
      </tr>
      <tr>
        <td>Keterangan</td>
        <td>:</td>
        <td>{{ $exam->obstetric_note }}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">PEMERIKSAAN KANDUNGAN</h1>
  <table class="pemfisik">
    <thead></thead>
    <tbody>
      <tr>
        <td style="width: 25%;">Vulva / Vagina</td>
        <td style="width: 2%;">:</td>
        <td>{{ $exam->vulva_vagina }}</td>
      </tr>
      <tr>
        <td>Porsio</td>
        <td>:</td>
        <td>{{ $exam->porsio }}</td>
      </tr>
      <tr>
        <td>Corpus Uteri</td>
        <td>:</td>
        <td>{{ $exam->corpusutery }}</td>
      </tr>
      <tr>
        <td>Adnexa Parametrium</td>
        <td>:</td>
        <td>
          <table>
            <tr>
              <td>D</td>
              <td>{{ $exam->adnexad }}</td>
            </tr>
            <tr>
              <td>S</td>
              <td>{{ $exam->adnexas }}</td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td>Cavum Douglas</td>
        <td>:</td>
        <td>{{ $exam->cavumdouglas }}</td>
      </tr>
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">PEMERIKSAAN PENUNJANG</h1>
  <table style="width: 100%;">
    <thead>
      <tr>
        <td>
          <h2 class="section-title">USG</h2>
        </td>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>
          <table class="pemfisik">
            <thead></thead>
            <tbody>
              <tr>
                <td style="width: 25%;">Biometri</td>
                <td style="width: 2%;">:</td>
                <td>{{ $exam->usg_biometry }}</td>
              </tr>
              <tr>
                <td>TBJ</td>
                <td>:</td>
                <td>{{ $exam->usg_fetal_weight_estimate }}</td>
              </tr>
              <tr>
                <td>DJJ</td>
                <td>:</td>
                <td>{{ $exam->usg_fetal_heart_beat }}</td>
              </tr>
              <tr>
                <td>Cairan Amnion</td>
                <td>:</td>
                <td>{{ $exam->usg_amnion }}</td>
              </tr>
              <tr>
                <td>Plasenta</td>
                <td>:</td>
                <td>{{ $exam->usg_placenta }}</td>
              </tr>
              <tr>
                <td>Gerak Janin</td>
                <td>:</td>
                <td>{{ $exam->usg_fetal_movement }}</td>
              </tr>
              <tr>
                <td>Fetal Survey</td>
                <td>:</td>
                <td>{{ $exam->usg_fetal_survey }}</td>
              </tr>
              <tr>
                <td>Screening</td>
                <td>:</td>
                <td>{{ $exam->usg_screening }}</td>
              </tr>
              <tr>
                <td>Adnexa</td>
                <td>:</td>
                <td>D {{ $exam->adnexad }} </td>
              </tr>
              <tr>
                <td></td>
                <td></td>
                <td>S {{ $exam->adnexas }}</td>
              </tr>
              <tr>
                <td>Lain-Lain</td>
                <td>:</td>
                <td>{{ $exam->usg_others }}</td>
              </tr>
            </tbody>
          </table>
        </td>
      </tr>
    </tbody>
  </table>
</div>
<div class="section">
  <h1 class="section-title">DIAGNOSIS</h1>
  <h3>Diagnosa Utama : {{ $exam->diagnosis }}</h3>

  <h3>Diagnosa Sekunder :</h3>
  <ul>
    @foreach ($exam->secondaryDiagnosis as $item)
    <li>{{ $item->diagnosis }}</li>
    @endforeach
  </ul>
</div>
<div class="section">
  <h1 class="section-title">RENCANA</h1>
  <table style="width: 100%;">
    <tr>
      <td style="width: 50%; text-align: center;">
        <h2>TERAPI</h2>
      </td>
      <td style="width: 50%;text-align: center;">
        <h2>MONITORING</h2>
      </td>
    </tr>

    <tr>
      <td>{{ $exam->planning }}</td>
      <td>
        {{ $exam->monitoring }}
      </td>
    </tr>
  </table>
</div>

</html>