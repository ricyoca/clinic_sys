@extends('print._layouts')

@section('content')

<table width="100%">
  <thead>
    <tr>
      <th>
        <div style="text-align: center">
          <h3 style="margin:0">Klinik dr. Putri Kencana Purwanto, Sp.OG, M.Biomed</h3>
          <div style="font-weight: normal">Jl. Mulyorejo Indah I, Mulyorejo, Kec. Mulyorejo, Surabaya</div>
          <div style="font-weight: normal">No. SIP: 503.446/01691/III/IP.DS/436.7.2/2019</div>
        </div>
        <hr>
      </th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <h4 style="text-align: center">Bukti Pembayaran</h4>
        <table width="100%" style="">
          <tr>
            <td>Pasien</td>
            <td>: {{ $exam->patient->name }}</td>
            <td style="text-align: right">Nomor :</td>
            <td>{{ $exam->invoice_id }}</td>
          </tr>
          <tr>
            <td>Terima dari</td>
            <td>: {{ $exam->invoice->billed_to }}</td>
            <td style="text-align: right">Tanggal :</td>
            <td>{{ $exam->invoice->billed_at->isoFormat('DD MMMM Y') }}</td>
          </tr>
        </table>
        <div>Untuk Pembayaran</div>

        <table width="100%" style="margin-left: 0.5em;">
          <tr>
            <td colspan="6" class="h5">Tindakan</td>
          </tr>
          @foreach ($exam->invoice->invoiceTreatment as $item)
          <tr>
            <td colspan="4">{{ $item->description }}</td>
            <td style="text-align: right">Rp</td>
            <td style="text-align: right">{{ number_format($item->subtotal) }}</td>
          </tr>    
          @endforeach
          
          <tr style="font-weight: bold;border-top:1px;">
            <td colspan="4"></td>
            <td style="text-align: right">Rp</td>
            <td style="text-align: right">{{ number_format($exam->invoice->invoiceTreatment->sum('subtotal')) }}</td>
          </tr>
          <tr style="border-top:1px;">
            <td colspan="6" class="h5">Obat</td>
          </tr>
          @foreach ($exam->invoice->invoiceMedication as $item)
          <tr style="page-break-inside: avoid;">
            <td>{{ $item->description }}</td>
            <td>{{ $item->qty }} {{ $item->uom }}</td>
            <td style="text-align: right">@ Rp</td>
            <td style="text-align: right">{{ number_format($item->unit_price) }} =</td>
            <td style="text-align: right">Rp</td>
            <td style="text-align: right">{{ number_format($item->subtotal) }}</td>
          </tr>
          @endforeach
          <tr style="font-weight: bold;border-top:1px;" class="border-top font-weight-bold">
            <td colspan="4"></td>
            <td style="text-align: right">Rp</td>
            <td style="text-align: right">{{ number_format($exam->invoice->invoiceMedication->sum('subtotal')) }}</td>
          </tr>
          <tr style="font-weight: bold;" class="border-top font-weight-bold">
            <td style="text-align: right;border-top: 1px solid; border-collapse:collapse" colspan="4">Total</td>
            <td style="text-align: right;border-top: 1px solid; border-collapse:collapse">Rp</td>
            <td style="text-align: right;border-top: 1px solid; border-collapse:collapse">{{ number_format($exam->invoice->grand_total) }}
            </td>
          </tr>
        </table>
        <div> Terbilang : {{ strtoupper( Terbilang::make($exam->invoice->grand_total) . ' rupiah' ) }} </div>
        <table width="100%" style="page-break-inside: avoid;">
          <tr>
            <td style="text-align: center" width="50%">
              Pasien/Wali<br>
              <br>
              <br>
              <br>
              <br>
              (_____________________)
            </td>
            <td style="text-align: center">
              Surabaya, {{ \Carbon\Carbon::now()->setTimezone(request()->cookie('timezone'))->isoFormat('DD MMMM Y') }}
              <br>
              <br>
              <br>
              <br>
              <br>
              (dr. Putri Kencana Purwanto, Sp.OG, M.Biomed)
            </td>

          </tr>

        </table>
      </td>
    </tr>
  </tbody>
</table>
@endsection