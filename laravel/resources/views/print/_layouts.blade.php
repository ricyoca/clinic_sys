<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  {{-- @include('print._bootstrap') --}}
  <style>
    

    @media screen, print {

      html {
        font-family: Arial, Helvetica, sans-serif
      }
    }
  </style>
  @stack('styles')
</head>

<body>
  <div class="container">
    @yield('content')
  </div>
</body>

</html>